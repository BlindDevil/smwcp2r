;HDMA codes for Sideshow Showdown (used by 2 files and 4 sublevels).

!gradtable = $7F9E00			;no need to change this

init:
LDX.b #TablesEnd-Table1

-
LDA.l Table1,x
STA !gradtable,x

DEX
CPX #$FF
BNE -

LDA.b #!gradtable>>16				;load bank of one of the tables
STA $4344					;store.
STA $4334					;store.
REP #$20
LDA #$3200
STA $4330
LDA #$3202
STA $4340					;16-bit A
LDA.w #!gradtable				;load address of first table
STA $4342					;store.
LDA.w #!gradtable+(Table2-Table1)		;load address of second table
STA $4332					;store.
SEP #$20
LDA #$18
TSB $0D9F|!addr

nmi:
LDA $24					;load layer 3 Y-pos low byte
AND #$0F				;preserve bits 0-3 only
STA $0F64|!addr				;store to free RAM.

LDA.l Table1				;load scanline count from first table
SEC					;set carry
SBC $0F64|!addr				;subtract value
STA !gradtable				;store to gradient RAM table.

LDA.l Table2				;load scanline count from second table
SEC					;set carry
SBC $0F64|!addr				;subtract value
STA.l !gradtable+(Table2-Table1)	;store to gradient RAM table.
RTL					;return.

Table1:
db $13,$22,$42
db $02,$22,$43
db $10,$23,$43
db $0E,$24,$43
db $03,$24,$44
db $10,$25,$44
db $0D,$26,$44
db $03,$26,$45
db $10,$27,$45
db $0B,$28,$45
db $6F,$25,$46
db $00

Table2:
db $0F,$83
db $0C,$84
db $0C,$85
db $0C,$86
db $0C,$87
db $0C,$88
db $0C,$89
db $0C,$8A
db $0C,$8B
db $02,$8C
db $6F,$89
db $00
TablesEnd: