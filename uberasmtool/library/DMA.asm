;DMA handlers (all to be called from 'nmi')

;Relevant labels:
;CGRAM
;UseIndex (to be called with with 16-bit A)
;UseNoIndex (to be called with with 16-bit A)

;Upload entire palette ($0703) into CGRAM (channel 2)
;No inputs/outputs.
CGRAM:
REP #$20
LDY #$00                ;\Set Address for CG-RAM Write to 00
STY $2121               ;/Address for CG-RAM Write
LDA #$2200              ;\One register write once, to 2122
STA $4320               ;/
LDA #$0703		;\Address of palette to upload
STA $4322               ;/ 
if !sa1
	LDY #$00		;\Bank to grab palette from
else
	LDY #$7E		;\Bank to grab palette from
endif
STY $4324               ;/ 
LDA #$0200              ;\Upload the whole palette, 200 bytes
STA $4325               ;/
LDX #$04		;\Start DMA
STX $420B		;/
SEP #$20
RTL

;Upload tilemap/GFX into VRAM (channel 2)
;If indexing is used, this gets somewhat complex.
;After the first portion of data is transferred, the code adds the value of bytes transferred to the index,
;so you don't have to do it yourself.

;Inputs:
;$00 (16-bit) = amount of bytes to transfer (if indexed, will transfer the same amount per part)
;$02 (24-bit) = use the 16-bit value from this address as the index for parts to upload (in other words, this is an indirect long address) - 'UseIndex' label only
;$07 (24-bit) = source data to upload to VRAM
;$0A (16-bit) = VRAM destination

UseIndex:
LDA [$02]		;indirectly load value from the long address stored in this scratch RAM
LSR			;divide by 2 - the DMA'd register is a write-twice-type one
CLC			;clear carry
ADC $0A			;add value corresponding to our VRAM destination
STA $2116		;store result to our VRAM destination which will be written to.

LDA [$02]		;indirectly load value from the long address stored in this scratch RAM
CLC			;clear carry
ADC $07			;add value of address corresponding to the GFX/tilemap (source) to upload
STA $4322		;store to source low/high byte register.

LDA [$02]		;indirectly load value from the long address stored in this scratch RAM yet again
CLC			;clear carry
ADC $00			;add value corresponding to amount of bytes to be transferred
STA [$02]		;store result back to address, indirectly.
BRA Finagler		;the rest is the rest, default stuff lol

UseNoIndex:
LDA $0A			;load VRAM destination
STA $2116		;store value in A to our VRAM destination which will be written to.

LDA $07			;load address of GFX (source) to upload
STA $4322		;store to source low/high byte register.

Finagler:
LDX $09			;load bank of GFX (source) to upload
STX $4324               ;store to source bank byte register.

LDX #$01		;$01 = transfer from cpu to ppu, $81 = transfer from ppu to cpu
STX $4320
LDX #$18		;DMA to: $2118 (VRAM)
STX $4321

LDA $00			;load amount of bytes to transfer from scratch RAM
STA $4325               ;store to bytes to transfer register.
LDX #$04		;load value (DMA on channel 2)
STX $420B		;store to DMA channel enable register.
SEP #$20		;8-bit A
RTL			;return.