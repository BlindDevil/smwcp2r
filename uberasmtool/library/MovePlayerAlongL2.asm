;this code makes the player "stand still" when standing on a layer 2 tile during autoscrollers
;that is, layer 2 scroll is set to none (so it doesn't actually move), and the player position is relative to layer 1
;so what this does is adding the amount of pixels layer 1 has moved to the player's X-pos within the level

run:
LDA $9D			;load sprites/animation locked flag
ORA $13D4|!addr		;OR with game paused flag
BNE +++			;if anything is set, return.

LDA $140E|!addr		;load "player is standing on layer 2" flag
BEQ +++			;if not, return.

LDA $17BD|!addr		;load how much layer 1 has moved in the current frame
REP #$20		;16-bit A
BPL +			;if value is positive, branch.

ORA #$FF00		;set all bits of high byte - make it a 16-bit negative value
BRA ++			;branch ahead

+
AND #$00FF		;clear all bits of high byte - erase garbage

++
CLC			;clear carry
ADC $94			;add player's X-pos within the level, next frame
STA $94			;store result in there.
SEP #$20		;8-bit A

+++
RTL			;return.