;Code that counts bits, used to keep track of all SMWC coins collected - by Blind Devil
;This counts all permanent (collected in a beaten level) SMWC coins, converting them from
;per-level bitflags to a numerical result.
;Outputs:
;A (16-bit) - number of permanent SMWC coins. Code returns with 16-bit A.

run:
STZ $00			;bits counted = 0
STZ $01			;it's 16-bit so reset the high byte as well

LDX #$5F		;loop count (number of translevels)
-
LDA !SMWCCoins,x	;load coins collected in level according to index
AND #$07		;we'll count 3 bits per level (remember, these are the permanent ones)
BEQ .endloop		;if zero, there's nothing to add, so don't even bother wasting cycles there.

STA $02			;possible values range from #%00000000-#%00000111.

.bitloop
LDA $02			;load bits
AND #$01		;preserve bit 0 only
STA $03			;store to scratch RAM. possible values: 0 or 1.

LDA $00			;load bits counted
CLC			;clear carry
ADC $03			;add value from scratch RAM
STA $00			;store result back.
LDA $01			;load high byte of bits counted
ADC #$00		;add carry if set
STA $01			;store it.

LDA $02			;take all bits again
BEQ .endloop		;if zero, there's nothing else to add so go away.
LSR			;else, right-shift bits once (so bit 2 becomes bit 1, and bit 1 becomes bit 0)
STA $02			;store result back.
BRA .bitloop		;loop.

.endloop
DEX			;decrement X by one
BPL -			;loop while it's positive.

REP #$20		;16-bit A
LDA $00			;load counted bits
RTL			;return.