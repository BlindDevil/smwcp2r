;Multilayer "Akarui Avenue/Onsen Overhang" BG - includes parallax (channel 5).
;channel 5: layer 2 parallax

;Note that this doesn't handle gradients - so use the 2-channel gradient code from the other library file.

!hdmaparallaxtbls = $7F9E00		;no need to change this, this address is nice lol
!L3MsgYPos = $0080			;Y-pos (relative to screen) of the message on the layer 3 tilemap
!ManualMsg = $87			;free RAM: should be set to #$00 or #$01 depending on which message (1 or 2) you want to display.
					;(uberASM main code can't read values that change during an active message box period for some reason)

NMIBGUpd:
LDX $010B|!addr			;load level number, low byte
CPX #$34			;compare to value
BNE +				;if equal, skip ahead.

REP #$20			;16-bit A
LDA #$00BA			;load value
STA $1468|!addr			;store to layer 2 Y-pos, next frame. update vram independently, but NOT the parallax.
SEP #$20			;8-bit A
RTL				;return.

+
REP #$20			;16-bit A
LDA $1464|!addr			;load layer 1 Y-pos, next frame
LSR #5				;divide by 2 five times
CLC				;clear carry
ADC #$00BA			;add value
STA $1468|!addr			;store to layer 2 Y-pos, next frame. update vram independently, but NOT the parallax.
SEP #$20			;8-bit A
RTL				;return.

HDMA:
REP #$20
LDA #$0F03			;mode 3 on $210F (a.k.a. affect layer 2 XY-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5
RTL				;return.

Logic:
PHB				;preserve data bank
PHK				;push program bank into stack
PLB				;pull it back as the new data bank

;vvvvvv  the amount to decrement from scanlines table according to the value defined here  vvvvvv
LDX $010B|!addr			;load level number, low byte
CPX #$34			;compare to value
BNE +				;if not equal, skip ahead.

REP #$20			;16-bit A
STZ $00				;reset scratch RAM. nothing to save here, honestly.
BRA ++				;branch ahead

+
REP #$20			;16-bit A
LDA $1C				;load layer 1 Y-pos, current frame
LSR #5				;divide by 2 five times
STA $00				;store to scratch RAM.

++
CLC				;clear carry
ADC #$00BA			;add value
STA $1468|!addr			;store to layer 2 Y-pos, next frame. update vram independently, but NOT the parallax.

LDX #$00			;HDMA position table index
LDY #$00			;HDMA position entry index

ScCountLooper:
LDA Tbl1,y			;load scanline count from table according to index
CPY #$00			;check Y to see if we're reading the first entry
BNE +				;if not, skip ahead.

SubLoop:
SEC				;set carry
SBC $00				;subtract layer 2 Y-pos from it
BEQ .wellzero			;if equal zero, go to next entry.
BPL +				;only store its value to the HDMA table if positive.

.wellzero
LDA $00				;load value from scratch RAM
SEC				;set carry
SBC Tbl1,y			;subtract value from table according to index
STA $00				;store result back.

INY				;increment entry index by one
LDA Tbl1,y			;load scanline count from table according to index
BRA SubLoop			;try again

+
STA !hdmaparallaxtbls,x		;store scanline count to table.

REP #$20			;16-bit A
PHX				;preserve X
TYA				;transfer Y to A
ASL				;multiply by 2
TAX				;transfer to X
LDA !hdmaparallaxtbls+30,x	;load value from indexed scratch RAM
PLX				;restore X
STA !hdmaparallaxtbls+1,x	;store to table.
LDA $00				;load layer 2 Y-pos
CLC				;clear carry
ADC #$00BA			;add value
STA !hdmaparallaxtbls+3,x	;store to table.
SEP #$20			;8-bit A

INX #5				;increment table index five times
INY				;increment entry index by one
CPY.b #Tbl1End-Tbl1		;compare to end of table value
BNE ScCountLooper		;if not equal, redo the whole loop.

LDA #$00			;load zero value to finalize
STA !hdmaparallaxtbls,x		;store to table.

PLB				;restore previous data bank
RTL				;return.

ProcXOffsets:
REP #$20			;16-bit A
LDA $1E				;load layer 2 X-pos current frame
LSR				;divide by 2 twice
CLC				;clear carry
ADC $1E				;add layer 2 X-pos current frame
LSR #2				;divide by 2 twice
STA.l !hdmaparallaxtbls+36	;store to reserve RAM.
LSR				;divide by 2
CLC				;clear carry
ADC.l !hdmaparallaxtbls+36	;add value from reserve RAM
LSR				;divide by 2
STA.l !hdmaparallaxtbls+34	;store to reserve RAM.
LSR				;divide by 2
CLC				;clear carry
ADC.l !hdmaparallaxtbls+34	;add value from reserve RAM
LSR				;divide by 2
STA.l !hdmaparallaxtbls+32	;store to reserve RAM.
CLC				;clear carry
ADC.l !hdmaparallaxtbls+32	;add value from reserve RAM
LSR				;divide by 2
STA.l !hdmaparallaxtbls+30	;store to reserve RAM.
STA.l !hdmaparallaxtbls+28	;store to reserve RAM.
SEP #$20			;8-bit A
RTL				;return.

Tbl1:
db $7F,$10,$0E,$01
Tbl1End: