;Initializes various Conditional Direct Map16 blocks, like SMWC coins and midway points.
;Also sets $0F2F to #$30 to prevent OW indicator sprite tiles from appearing in level transitions.

init:
LDA #$30
STA $0F2F|!addr

LDA $141A|!addr		;load times the player has warped while in a level
BNE .ret		;if not zero, return.

LDA #$0F		;load value
STA $0DAE|!addr		;set brightness.
STZ $0D9F|!addr		;disable hdma

JSL OWStuff_GetLvlFromCoord
TAX

LDA !SMWCCoins,x
AND #$70
LSR #4
STA $7FC060
LDA !SMWCCoins,x
AND #$07
ORA $7FC060
STA $7FC060

LDA $7FB4D9,x
CMP #$01
BEQ +
CMP #$02
BNE .ret

LDA #$18
BRA ++

+
LDA #$08

++
ORA $7FC060
STA $7FC060

.ret
RTL