;Custom Game Mode: Prepare Stats screen (set up GFX, tilemaps, palettes and counters)
;by Blind Devil

!StatsScreenSong = $00		;formerly the new forest map theme, salvaged for the menu

!SaveSlotCoord = $7FCA24	;coordinates for the current save slot
!ExitCtCoord = $7FCB20		;coordinates for the exits counter
!SMWCCtCoord = $7FCBA4		;coordinates for the SMWC coin counter
!DeathCtCoord = $7FCCA4		;coordinates for the death counter
!TimeCtCoord = $7FCE26		;coordinates for the play time clock

!YSwitchX = $7C			;yellow switch block X-pos
!GSwitchX = $90			;green switch block X-pos
!RSwitchX = $A4			;red switch block X-pos
!BSwitchX = $B8			;blue switch block X-pos

!SwitchY = $7B			;all switch blocks Y-pos

!SwBlank = $E4			;empty switch block tile
!SwFilled = $E6			;filled switch block tile

!YSwitchProp = $30		;yellow switch block palette/properties
!GSwitchProp = $3A		;green switch block palette/properties
!RSwitchProp = $34		;red switch block palette/properties
!BSwitchProp = $32		;blue switch block palette/properties

init:
STZ $0D9F|!addr		;disable all hdma.

JSL $7F8000		;move all sprite tiles offscreen

LDX #$5F		;loop count

-
LDA Pal,x		;load color from table
STA $0703|!addr,x	;store to palette buffer.
DEX			;decrement X by one
BPL -			;loop while it's positive
JSL DMA_CGRAM		;upload palette to CGRAM via DMA

REP #$30		;16-bit AXY
STZ $1A			;reset layer 1 X-pos.
STZ $1C			;reset layer 1 Y-pos.

LDX #$074E		;loop count
LDA #$11FF		;load tile number and properties

-
STA $7EC800,x		;store to RAM buffer.
DEX #2			;decrement X twice
BPL -			;loop while it's positive
SEP #$30		;8-bit AXY

LDA #$80		;load value
STA $2100		;store to register to force blank.

;Layer 1 GFX DMA
REP #$20		;16-bit A
LDA #$06C0		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #statsgfx>>16	;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA.w #statsgfx		;load absolute address of tilemap table
STA $07			;store to scratch RAM.
STZ $0A			;reset scratch RAM ($0000 is our VRAM destination).
JSL DMA_UseNoIndex	;execute DMA (A goes 8-bit after this routine)

;Layer 2 Tilemap DMA
REP #$20			;16-bit A
LDA #$0800			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #statsl2tl_data>>16	;load bank byte of tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #statsl2tl_data		;load absolute address of tilemap table
STA $07				;store to scratch RAM.
LDA #$3000			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after this routine)
REP #$20			;16-bit A
LDA #$3400			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after this routine)
REP #$20			;16-bit A
LDA #$3800			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after this routine)
REP #$20			;16-bit A
LDA #$3C00			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after this routine)

;Layer 2 GFX DMA (would use decompression method but it often crashes on zsnes, and I really, REALLY want the game to work in it, too)
REP #$20		;16-bit A
LDA #$1000		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #ExGFX9F>>16	;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA.w #ExGFX9F		;load absolute address of tilemap table
STA $07			;store to scratch RAM.
LDA #$0800		;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_UseNoIndex	;execute DMA (A goes 8-bit after this routine)

REP #$20		;16-bit A
LDX #$DC		;loop count
-
LDA Layer1TL,x		;load tile number/properties from table according to index
STA $7EC890,x		;store to RAM buffer.

DEX #2			;decrement X twice
CPX #$FE		;compare X to value
BNE -			;if not equal, redo the loop.

REP #$10			;16-bit AXY
LDA #$0FFF			;load amount of bytes to transfer into A, minus one
LDX.w #statsl3tl		;load word address of our layer 3 tilemap from the library into X (the data source to transfer)
LDY.w #$C800			;load word address of where we want data to be transferred
db $54				;MVN opcode in hex, because we don't really know the bank our bin file is located, and it may vary on insertion
db $7F				;bank address of destination to transfer the data
db statsl3tl>>16|!bank8		;bank address of data source, the variable thing
SEP #$30			;8-bit XY, 16-bit A

;make the relevant changes to layer 3 ram counters here
JSR WriteCounters	;let's write ram to vram buffer, shall we?
RTL			;return.

main:
JSR WriteSwTiles

LDA #$80		;load value
STA $2100		;store to register to force blank.

;Layer 3 Tilemap DMA
REP #$20		;16-bit A
LDA #$1000		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #$7F		;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA.w #$C800		;load absolute address of tilemap table
STA $07			;store to scratch RAM.
LDA #$5000		;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_UseNoIndex	;execute DMA (A goes 8-bit after this routine)

;Layer 1 Tilemap DMA
REP #$20		;16-bit A
LDA #$0750		;load amount of bytes to transfer
STA $00			;store to scratch RAM.
LDX.b #$7E		;load bank byte of tilemap table into X
STX $09			;store X to scratch RAM.
LDA #$2000		;load VRAM destination
STA $0A			;store to scratch RAM.
JSL DMA_UseNoIndex	;execute DMA (A goes 8-bit after this routine)

if !StatsScreenSong
LDA #!StatsScreenSong	;load song number
STA $1DFB|!addr		;store to address to play it.
endif

LDA #$17
STA $212C
STZ $212D
STZ $40

INC $0100|!addr		;increment game mode by one
RTL			;return.

Pal:
db $86,$59,$00,$00,$d0,$7a,$dd,$7f,$93,$73,$00,$00,$bb,$0a,$ff,$03
db $93,$73,$00,$00,$fb,$0c,$eb,$2f,$93,$73,$00,$00,$dd,$7f,$7f,$2d
db $86,$59,$23,$10,$43,$18,$64,$20,$84,$28,$a4,$30,$a4,$34,$c4,$3c
db $e5,$44,$05,$4d,$25,$55,$45,$5d,$65,$65,$66,$69,$86,$71,$a6,$79
db $86,$59,$dd,$7f,$ff,$7f,$a5,$30,$e7,$40,$08,$4d,$6b,$5d,$10,$5a
db $73,$5e,$d6,$62,$39,$67,$7b,$6f,$de,$7b,$ef,$41,$6b,$49,$7f,$3b

Layer1TL:
db $FF,$11,$00,$09,$01,$09,$02,$09,$03,$09,$04,$09,$05,$09,$06,$09	;start of the stats text line ($00A0)
db $07,$09,$08,$09,$09,$09,$0A,$09,$0B,$09,$0C,$09,$0D,$09,$FF,$11
db $FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11
db $FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11
db $FF,$11,$0E,$09,$0F,$09,$10,$09,$11,$09,$12,$09,$13,$09,$14,$09
db $15,$09,$16,$09,$17,$09,$18,$09,$19,$09,$1A,$09,$1B,$09,$FF,$11
db $FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11
db $FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11
db $FF,$11,$1C,$09,$1D,$09,$1E,$09,$1F,$09,$20,$09,$21,$09,$22,$09
db $23,$09,$24,$09,$25,$09,$FF,$11,$26,$09,$27,$09,$28,$09,$FF,$11
db $FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11
db $FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11,$FF,$11
db $FF,$11,$29,$09,$2A,$09,$2B,$09,$2C,$09,$2D,$09,$2E,$09,$2F,$09
db $30,$09,$31,$09,$32,$09,$FF,$11,$33,$09,$34,$09,$35,$09		;end of the stats text line ($DE bytes to write total)

WriteCounters:
LDA $7E010A		;load current save file number (for some random reason the word address doesn't work, WTF)
CLC			;clear carry
ADC #$0A		;add value
STA !SaveSlotCoord	;store to address.

REP #$20		;16-bit A
LDA $7E1F2E		;load amount of events triggered (exits count)
AND #$00FF		;preserve low byte only
JSL Hex2Dec_run		;convert to decimal digits (A goes 8-bit after that)
STA !ExitCtCoord+4	;store ones to address.
CPY #$00		;check if hundreds is equal zero
BEQ +			;if yes, don't draw digit (keep the blank tile).
TYA			;transfer hundreds to A
STA !ExitCtCoord	;store hundreds to address.
BRA ++			;and in this case, always write tens.

+
CPX #$00		;check if tens is equal zero
BEQ +++			;if yes, don't draw digit (keep the blank tile).

++
TXA			;transfer tens to A
STA !ExitCtCoord+2	;store tens to address.

+++
REP #$20		;16-bit A
LDA $7FB599		;load death count
JSL Hex2Dec_run		;convert to decimal digits (A goes 8-bit after that)

STA !DeathCtCoord+4	;store ones to address.

CPY #$00		;check if hundreds is equal zero
BEQ NoHundreds		;if yes, don't draw digit (keep the blank tile).

TYA			;transfer hundreds to A
STA !DeathCtCoord	;store hundreds to address.
BRA WriteTens		;and in this case, always write tens.

NoHundreds:
CPX #$00		;check if tens is equal zero
BEQ NoTens		;if yes, don't draw digit (keep the blank tile).

WriteTens:
TXA			;transfer tens to A
STA !DeathCtCoord+2	;store tens to address.

NoTens:
JSL Hex2Dec_LeGameClock	;get converted hours and minutes of play time

LDA $03			;load hours' tens
STA !TimeCtCoord	;store to address.
LDA $02			;load hours' ones
STA !TimeCtCoord+2	;store to address.
LDA $01			;load hours' tens
STA !TimeCtCoord+6	;store to address.
LDA $00			;load hours' ones
STA !TimeCtCoord+8	;store to address.

JSL BitCoinCalc_run	;convert bitflags for all collected SMWC coins to numeric (A goes 16-bit after that)
JSL Hex2Dec_run		;convert to decimal digits (A goes 8-bit after that)

STA !SMWCCtCoord+4	;store ones to address.

CPY #$00		;check if hundreds is equal zero
BEQ NoHundreds2		;if yes, don't draw digit (keep the blank tile).

TYA			;transfer hundreds to A
STA !SMWCCtCoord	;store hundreds to address.
BRA WriteTens2		;and in this case, always write tens.

NoHundreds2:
CPX #$00		;check if tens is equal zero
BEQ NoTens2		;if yes, don't draw digit (keep the blank tile).

WriteTens2:
TXA			;transfer tens to A
STA !SMWCCtCoord+2	;store tens to address.

NoTens2:
RTS			;return.

WriteSwTiles:
LDA #!SwBlank		;load tile number for blank block
STA $00			;store to scratch RAM.
STA $01			;store to scratch RAM.
STA $02			;store to scratch RAM.
STA $03			;store to scratch RAM.

LDX #!SwFilled		;load tile number for filled block into X

LDA $7E1F28		;load yellow switch flag
BEQ +			;if not set, skip ahead.
STX $00			;store X to scratch RAM.

+
LDA $7E1F27		;load green switch flag
BEQ +			;if not set, skip ahead.
STX $01			;store X to scratch RAM.

+
LDA $7E1F2A		;load red switch flag
BEQ +			;if not set, skip ahead.
STX $02			;store X to scratch RAM.

+
LDA $7E1F29		;load blue switch flag
BEQ +			;if not set, skip ahead.
STX $03			;store X to scratch RAM.

+
LDA #!YSwitchX		;load yellow block's X-pos
STA $0340		;store to OAM.
LDA #!GSwitchX		;load green block's X-pos
STA $0344		;store to OAM.
LDA #!RSwitchX		;load red block's X-pos
STA $0348		;store to OAM.
LDA #!BSwitchX		;load blue block's X-pos
STA $034C		;store to OAM.
LDA #!SwitchY		;load blocks' Y-pos
STA $0341		;store to OAM.
STA $0345		;store to OAM.
STA $0349		;store to OAM.
STA $034D		;store to OAM.

LDA $00			;load yellow block tilemap
STA $0342		;store to OAM.
LDA $01			;load green block tilemap
STA $0346		;store to OAM.
LDA $02			;load red block tilemap
STA $034A		;store to OAM.
LDA $03			;load blue block tilemap
STA $034E		;store to OAM.

LDA #!YSwitchProp	;load yellow block properties
STA $0343		;store to OAM.
LDA #!GSwitchProp	;load green block properties
STA $0347		;store to OAM.
LDA #!RSwitchProp	;load red block properties
STA $034B		;store to OAM.
LDA #!BSwitchProp	;load blue block properties
STA $034F		;store to OAM.

LDA #$AA		;load tile size for all tiles
STA $0414		;store to OAM.

RTS			;return.