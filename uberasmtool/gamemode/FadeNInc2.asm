;Custom Game Mode: Fade music and screen back in to normal and increment gamemode address
;by Blind Devil

main:
LDA $0DAE|!addr		;load brightness mirror
CMP #$0F
BEQ IncGM		;if equal, increment game mode.

INC $0DAE|!addr		;decrement brightness by one
RTL			;return.

IncGM:
INC $0100|!addr		;increment game mode by one
RTL			;return.