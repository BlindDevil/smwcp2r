init:
STZ $2111
STZ $2111
STZ $2112
STZ $2112

LDA $62				;load number 2 on titlescreen flag
CMP #$02			;compare to value
BEQ +				;if equal, return.

LDA #$15			;load SFX value
STA $1DFC|!addr			;store to address to play it.

STZ $1DF5|!addr			;reset time to dismiss intro message.
INC $62				;increment address by one

+
RTL				;return.