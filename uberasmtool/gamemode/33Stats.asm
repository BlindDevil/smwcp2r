;Custom Game Mode: Stats screen (layer 2 scrolls diagonally and the play time gets updated unless the game is 100%)
;by Blind Devil

!TimeCtCoord = $7FCE26	;same as in gamemode 31
!fademusic = 0		;fading off if 0, fading on if 1

main:
LDA $16			;load controller data 1, first frame only
AND #$B0		;check if either B, start or select are pressed
BNE BackToMap		;if any of them are pressed, go back to the overworld.
LDA $16			;load controller data 2, first frame only
AND #$80		;check if A is pressed
BNE BackToMap

LDA $13			;load game frame counter
AND #$01		;check if bit 0 is set
BEQ +			;if not set, skip ahead.

REP #$20		;16-bit A
INC $1E			;increment layer 2 x-pos by one
INC $20			;increment layer 2 y-pos by one
SEP #$20		;8-bit A

+
;todo: check if game is complete, and if so, return to NoDMA

JSL Hex2Dec_LeGameClock	;get converted hours and minutes of play time

LDA $03			;load hours' tens
STA !TimeCtCoord	;store to address.
LDA $02			;load hours' ones
STA !TimeCtCoord+2	;store to address.
LDA $01			;load hours' tens
STA !TimeCtCoord+6	;store to address.
LDA $00			;load hours' ones
STA !TimeCtCoord+8	;store to address.

LDA $13			;load frame counter
LSR #5			;divide by 2 five times
AND #$01		;preserve bit 0
TAX			;transfer to X

LDA Tile,x		;load tile from table according to index (alternate between blank and colon tiles)
STA !TimeCtCoord+4	;store to address.

REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #$7F			;load bank byte of tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #$CE26			;load absolute address of tilemap table
STA $07				;store to scratch RAM.
LDA #$5313			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after that)

NoDMA:
RTL			;return.

BackToMap:
LDA #$0B		;load fade to map game mode
STA $0100|!addr		;store to gamemode address.

if !fademusic
LDA #$FF		;load value
STA $1DFB|!addr		;store to music address to make the song fade out.
endif
RTL			;return.

Tile:
db $78,$FC