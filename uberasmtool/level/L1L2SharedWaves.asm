!hdmaparallaxtbls = $7F9E00

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
REP #$20
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4350
LDA #$1002			;mode 2 on $2110 (a.k.a. affect layer 2 Y-wise)
STA $4360
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
STA $4362
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
STA $4364
LDA #$60			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channels 5 and 6

main:
LDA $9D
ORA $13D4|!addr
BNE .ret

LDX #$00			;\  Init of
LDY #$00			; | X and Y. Y will be the loop counter, X the index for writing the table to the RAM
LDA $14				; | Speed of Waves
LSR #2				; | Slowing down A
STA $00				;/  Save for later use

.Loop
LDA #$08			;\  Set scanline height
STA !hdmaparallaxtbls,x		; | for each wave
TYA				; | Transfer Y to A
ADC $00				; | Add in frame counter
AND #$07			; | only the lower half of the byte
PHY				; | Push Y, so that the loop counter isn't lost.
TAY				;/  Transfer A to Y

LDA.w Wave_Table,y		;\  Load in wave values
LSR				; | half of waves only
CLC				; | Clear carry flag for proper addition
ADC $1C				; | Add value from the wave table to layer y position (low byte).
STA !hdmaparallaxtbls+1,x	; | X position low byte
LDA $1D				; | Load layer y position (high byte).
ADC #$00			; | Add #$00 without clearing the carry for pseudo 16-bit addition
STA !hdmaparallaxtbls+2,x	;/  X position high byte

PLY				;\  Pull Y (original loop counter)
CPY #$1A			; | Compare with #$1C scanlines
BPL .End			; | If bigger, end HDMA
INX #3				; | Increase X three times, so that in the next loop, it writes the new table data at the end of the old one...
INY				; | Increase Y
BRA .Loop			;/  Do the loop

.End
LDA #$00			;\  End HDMA by writing
STA !hdmaparallaxtbls+3,x	;/ #$00 here

.ret
RTL				;return.

Wave_Table:
db $00,$01,$02,$03,$03,$02,$01,$00