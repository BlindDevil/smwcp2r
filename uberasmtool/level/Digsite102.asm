;Digsite Dangers ASM (all outdoor sublevels).
;Includes:
;load: SMWC coin custom trigger init
;init/main: parallax HDMA init/processing
;init: message box text DMA (complete)
;main: spawn cluster sandstorm, conditional message DMA per screen

!hdmaparallaxtbls = $7F9E00		;no need to change this, this address is nice lol
!DMAFlags = $0F5E|!addr			;dma flags

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
STZ !DMAFlags			;reset dma update flags
JSL ProcOffsets			;process offsets for use before we set the effect on

REP #$20
LDA #$0F03			;mode 3 on $210F (a.k.a. affect layer 2 XY-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1C>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA $010B
CMP #$019F
BNE +

LDX #$B0
STX $1887

LDA.w #MsgDMA5C			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58A0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

REP #$20			;16-bit A
LDA.w #MsgDMA6C			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CA0			;load VRAM destination
BRA SetDest			;branch to set destination and end this nightmare

+
LDA.w #MsgDMA1C			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58A0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

REP #$20			;16-bit A
LDA.w #MsgDMA2C			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CA0			;load VRAM destination

SetDest:
STA $0A				;store to scratch RAM.
JML DMA_UseNoIndex		;execute DMA and return

main:
LDA $010B
CMP #$02
BNE +

JSL DMAProc			;process messages dma before anything else (please don't break my hdma, pleaseeeeee...)

+
LDA #$0B			;load sprite number to spawn
JSL clusterspawn_run		;spawn cluster sandstorm

ProcOffsets:
REP #$20			;16-bit A
LDA $1E				;load layer 2 X-pos current frame
STA !hdmaparallaxtbls+34	;store to HDMA table.
LSR				;divide by 2
STA !hdmaparallaxtbls+30	;store to HDMA table.
STA !hdmaparallaxtbls+32	;store to HDMA table.

LDA $20				;load layer 2 Y-pos, current frame
SEC				;set carry
SBC #$0060			;subtract value
STA $0F60			;store to scratch RAM.
SEP #$20			;8-bit A

LDX #$00			;HDMA position table index
LDY #$00			;HDMA position entry index

ScCountLooper:
LDA Tbl1,y			;load scanline count from table according to index
CPY #$00			;check Y to see if we're reading the first entry
BNE +				;if not, skip ahead.

SubLoop:
SEC				;set carry
SBC $0F60			;subtract layer 2 Y-pos from it
BEQ .wellzero			;if equal zero, go to next entry.
BPL +				;only store its value to the HDMA table if positive.

.wellzero
LDA $0F60			;load value from scratch RAM
SEC				;set carry
SBC Tbl1,y			;subtract value from table according to index
STA $0F60			;store result back.

INY				;increment entry index by one
LDA Tbl1,y			;load scanline count from table according to index
BRA SubLoop			;try again

+
STA !hdmaparallaxtbls,x		;store scanline count to table.

REP #$20			;16-bit A
PHX				;preserve X
TYA				;transfer Y to A
ASL				;multiply by 2
TAX				;transfer to X
LDA !hdmaparallaxtbls+30,x	;load value from indexed scratch RAM
PLX				;restore X
STA !hdmaparallaxtbls+1,x	;store to table.
LDA $0F60			;load layer 2 Y-pos
CLC				;clear carry
ADC #$0060			;add value
STA !hdmaparallaxtbls+3,x	;store to table.
SEP #$20			;8-bit A

INX #5				;increment table index five times
INY				;increment entry index by one
CPY.b #Tbl1End-Tbl1		;compare to end of table value
BNE ScCountLooper		;if not equal, redo the whole loop.

LDA #$00			;load zero value to finalize
STA !hdmaparallaxtbls,x		;store to table.
-

LDA $010B
CMP #$A2
BEQ +

LDA $9D
ORA $13D4
BNE +

LDA $14
AND #$03
BNE +

LDA $1887
BEQ +

LDA #$1A
STA $1DFC

+
RTL				;return.

Tbl1:
db $7F,$3D,$01
Tbl1End:

DMAProc:
LDA $14
AND #$03
ORA $9D
ORA $13D4
BNE -				;we won't do anything in these conditions

REP #$20			;16-bit A
LDA #$0180			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA $94				;load player's X-pos within the level
CMP #$0158			;compare to value
BCC ClearFlags			;if lower, clear flags, update to messages 1 and 2.

SEP #$20			;8-bit A
LDA !DMAFlags			;load flags prior checks
CMP #$02			;compare to value
BEQ AllSet			;if equal, return. messages were already set.

INC !DMAFlags			;increment flags by one
CMP #$01			;compare to value
BEQ three			;if set, update to message 3.

REP #$20			;16-bit A
LDA.w #MsgDMA4			;load absolute address of text tilemap table 4
BRA le2cont			;branch

three:
REP #$20			;16-bit A
LDA.w #MsgDMA3			;load absolute address of text tilemap table 3
BRA le1cont			;branch

ClearFlags:
SEP #$20			;8-bit A
LDA !DMAFlags			;load flags prior checks
BEQ AllSet			;if equal zero, return. messages were already set.

DEC !DMAFlags			;decrement flags by one
BEQ one				;if equal zero, update to message 1.

REP #$20			;16-bit A
LDA.w #MsgDMA2			;load absolute address of text tilemap table 2

le2cont:
STA $07				;store to scratch RAM.
LDA #$5CC0			;load VRAM destination
JMP SetDest			;jump - set destination and execute dma

one:
REP #$20			;16-bit A
LDA.w #MsgDMA1			;load absolute address of text tilemap table 1

le1cont:
STA $07				;store to scratch RAM.
LDA #$58C0			;load VRAM destination
JMP SetDest			;jump - set destination and execute dma

AllSet:
RTL				;return.

MsgDMA1C:		;complete tilemap
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
MsgDMA1:
db $FE,$3C,$0E,$3D,$47,$3D,$1D,$3D,$FE,$3C,$47,$3D,$44,$3D,$58,$3D
db $FE,$3C,$0C,$3D,$40,$3D,$51,$3D,$48,$3D,$4E,$3D,$1B,$3D,$FE,$3C
db $14,$3D,$52,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$40,$3D,$51,$3D,$42,$3D,$47,$3D,$44,$3D,$4E,$3D,$4B,$3D
db $4E,$3D,$46,$3D,$48,$3D,$52,$3D,$53,$3D,$52,$3D,$FE,$3C,$49,$3D
db $54,$3D,$52,$3D,$53,$3D,$FE,$3C,$45,$3D,$4E,$3D,$54,$3D,$4D,$3D
db $43,$3D,$FE,$3C,$53,$3D,$47,$3D,$48,$3D,$52,$3D,$FE,$3C,$FE,$3C
db $FE,$3C,$4F,$3D,$58,$3D,$51,$3D,$40,$3D,$4C,$3D,$48,$3D,$43,$3D
db $1D,$3D,$FE,$3C,$40,$3D,$4D,$3D,$43,$3D,$FE,$3C,$56,$3D,$44,$3D
db $5D,$3D,$51,$3D,$44,$3D,$FE,$3C,$51,$3D,$44,$3D,$40,$3D,$4B,$3D
db $FE,$3C,$41,$3D,$54,$3D,$52,$3D,$58,$3D,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$43,$3D,$48,$3D,$46,$3D,$46,$3D,$48,$3D,$4D,$3D,$46,$3D
db $FE,$3C,$48,$3D,$53,$3D,$FE,$3C,$4E,$3D,$54,$3D,$53,$3D,$1B,$3D
db $FE,$3C,$05,$3D,$44,$3D,$44,$3D,$4B,$3D,$FE,$3C,$45,$3D,$51,$3D
db $44,$3D,$44,$3D,$FE,$3C,$53,$3D,$4E,$3D,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$46,$3D,$4E,$3D,$FE,$3C,$48,$3D,$4D,$3D,$FE,$3C,$40,$3D
db $4D,$3D,$43,$3D,$FE,$3C,$44,$3D,$57,$3D,$4F,$3D,$4B,$3D,$4E,$3D
db $51,$3D,$44,$3D,$1D,$3D,$FE,$3C,$53,$3D,$47,$3D,$4E,$3D,$54,$3D
db $46,$3D,$47,$3D,$1B,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C

MsgDMA2C:		;complete tilemap
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
MsgDMA2:
db $FE,$3C,$13,$3D,$47,$3D,$48,$3D,$52,$3D,$FE,$3C,$4F,$3D,$4B,$3D
db $40,$3D,$42,$3D,$44,$3D,$FE,$3C,$52,$3D,$54,$3D,$51,$3D,$44,$3D
db $4B,$3D,$58,$3D,$FE,$3C,$48,$3D,$52,$3D,$FE,$3C,$42,$3D,$51,$3D
db $44,$3D,$44,$3D,$4F,$3D,$58,$3D,$1B,$3D,$1B,$3D,$1B,$3D,$FE,$3C
db $FE,$3C,$44,$3D,$55,$3D,$44,$3D,$51,$3D,$58,$3D,$4E,$3D,$4D,$3D
db $44,$3D,$FE,$3C,$47,$3D,$44,$3D,$51,$3D,$44,$3D,$FE,$3C,$48,$3D
db $52,$3D,$FE,$3C,$53,$3D,$4E,$3D,$4E,$3D,$FE,$3C,$52,$3D,$42,$3D
db $40,$3D,$51,$3D,$44,$3D,$43,$3D,$FE,$3C,$53,$3D,$4E,$3D,$FE,$3C
db $FE,$3C,$46,$3D,$4E,$3D,$FE,$3C,$4F,$3D,$40,$3D,$52,$3D,$53,$3D
db $FE,$3C,$53,$3D,$47,$3D,$44,$3D,$FE,$3C,$45,$3D,$48,$3D,$51,$3D
db $52,$3D,$53,$3D,$FE,$3C,$51,$3D,$4E,$3D,$4E,$3D,$4C,$3D,$1B,$3D
db $FE,$3C,$13,$3D,$47,$3D,$44,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$56,$3D,$40,$3D,$4B,$3D,$4B,$3D,$52,$3D,$FE,$3C,$52,$3D
db $40,$3D,$58,$3D,$FE,$3C,$53,$3D,$47,$3D,$44,$3D,$51,$3D,$44,$3D
db $FE,$3C,$40,$3D,$51,$3D,$44,$3D,$FE,$3C,$4C,$3D,$54,$3D,$4C,$3D
db $4C,$3D,$48,$3D,$44,$3D,$52,$3D,$1B,$3D,$1B,$3D,$1B,$3D,$FE,$3C
db $FE,$3C,$44,$3D,$55,$3D,$48,$3D,$4B,$3D,$FE,$3C,$42,$3D,$40,$3D
db $53,$3D,$FE,$3C,$4C,$3D,$54,$3D,$4C,$3D,$4C,$3D,$48,$3D,$44,$3D
db $52,$3D,$1A,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C

MsgDMA3:
db $FE,$3C,$13,$3D,$47,$3D,$44,$3D,$FE,$3C,$50,$3D,$54,$3D,$48,$3D
db $42,$3D,$4A,$3D,$52,$3D,$40,$3D,$4D,$3D,$43,$3D,$FE,$3C,$48,$3D
db $4D,$3D,$52,$3D,$48,$3D,$43,$3D,$44,$3D,$FE,$3C,$48,$3D,$52,$3D
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$55,$3D,$48,$3D,$4E,$3D,$4B,$3D,$44,$3D,$4D,$3D,$53,$3D
db $1A,$3D,$FE,$3C,$16,$3D,$44,$3D,$FE,$3C,$40,$3D,$4B,$3D,$4C,$3D
db $4E,$3D,$52,$3D,$53,$3D,$FE,$3C,$4B,$3D,$4E,$3D,$52,$3D,$53,$3D
db $FE,$3C,$40,$3D,$FE,$3C,$46,$3D,$54,$3D,$58,$3D,$FE,$3C,$FE,$3C
db $FE,$3C,$53,$3D,$4E,$3D,$FE,$3C,$48,$3D,$53,$3D,$FE,$3C,$58,$3D
db $44,$3D,$52,$3D,$53,$3D,$44,$3D,$51,$3D,$43,$3D,$40,$3D,$58,$3D
db $1B,$3D,$FE,$3C,$07,$3D,$44,$3D,$FE,$3C,$56,$3D,$40,$3D,$52,$3D
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$46,$3D,$4E,$3D,$4E,$3D,$45,$3D,$48,$3D,$4D,$3D,$46,$3D
db $FE,$3C,$40,$3D,$51,$3D,$4E,$3D,$54,$3D,$4D,$3D,$43,$3D,$FE,$3C
db $4E,$3D,$4D,$3D,$FE,$3C,$53,$3D,$47,$3D,$4E,$3D,$52,$3D,$44,$3D
db $FE,$3C,$4E,$3D,$43,$3D,$43,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$45,$3D,$4B,$3D,$4E,$3D,$40,$3D,$53,$3D,$48,$3D,$4D,$3D
db $46,$3D,$FE,$3C,$41,$3D,$4B,$3D,$4E,$3D,$42,$3D,$4A,$3D,$52,$3D
db $1B,$3D,$FE,$3C,$01,$3D,$54,$3D,$53,$3D,$FE,$3C,$56,$3D,$47,$3D
db $58,$3D,$FE,$3C,$43,$3D,$4E,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$53,$3D,$47,$3D,$44,$3D,$58,$3D,$FE,$3C,$45,$3D,$4B,$3D
db $4E,$3D,$40,$3D,$53,$3D,$FE,$3C,$4E,$3D,$4D,$3D,$FE,$3C,$50,$3D
db $54,$3D,$48,$3D,$42,$3D,$4A,$3D,$52,$3D,$40,$3D,$4D,$3D,$43,$3D
db $1E,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C

MsgDMA4:
db $FE,$3C,$07,$3D,$44,$3D,$58,$3D,$1D,$3D,$FE,$3C,$56,$3D,$44,$3D
db $5D,$3D,$55,$3D,$44,$3D,$FE,$3C,$43,$3D,$48,$3D,$52,$3D,$42,$3D
db $4E,$3D,$55,$3D,$44,$3D,$51,$3D,$44,$3D,$43,$3D,$FE,$3C,$53,$3D
db $56,$3D,$4E,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$4F,$3D,$40,$3D,$53,$3D,$47,$3D,$52,$3D,$1B,$3D,$FE,$3C
db $16,$3D,$44,$3D,$5D,$3D,$51,$3D,$44,$3D,$FE,$3C,$52,$3D,$53,$3D
db $48,$3D,$4B,$3D,$4B,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$48,$3D,$4D,$3D,$55,$3D,$44,$3D,$52,$3D,$53,$3D,$48,$3D
db $46,$3D,$40,$3D,$53,$3D,$48,$3D,$4D,$3D,$46,$3D,$FE,$3C,$53,$3D
db $47,$3D,$44,$3D,$FE,$3C,$54,$3D,$4F,$3D,$4F,$3D,$44,$3D,$51,$3D
db $FE,$3C,$4F,$3D,$40,$3D,$53,$3D,$47,$3D,$1D,$3D,$FE,$3C,$FE,$3C
db $FE,$3C,$52,$3D,$4E,$3D,$FE,$3C,$4C,$3D,$54,$3D,$52,$3D,$47,$3D
db $51,$3D,$4E,$3D,$4E,$3D,$4C,$3D,$52,$3D,$FE,$3C,$4E,$3D,$4D,$3D
db $4B,$3D,$58,$3D,$1B,$3D,$FE,$3C,$16,$3D,$44,$3D,$FE,$3C,$43,$3D
db $4E,$3D,$4D,$3D,$5D,$3D,$53,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$56,$3D,$40,$3D,$4D,$3D,$4D,$3D,$40,$3D,$FE,$3C,$41,$3D
db $51,$3D,$44,$3D,$40,$3D,$4A,$3D,$FE,$3C,$40,$3D,$4D,$3D,$58,$3D
db $53,$3D,$47,$3D,$48,$3D,$4D,$3D,$46,$3D,$FE,$3C,$53,$3D,$47,$3D
db $40,$3D,$53,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$42,$3D,$4E,$3D,$54,$3D,$4B,$3D,$43,$3D,$FE,$3C,$41,$3D
db $44,$3D,$FE,$3C,$55,$3D,$40,$3D,$4B,$3D,$54,$3D,$40,$3D,$41,$3D
db $4B,$3D,$44,$3D,$1A,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C

MsgDMA5C:
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$16,$3D,$47,$3D,$40,$3D,$53,$3D,$FE,$3C,$56,$3D,$40,$3D
db $52,$3D,$FE,$3C,$53,$3D,$47,$3D,$40,$3D,$53,$3D,$FE,$3C,$4D,$3D
db $4E,$3D,$48,$3D,$52,$3D,$44,$3D,$1E,$3D,$FE,$3C,$03,$3D,$4E,$3D
db $4D,$3D,$5D,$3D,$53,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$53,$3D,$44,$3D,$4B,$3D,$4B,$3D,$FE,$3C,$4C,$3D,$44,$3D
db $FE,$3C,$58,$3D,$4E,$3D,$54,$3D,$FE,$3C,$4F,$3D,$51,$3D,$44,$3D
db $52,$3D,$52,$3D,$44,$3D,$43,$3D,$FE,$3C,$53,$3D,$47,$3D,$44,$3D
db $FE,$3C,$41,$3D,$54,$3D,$53,$3D,$53,$3D,$4E,$3D,$4D,$3D,$FE,$3C
db $FE,$3C,$54,$3D,$4F,$3D,$FE,$3C,$53,$3D,$47,$3D,$44,$3D,$51,$3D
db $44,$3D,$1D,$3D,$FE,$3C,$43,$3D,$48,$3D,$43,$3D,$FE,$3C,$58,$3D
db $4E,$3D,$54,$3D,$1E,$3D,$FE,$3C,$00,$3D,$00,$3D,$00,$3D,$00,$3D
db $11,$3D,$06,$3D,$07,$3D,$1A,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$13,$3D,$47,$3D,$40,$3D,$4D,$3D,$4A,$3D,$52,$3D,$FE,$3C
db $40,$3D,$FE,$3C,$4B,$3D,$4E,$3D,$53,$3D,$1D,$3D,$FE,$3C,$0C,$3D
db $40,$3D,$51,$3D,$48,$3D,$4E,$3D,$1B,$3D,$FE,$3C,$18,$3D,$44,$3D
db $40,$3D,$51,$3D,$52,$3D,$FE,$3C,$4E,$3D,$45,$3D,$FE,$3C,$FE,$3C
db $FE,$3C,$56,$3D,$4E,$3D,$51,$3D,$4A,$3D,$FE,$3C,$43,$3D,$4E,$3D
db $56,$3D,$4D,$3D,$FE,$3C,$53,$3D,$47,$3D,$44,$3D,$FE,$3C,$43,$3D
db $51,$3D,$40,$3D,$48,$3D,$4D,$3D,$1B,$3D,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C

MsgDMA6C:
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$03,$3D,$4E,$3D,$4D,$3D,$5D,$3D,$53,$3D,$FE,$3C,$56,$3D
db $4E,$3D,$51,$3D,$51,$3D,$58,$3D,$FE,$3C,$40,$3D,$41,$3D,$4E,$3D
db $54,$3D,$53,$3D,$FE,$3C,$53,$3D,$47,$3D,$40,$3D,$53,$3D,$FE,$3C
db $46,$3D,$54,$3D,$58,$3D,$1B,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$07,$3D,$44,$3D,$5D,$3D,$52,$3D,$FE,$3C,$41,$3D,$44,$3D
db $48,$3D,$4D,$3D,$46,$3D,$FE,$3C,$4E,$3D,$55,$3D,$44,$3D,$51,$3D
db $43,$3D,$51,$3D,$40,$3D,$4C,$3D,$40,$3D,$53,$3D,$48,$3D,$42,$3D
db $1B,$3D,$FE,$3C,$13,$3D,$47,$3D,$40,$3D,$53,$3D,$FE,$3C,$FE,$3C
db $FE,$3C,$56,$3D,$40,$3D,$52,$3D,$FE,$3C,$4E,$3D,$4D,$3D,$4B,$3D
db $58,$3D,$FE,$3C,$53,$3D,$47,$3D,$44,$3D,$FE,$3C,$52,$3D,$4E,$3D
db $54,$3D,$4D,$3D,$43,$3D,$FE,$3C,$4E,$3D,$45,$3D,$FE,$3C,$53,$3D
db $47,$3D,$44,$3D,$FE,$3C,$51,$3D,$4E,$3D,$4E,$3D,$4C,$3D,$FE,$3C
db $FE,$3C,$51,$3D,$44,$3D,$52,$3D,$44,$3D,$53,$3D,$53,$3D,$48,$3D
db $4D,$3D,$46,$3D,$FE,$3C,$48,$3D,$53,$3D,$52,$3D,$44,$3D,$4B,$3D
db $45,$3D,$1B,$3D,$FE,$3C,$0D,$3D,$4E,$3D,$53,$3D,$FE,$3C,$52,$3D
db $54,$3D,$51,$3D,$44,$3D,$FE,$3C,$56,$3D,$47,$3D,$58,$3D,$FE,$3C
db $FE,$3C,$48,$3D,$53,$3D,$FE,$3C,$43,$3D,$4E,$3D,$44,$3D,$52,$3D
db $FE,$3C,$53,$3D,$47,$3D,$40,$3D,$53,$3D,$1D,$3D,$FE,$3C,$53,$3D
db $47,$3D,$4E,$3D,$54,$3D,$46,$3D,$47,$3D,$1B,$3D,$1B,$3D,$1B,$3D
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C