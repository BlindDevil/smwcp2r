;Incineration Station/Perilous Paths ASM (levels 12C and 183).
;Includes:
;load: SMWC coin trigger initializer
;init/nmi: enable and process layer 2 parallax
;nmi: process layer 2 parallax

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL StonksParallax_init

nmi:
JML StonksParallax_nmi