;Countdown Chamber ASM (level B1).
;Includes:
;> DMA to write message for message box.

init:
REP #$20			;16-bit A
LDA #$0140			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA2>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #MsgDMA2			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CC0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA
RTL				;return.

MsgDMA2:
db $FE,$28,$13,$29,$47,$29,$44,$29,$FE,$28,$46,$2D,$51,$2D,$44,$2D
db $44,$2D,$4D,$2D,$FE,$2C,$42,$2D,$4E,$2D,$48,$2D,$4D,$2D,$52,$2D
db $FE,$28,$48,$29,$4D,$29,$FE,$28,$53,$29,$47,$29,$44,$29,$FE,$28
db $4D,$29,$44,$29,$57,$29,$53,$29,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$40,$29,$51,$29,$44,$29,$40,$29,$FE,$28,$56,$29,$48,$29
db $4B,$29,$4B,$29,$FE,$28,$48,$29,$4D,$29,$42,$29,$51,$29,$44,$29
db $40,$29,$52,$29,$44,$29,$FE,$28,$53,$29,$47,$29,$44,$29,$FE,$28
db $40,$29,$4C,$29,$4E,$29,$54,$29,$4D,$29,$53,$29,$FE,$28,$FE,$28
db $FE,$28,$4E,$29,$45,$29,$FE,$28,$53,$2D,$48,$2D,$4C,$2D,$44,$2D
db $FE,$2C,$51,$2D,$44,$2D,$4C,$2D,$40,$2D,$48,$2D,$4D,$2D,$48,$2D
db $4D,$2D,$46,$2D,$FE,$28,$4E,$29,$4D,$29,$FE,$28,$53,$29,$47,$29
db $44,$29,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28
db $FE,$28,$42,$29,$4B,$29,$4E,$29,$42,$29,$4A,$29,$1B,$29,$FE,$28
db $08,$29,$45,$29,$FE,$28,$58,$29,$4E,$29,$54,$29,$FE,$28,$41,$29
db $44,$29,$40,$29,$53,$29,$FE,$28,$53,$29,$47,$29,$44,$29,$FE,$28
db $42,$29,$4B,$29,$4E,$29,$42,$29,$4A,$29,$1D,$29,$FE,$28,$FE,$28
db $FE,$28,$58,$2D,$4E,$2D,$54,$2D,$5D,$2D,$4B,$2D,$4B,$2D,$FE,$2C
db $56,$2D,$48,$2D,$4D,$2D,$FE,$2C,$52,$2D,$4E,$2D,$4C,$2D,$44,$2D
db $53,$2D,$47,$2D,$48,$2D,$4D,$2D,$46,$2D,$FE,$2C,$46,$2D,$4E,$2D
db $4E,$2D,$43,$2D,$1A,$29,$FE,$28,$FE,$28,$FE,$28,$FE,$28,$FE,$28