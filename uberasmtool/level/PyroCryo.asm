;Pyro-Cryo Castle ASM.
;Includes
;load/init/main: back area color subtraction for all layers, on/off palette animations handling, set up main/subscreen hdma
;main: slippery blocks handling
;nmi: handle main/subscreen hdma for message boxes

!BackAreaIndex = $0F08|!addr

load:
init:
REP #$20
LDA #$2C03			;mode 3 on $212C (a.k.a. affect main/subscreen)
STA $4350
LDA.w #Table			;address of table
STA $4352
SEP #$20
LDA.b #Table>>16		;bank of table
STA $4354
LDA #$20		;load value
TRB $0D9F|!addr		;disable hdma on this channel.

LDA #$17		;\ Everything
STA $212C		; | is on main screen
STZ $212D		;/ Nothing is on sub screen

LDA $141A|!addr		;load times player has changed between sublevels
BNE docolorstuff	;if not zero, skip ahead.

LDA #$00		;load value
STA $7FC074		;store to manual trigger 4.
STZ !BackAreaIndex	;reset back area color index.
BRA docolorstuff	;branch ahead

nmi:
LDA $1B89|!addr		;load message box timer (now used as a pointer)
CMP #$03		;check if on displaying mode
BEQ +			;if not, skip ahead.

LDA #$20		;load value
TRB $0D9F|!addr		;disable hdma on this channel.
LDA #$17		;\ Everything
STA $212C		; | is on main screen
STZ $212D		;/ Nothing is on sub screen
RTL

+
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 7
RTL

Table:
db $7F,$17,$17,$17,$00
db $28,$17,$17,$17,$00
db $01,$04,$04,$00,$00
db $00

main:
LDA $71			;load player animation index
ORA $9D			;OR with sprites/animations locked flag
ORA $13D4|!addr		;OR with pause flag
BNE +			;if any are set, skip ahead.

LDA $14			;load effective frame counter
AND #$07		;preserve bits 0-2
BNE +			;if any are set, skip ahead.

docolorstuff:
JSR colorshenanigans	;handle back area color, color math and manual trigger exanimations depending on the on/off status

+
JML SlipperyBlocks_main	;call slippery blocks handling routine and return.

colorshenanigans:
LDA !BackAreaIndex	;load back area table index
ASL			;multiply by 2
TAX			;transfer to X
REP #$20		;16-bit A
LDA ColorTable,x	;load color value from table according to index
STA $0701|!addr		;store to back area color.
SEP #$20		;8-bit A
LDA !BackAreaIndex	;load back area table index again
LSR #2			;divide by 2 twice
TAX			;transfer to X
LDA ColorMath,x		;load CGADSUB value from table according to index
STA $40			;store to CGADSUB mirror.

LDA $14AF|!addr		;load on/off status
BNE .switchoff		;if off, branch.

LDA !BackAreaIndex	;load index for back area color table
BEQ +			;if zero, skip ahead.

DEC !BackAreaIndex	;decrement back area index by one

+
LDA $7FC074		;load manual trigger 4
BEQ +			;if equal, skip ahead.

DEC			;decrement A by one
BRA .storeman4		;branch ahead

+
RTS			;return.

.switchoff
LDA !BackAreaIndex	;load index for back area color table
CMP #$07		;compare to value
BEQ +			;if equal, skip ahead.

INC !BackAreaIndex	;decrement back area index by one

+
LDA $7FC074		;load manual trigger 4
CMP #$07		;compare to value
BEQ +			;if equal, skip ahead.

INC			;increment A by one

.storeman4
STA $7FC074		;store result back.

+
RTS			;return.

ColorTable:
dw $084D,$0429,$0424,$0000,$0022,$0045,$0047,$0069	;if add for fire and sub for ice
;dw $6D80,$5D62,$4D43,$3D25,$3126,$2108,$10EA,$00CB	;if sub for both fire and ice

ColorMath:
db $27,$A7	;1st: fire (add), 2nd: ice (sub)

!colortester = 0

;A small code I wrote so I could change RGB values for the back area color on the fly.
if !colortester
lda $13d4
bne +
rts

+
lda $16
and #$20
beq +

lda $40
eor #$80
sta $40

+
lda $15
and #$03
beq .noleftright
cmp #$02
beq .left

lda $0f10
cmp #$1f
beq .noleftright
inc $0f10
bra .noleftright

.left
lda $0f10
beq .noleftright
dec $0f10

.noleftright
lda $15
and #$0C
beq .noupdown
cmp #$08
beq .up

lda $0f11
cmp #$1f
beq .noupdown
inc $0f11
bra .noupdown

.up
lda $0f11
beq .noupdown
dec $0f11

.noupdown
lda $17
and #$30
beq .nolr
cmp #$20
beq .l

lda $0f12
cmp #$1f
beq .nolr
inc $0f12
bra .nolr

.l
lda $0f12
beq .nolr
dec $0f12

.nolr
rep #$20
lda $0f10
and #$001f
sta $00
lda $0f11
and #$001f
asl #5
ora $00
sta $00
lda $0f12
and #$001f
asl #10
ora $00
sta $0701
sep #$20
rts
endif