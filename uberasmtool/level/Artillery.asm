;Artillery Auberge/Radiatus Ruins/Asteroid Antics (level 13A) ASM.
;Includes:
;load: SMWC coin custom trigger init
;init: message box text DMA, 2-channel HDMA gradient
;main: kill springboards on level 13D that are below screen 02 and when mario is over screen 02, to kill slowdown

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58C0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

REP #$20			;16-bit A
LDA.w #MsgDMA2			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CC0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

main:
LDA $010B
CMP #$3D
BNE NoKillSprings

LDA $97
CMP #$02
BCS NoKillSprings

LDX #$0B

.loop
LDA $14C8,x
BEQ .void
LDA $9E,x
CMP #$2F
BNE .void
LDA $14D4,x
CMP #$03
BCC .void

STZ $14C8,x

.void
DEX
BPL .loop

NoKillSprings:
RTL

Table1:
db $0B,$2D,$55
db $17,$2D,$56
db $03,$2E,$56
db $0F,$2E,$57
db $0C,$2F,$58
db $02,$2F,$59
db $0A,$30,$59
db $03,$30,$5A
db $08,$31,$5A
db $05,$31,$5B
db $07,$32,$5B
db $06,$32,$5C
db $08,$33,$5C
db $09,$33,$5D
db $08,$34,$5D
db $1F,$34,$5E
db $0B,$35,$5E
db $07,$36,$5E
db $06,$37,$5E
db $06,$38,$5E
db $02,$39,$5E
db $06,$39,$5F
db $19,$3A,$5F
db $00

Table2:
db $80,$9E
db $41,$9E
db $1F,$9F
db $00

MsgDMA2:
incbin "msgbin/11C-1msg.bin":0-200

MsgDMA1:
incbin "msgbin/11C-2msg.bin":0-200