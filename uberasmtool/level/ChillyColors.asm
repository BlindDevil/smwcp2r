;Chilly Colors ASM.
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient
;main: change on/off status on jumping/spinjumping (spinjumping only if on level 129)

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

main:
lda $7C
beq .not_inc_ram
	inc
	sta $7C
	cmp.b #$04
	bne +
		stz $7C
	+
.not_inc_ram

lda $75
bne .return

LDA $79
bne .check_ground

LDA $7D
BMI +
	stz $7C
	lda #$01
	sta $79
	RTL

+
LDA $010B
CMP #$29
BNE .alljumps

LDA $140D
BEQ .return

.alljumps
inc $7C
inc $79
LDA $14AF
EOR #$01
STA $14AF
LDA #$0B
STA $1DF9
RTL

.check_ground
LDA $77
AND #$04
BEQ .return
stz $79
.return
RTL