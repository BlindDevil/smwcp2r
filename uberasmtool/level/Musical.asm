;Musical Mechanisms ASM.
;Includes:
;load: SMWC coin trigger initializer
;init/nmi: enable and process layer 2 parallax
;main: handle manual animations depending on song beat, disable L/R scrolling

;cheatsheet:
;$0F5E holds song ticks in 1/192 (this sucks, it's a multiple of 3)
;(note that values are approximate because snes math)
;#$00 = 1/4
;#$32 = 2/4
;#$64 = 3/4
;#$96 = 4/4
;#$BF = last tick before it loops back to #$00

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
JSL StonksParallax_init
nmi:
JML StonksParallax_nmi

main:
STZ $1401|!addr		;reset L/R time to scroll the screen.

LDA $9D			;load sprites/animation locked flag
ORA $13D4|!addr		;OR with pause flag
BNE +			;if any are set, skip ahead.

LDA $18			;load controller data 2, first frame only
AND #$30		;check if L or R are pressed
BEQ +			;if not pressed, skip ahead.

LDA #$2A		;load SFX value
STA $1DFC|!addr		;store to address to play it.

+
;get beat in 4/4 by kipernalsux
REP #$20
LDA $7FB004
STA $4204
SEP #$20
LDA #$C0
STA $4206
NOP #8
LDA $4216
STA $0F5E|!addr

CMP #$05		;compare to value
BCC .checkdecrementer	;if lower, check decrementer to flip on/off switch status (pause workaround tentative).
CMP #$BD		;compare to value
BCC +			;if lower, skip ahead.

.checkdecrementer
LDA $14A8|!addr		;load decrementer
BNE +			;if not zero, skip ahead.

LDA $14AF|!addr		;load on/off status
EOR #$01		;flip bit
STA $14AF|!addr		;store result back.
LDA #$0C		;load value
STA $14A8|!addr		;store to decrementer.

+
JSR NumbersExAn		;exanimate some stuff (manual triggers 5 and 6, one shot trigger 0)
RTL			;return.

NumbersExAn:
LDX $14AF|!addr		;load on/off status into X
LDA $0F5E|!addr		;load song ticks
LSR #4			;divide by 2 four times
STA $7FC075		;store to manual trigger 5.
CLC			;clear carry
ADC OnOffOffset,x	;add offset from table according to index
STA $7FC076		;store to manual trigger 6.

;LDA $9D			;load sprites/animation locked flag
;ORA $13D4|!addr		;OR with pause flag
;BNE .novalve		;if any are set, don't trigger the one shot animation.

LDA $0F5E|!addr		;load song ticks again
CMP #$03		;compare to value
BCS .novalve		;if higher or equal, don't trigger the one shot animation.

LDA #$01		;load value
STA $7FC0F8		;store to one shot triggers address 1.

.novalve
RTS			;return.

OnOffOffset:
db $00,$0C		;on, off