;Akarui Avenue ASM (level 1C0).
;Includes:
;init: message box text DMA, set a timer to teleport
;main: handle custom timer, teleport player when custom timer is zero

init:
LDA #$26			;load value
STA $0F5E|!addr			;store to address.

REP #$20			;16-bit A
LDA #$0140			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA.w #MsgDMA1			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$58C0			;load VRAM destination
STA $0A				;store to scratch RAM.
JML DMA_UseNoIndex		;execute DMA and return

main:
LDA $0F5E|!addr			;load address
BEQ .tele			;if equal zero, teleport.

LDA $9D				;load sprites/animations blocked flag
ORA $13D4|!addr			;OR with game paused flag
BNE .ret			;if any are set, return.

DEC $0F5E|!addr			;decrement address by one

.ret
RTL				;return.

.tele
LDA #$06
STA $71
STZ $89
STZ $88
RTL				;return.

MsgDMA1:
db $FE,$38,$0E,$39,$47,$39,$1D,$39,$FE,$38,$43,$39,$44,$39,$40,$39
db $51,$39,$1B,$39,$1B,$39,$1B,$39,$FE,$38,$48,$39,$53,$39,$5D,$39
db $52,$39,$FE,$38,$53,$39,$4E,$39,$4E,$39,$FE,$38,$43,$39,$40,$39
db $51,$39,$4A,$39,$FE,$38,$48,$39,$4D,$39,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$47,$39,$44,$39,$51,$39,$44,$39,$1B,$39,$FE,$38,$06,$39
db $4E,$39,$FE,$38,$41,$39,$40,$39,$42,$39,$4A,$39,$FE,$38,$4E,$39
db $54,$39,$53,$39,$52,$39,$48,$39,$43,$39,$44,$39,$FE,$38,$1C,$39
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$4F,$39,$44,$39,$51,$39,$47,$39,$40,$39,$4F,$39,$52,$39
db $FE,$38,$58,$39,$4E,$39,$54,$39,$FE,$38,$42,$39,$40,$39,$4D,$39
db $FE,$38,$45,$39,$48,$39,$4D,$39,$43,$39,$FE,$38,$52,$3D,$4E,$3D
db $4C,$3D,$44,$3D,$FE,$38,$52,$3D,$4E,$3D,$51,$3D,$53,$3D,$FE,$38
db $FE,$38,$4E,$3D,$45,$3D,$FE,$38,$4B,$3D,$40,$3D,$4D,$3D,$53,$3D
db $44,$3D,$51,$3D,$4D,$3D,$FE,$38,$56,$39,$47,$39,$48,$39,$42,$39
db $47,$39,$FE,$38,$56,$39,$48,$39,$4B,$39,$4B,$39,$FE,$38,$41,$39
db $51,$39,$48,$39,$46,$39,$47,$39,$53,$39,$44,$39,$4D,$39,$FE,$38
db $FE,$38,$53,$39,$47,$39,$48,$39,$52,$39,$FE,$38,$4F,$39,$4B,$39
db $40,$39,$42,$39,$44,$39,$1B,$39,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38
db $FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38,$FE,$38