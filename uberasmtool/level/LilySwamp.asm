;Lily Swamp Romp ASM.
;Includes:
;init: reorder layers on main/subscreen, shift layer 2 down by two pixels, message box text DMA
;main: make the player stand still relative to layer 2 when stepping on it, make water toxic
;nmi: display warning sprite tile if mario stays on swamp water for too long

!inittimer = $80		;amount of frames that mario can stay in water
!alertwhen = $50		;when the timer value is lower than this, an exclamation mark will appear near mario
!timer = $14A8|!addr		;unused decrementer address (for a slower one, use $14AC instead)

init:
LDA #$02			;load bit value for layer 2
STA $212C			;store to mainscreen register.
LDA #$17			;load bit value for everything, even layer 2 and sprites, too
STA $212D			;store to subscreen register.

REP #$20			;16-bit A
LDA #$00BE			;load value
STA $20				;store to layer 2 Y-pos current frame.
STA $1468|!addr			;store to layer 2 Y-pos next frame.

LDA #$0140			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58C0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA (A goes 8-bit after that)

resettimer:
LDA #!inittimer			;load value
STA !timer			;store to timer.
RTL				;return.

main:
JSL MovePlayerAlongL2_run	;make the player stand still relative to layer 2

LDA $75			;load player is in water flag
BEQ resettimer		;if not in water, then reset the timer.

LDA !timer		;load timer
BNE .ret		;if not equal zero, don't hurt the player.

JSL $00F5B7|!bank	;hurt the player

.ret
RTL			;return.

nmi:
LDA $71			;load player animation pointer
BNE .ret		;if not equal zero, don't display warning indicator.

LDA !timer		;load timer
CMP #!alertwhen		;compare to value
BCS .ret		;if higher, don't display warning indicator.

;draw exclamation mark (now I've copied this from KKevinM's code, just did small changes)
LDX #$00	;\ If Mario is big, we'll use a different y offset.
LDA $19		;|
BEQ +		;|
INX		;/
+
LDY $76		;\ Get the X offset based on Mario's direction.
LDA XOffset,y   ;/
CLC : ADC $7E   ; Sum with Mario's X position.
STA $0200|!addr ;
LDA YOffset,x   ; Get the Y offset based on Mario's size.
CLC : ADC $80   ; Sum with Mario's Y position.
STA $0201|!addr ;
LDA #$1D	; <<< Tile number!
STA $0202|!addr ;
LDA #$28	; <<< Tile properties!
STA $0203|!addr ;
STZ $0420|!addr ; (8x8 tile)

.ret
RTL

XOffset:
    db $F8,$10

YOffset:
    db $0E,$04

MsgDMA1:
db $1F,$2D,$16,$2D,$40,$2D,$53,$2D,$42,$2D,$47,$2D,$1F,$2D,$4E,$2D
db $54,$2D,$53,$2D,$1F,$2D,$45,$2D,$4E,$2D,$51,$2D,$1F,$2D,$53,$2D
db $47,$2D,$44,$2D,$1F,$2D,$48,$29,$42,$29,$4A,$29,$58,$29,$1F,$2D
db $56,$29,$40,$29,$53,$29,$44,$29,$51,$29,$1A,$2D,$1F,$2D,$1F,$2D
db $1F,$2D,$08,$2D,$45,$2D,$1F,$2D,$0C,$2D,$40,$2D,$51,$2D,$48,$2D
db $4E,$2D,$1F,$2D,$52,$2D,$53,$2D,$40,$2D,$58,$2D,$52,$2D,$1F,$2D
db $45,$2D,$4E,$2D,$51,$2D,$1F,$2D,$53,$2D,$4E,$2D,$4E,$2D,$1F,$2D
db $4B,$2D,$4E,$2D,$4D,$2D,$46,$2D,$1F,$2D,$48,$2D,$4D,$2D,$1F,$2D
db $1F,$2D,$48,$2D,$53,$2D,$1D,$2D,$1F,$2D,$47,$29,$44,$29,$5D,$29
db $4B,$29,$4B,$29,$1F,$2D,$52,$29,$54,$29,$51,$29,$44,$29,$4B,$29
db $58,$29,$1F,$2D,$46,$29,$44,$29,$53,$29,$1F,$2D,$52,$29,$48,$29
db $42,$29,$4A,$29,$1A,$2D,$1F,$2D,$13,$2D,$51,$2D,$58,$2D,$1F,$2D
db $1F,$2D,$53,$2D,$40,$2D,$4A,$2D,$48,$2D,$4D,$2D,$46,$2D,$1F,$2D
db $53,$2D,$47,$2D,$44,$2D,$1F,$2D,$4B,$2D,$48,$2D,$4B,$2D,$58,$2D
db $1F,$2D,$4F,$2D,$40,$2D,$43,$2D,$52,$2D,$1F,$2D,$48,$2D,$4D,$2D
db $52,$2D,$53,$2D,$44,$2D,$40,$2D,$43,$2D,$1A,$2D,$1F,$2D,$1F,$2D
db $1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D
db $1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D
db $1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D
db $1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D,$1F,$2D