;Bedazzling Bastion ASM (levels 1B0 and 1B1).
;Includes:
;init: message box text DMA

init:
REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.
LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58A0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA
REP #$20			;16-bit A
LDA.w #MsgDMA2			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CA0			;load VRAM destination
STA $0A				;store to scratch RAM.
JML DMA_UseNoIndex		;execute DMA and return.

MsgDMA1:
incbin "msgbin/139-1msg2msg.bin":0-200

MsgDMA2:
incbin "msgbin/139-1msg2msg.bin":200-400