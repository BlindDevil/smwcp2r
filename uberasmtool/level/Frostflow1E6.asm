;Frostflow Freezer ASM (level 1E6). The most evil ASM ever.
;Includes:
;load: SMWC coin custom trigger init
;init: set up snowball stuff, set up correct initial position for mario (ice skates)
;main: handle snowball stuff... oh my fucking gosh.

!SnowballXOff = $0F5E|!addr
!SnowballYOff = $0F60|!addr
!SnowballAction = $0F62|!addr
!SnowballAccIndex = $0F63|!addr

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA $76		;load player direction
STA $0F0B	;store to ice skates direction preserver.

STZ !SnowballAction

STZ $212D	;hide layer 2

REP #$20
LDA #$01B8
STA !SnowballXOff
LDA #$0020
STA !SnowballYOff

nmi:
REP #$20
LDA $1462|!addr
CLC
ADC !SnowballXOff
STA $1466|!addr

LDA $1464|!addr
CLC
ADC !SnowballYOff
STA $1468|!addr
SEP #$20
RTL

main:
LDA !SnowballAction
BNE +

LDA $1B
CMP #$0E
BCS .displayL2

.hideforever
STZ $212D	;hide layer 2
RTL

.displayL2
LDA #$02
STA $212D	;show layer 2
BRA .ahead		;branch ahead

+
CMP #$03
BEQ .hideforever

LDA $1F			;load layer 2 X-pos current frame high byte
CMP #$14		;compare to value
BCC .hide		;if lower, hide layer 2.
CMP #$16		;compare again
BCC .displayL2		;if lower, display layer 2.

.hide
STZ $212D		;clear subscreen, hide layer 2.

.ahead
LDA $71
ORA $9D
ORA $13D4|!addr
BNE .noproc

LDA !SnowballAccIndex
BEQ +

DEC !SnowballAccIndex

+
JSR HandleSnowball

.noproc
RTL

HandleSnowball:
LDA !SnowballAccIndex	;load decrementer
LSR #4
ASL			;multiply by 2
TAX			;transfer to X
REP #$20		;16-bit A
LDA SnowballAccel,x	;load value from table according to index
STA $00			;store to scratch RAM.
SEP #$20		;8-bit A

LDA !SnowballAction	;load snowball action/behavior
BNE +			;if not zero, branch ahead.

REP #$20		;16-bit A
LDA $94			;load player's X-pos within the level
AND #$FFF0		;preserve these bits
CMP #$13B0		;compare to value
BNE .notrigger		;if not equal, don't trigger snowball stuff.

SEP #$20		;8-bit A
INC !SnowballAction	;increment snowball action value
LDA #$21		;load SFX value
STA $1DF9|!addr		;store to address to play it.
LDA #$A0		;load value
STA $1887|!addr		;store to layer 1 shake timer.
LDA #$F0		;load value
STA !SnowballAccIndex	;store to decrementer. our index for the snowball accel table.

.notrigger
SEP #$20		;8-bit A
RTS			;return.

+
DEC			;decrement A
BNE ++			;if action value isn't equal #$01, branch ahead.

REP #$20		;16-bit A
LDA !SnowballYOff	;load snowball Y offset
CMP #$0120		;compare to value
BEQ .doneYoff		;if equal, stop updating it.

CLC			;clear carry
ADC #$0010		;add value
STA !SnowballYOff	;store result back.

.doneYoff
LDA !SnowballXOff	;load snowball X offset
SEC			;set carry
SBC $00			;subtract value from scratch RAM
STA !SnowballXOff	;store result back.
AND #$FFF0		;preserve these bits
CMP #$F6C0		;compare to value
BNE +			;if not equal, keep shattering blocks.

SEP #$20		;8-bit A
INC !SnowballAction	;increment snowball action value
LDA #$70		;load value
STA !SnowballAccIndex	;store to decrementer. our index for the snowball accel table.
RTS

+
SEP #$20		;8-bit A

BRA ShatterBlocks	;shitbrix-- oops, shatter blocks

++
DEC			;decrement A
BNE +			;if action value isn't equal #$02, return.

LDA !SnowballAccIndex	;load decrementer
LSR #4
AND #$07
ASL			;multiply by 2
TAX			;transfer to X
REP #$20		;16-bit A
LDA !SnowballYOff	;load snowball Y offset
SEC			;set carry
SBC SnowballFall,x	;subtract value from table according to index
STA !SnowballYOff	;store result back.
CMP #$0060		;compare to value
BCS .stillfalling	;if higher, keep snowball falling.
SEP #$20		;8-bit A

INC !SnowballAction	;increment snowball action value
RTS

.stillfalling
LDA !SnowballXOff	;load snowball X offset
SEC			;set carry
SBC SnowballDec,x	;subtract value from table according to index
STA !SnowballXOff	;store result back.
SEP #$20		;8-bit A

+
RTS

SnowballAccel:
dw $0003,$0003,$0003,$0003,$0003,$0003,$0003,$0004
dw $0005,$0006,$0007,$0008,$0008,$0008,$FFF0,$FFF0

SnowballFall:
dw $0007,$0006,$0005,$0004,$0003,$0002,$0001,$0001

SnowballDec:
dw $0000,$0000,$0000,$0000,$0000,$0001,$0001,$0002

ShatterBlocks:
	LDA $14
	AND #$01
	ASL A
	TAX
.ShatterBlocksLoop
	REP #$20
	LDA $98
	PHA
	LDA $9A
	PHA

	LDA !SnowballXOff
	EOR #$FFFF
	CLC
	ADC #$1540
	STA $9A
	STA $7F8888
	LDA .ShatterYOffset,x
	STA $98

	SEP #$20
	PHY		;preserve map16 high

	LDA $14
	AND #$03
	BNE .SkipShatter

	PHB		;need to change bank
	LDA #$82
	PHA
	PLB		;to 02
	LDA #$00	;default shatter
	JSL $828663	;shatter block
	PLB		;restore bank

.SkipShatter
	LDA #$02
	STA $9C
	JSL $80BEB0
	PLY

	REP #$20
	PLA
	STA $9A
	PLA
	STA $98
	SEP #$20
	RTS

.ShatterYOffset
dw $0160,$0170