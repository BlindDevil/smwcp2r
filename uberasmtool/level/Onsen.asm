;Onsen Overhang ASM.
;Includes:
;load: disable layer 2 vertical scroll, SMWC coin trigger initializer
;init: message box text DMA except for sublevel 34, 2-channel HDMA gradient, parallax X offset processing
;nmi: very slow BG V-scroll relative to layer 1
;main: parallax X offset processing, water kills the player on sublevel 4E

load:
JSL Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

LDA $1464|!addr			;load layer 1 Y-pos, next frame
STA $1C				;store to layer 1 Y-pos, current frame.
STZ $1414|!addr			;set BG Y-scroll in LM to none.
STZ $1E
STZ $1466|!addr
JSL OrientParallax_Logic	;handle layer 2 parallax logic
JSL OrientParallax_ProcXOffsets	;handle X position update for layer 2 parallax
BRA nmi

init:
JSL OrientParallax_Logic	;handle layer 2 parallax logic
JSL OrientParallax_HDMA		;execute HDMA

LDA $010B|!addr			;load level number, low byte
CMP #$34			;compare to value
BEQ skip			;if equal, skip ahead.

REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA1>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA.w #MsgDMA1			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$58A0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

REP #$20			;16-bit A
LDA.w #MsgDMA2			;load absolute address of text tilemap table 2
STA $07				;store to scratch RAM.
LDA #$5CA0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

LDA $010B|!addr			;load level number, low byte
CMP #$14			;compare to value
BNE +				;if not equal, skip ahead.

REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
BRA ++				;branch ahead

+
REP #$20			;16-bit A
LDA.w #Table5			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table6			;load address of second table
BRA ++				;branch ahead

skip:
REP #$20			;16-bit A
LDA.w #Table3			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table4			;load address of second table
BRA ++				;branch ahead

++
STA $03				;store to scratch RAM.
LDX.b #Table1>>16		;load bank of one of the tables
STX $02				;store to scratch RAM.
STX $05				;store to scratch RAM.
REP #$20			;16-bit A
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)
SEP #$20			;8-bit A

nmi:
JML OrientParallax_NMIBGUpd	;update layer 2 Y-pos manually

main:
LDA $010B|!addr			;load sublevel number low byte
CMP #$4E			;compare to value
BNE +				;if not equal, skip ahead.

LDA $75				;load player is in water flag
BEQ +				;if not in water, skip ahead.

STZ $19				;remove player's powerup
JSL $00F5B7|!bank		;hurt the player (and it's a death).

+
JSL OrientParallax_ProcXOffsets	;handle X position update for layer 2 parallax
JML OrientParallax_Logic	;handle layer 2 parallax logic and return.

MsgDMA1:
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$13,$31,$47,$31,$44,$31,$FE,$30,$56,$31,$40,$31,$53,$31
db $44,$31,$51,$31,$FE,$30,$47,$31,$44,$31,$51,$31,$44,$31,$FE,$30
db $48,$31,$52,$31,$FE,$30,$4F,$31,$4E,$31,$4B,$31,$4B,$31,$54,$31
db $53,$31,$44,$31,$43,$31,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$41,$31,$44,$31,$42,$31,$40,$31,$54,$31,$52,$31,$44,$31
db $FE,$30,$4E,$31,$45,$31,$FE,$30,$0D,$31,$4E,$31,$51,$31,$55,$31
db $44,$31,$46,$31,$5D,$31,$52,$31,$FE,$30,$56,$31,$51,$31,$44,$31
db $53,$31,$42,$31,$47,$31,$44,$31,$43,$31,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$4C,$31,$40,$31,$42,$31,$47,$31,$48,$31,$4D,$31,$44,$31
db $52,$31,$1B,$31,$FE,$30,$0D,$35,$4E,$35,$56,$35,$FE,$30,$58,$35
db $4E,$35,$54,$35,$FE,$30,$42,$35,$40,$35,$4D,$35,$5D,$35,$53,$35
db $FE,$30,$44,$35,$55,$35,$44,$35,$4D,$35,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$52,$35,$56,$35,$48,$35,$4C,$35,$FE,$30,$56,$35,$48,$35
db $53,$35,$47,$35,$4E,$35,$54,$35,$53,$35,$FE,$30,$46,$35,$44,$35
db $53,$35,$53,$35,$48,$35,$4D,$35,$46,$35,$FE,$30,$4A,$35,$48,$35
db $4B,$35,$4B,$35,$44,$35,$41,$75,$1A,$31,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30

MsgDMA2:
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$16,$31,$47,$31,$4E,$31,$40,$31,$1D,$31,$FE,$30,$53,$31
db $47,$31,$44,$31,$FE,$30,$47,$35,$4E,$35,$53,$35,$FE,$30,$52,$35
db $4F,$35,$51,$35,$48,$35,$4D,$35,$46,$35,$52,$35,$FE,$30,$40,$31
db $4C,$31,$4E,$31,$4D,$31,$46,$31,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$53,$31,$47,$31,$44,$31,$52,$31,$44,$31,$FE,$30,$4C,$31
db $4E,$31,$54,$31,$4D,$31,$53,$31,$40,$31,$48,$31,$4D,$31,$52,$31
db $FE,$30,$40,$31,$51,$31,$44,$31,$FE,$30,$51,$31,$44,$31,$40,$31
db $4B,$31,$4B,$31,$58,$31,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$56,$31,$40,$31,$51,$31,$4C,$31,$1A,$31,$FE,$30,$03,$35
db $4E,$35,$4D,$35,$5D,$35,$53,$35,$FE,$30,$52,$35,$53,$35,$40,$35
db $58,$35,$FE,$30,$48,$35,$4D,$35,$FE,$30,$45,$35,$4E,$35,$51,$35
db $FE,$30,$53,$35,$4E,$35,$4E,$35,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$4B,$35,$4E,$35,$4D,$35,$46,$35,$1D,$31,$FE,$30,$4E,$31
db $51,$31,$FE,$30,$44,$31,$4B,$31,$52,$31,$44,$31,$FE,$30,$58,$31
db $4E,$31,$54,$31,$FE,$30,$56,$31,$48,$31,$4B,$31,$4B,$31,$FE,$30
db $41,$31,$54,$31,$51,$31,$4D,$31,$1A,$31,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$00,$31,$4D,$31,$43,$31,$FE,$30,$56,$31,$44,$31,$FE,$30
db $43,$31,$4E,$31,$4D,$31,$5D,$31,$53,$31,$FE,$30,$56,$31,$40,$31
db $4D,$31,$53,$31,$FE,$30,$53,$31,$47,$31,$40,$31,$53,$31,$1D,$31
db $FE,$30,$51,$31,$48,$31,$46,$31,$47,$31,$53,$31,$1E,$31,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30
db $FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30,$FE,$30

Table1:
db $08,$2B,$42
db $02,$2C,$42
db $04,$2D,$43
db $03,$2E,$43
db $05,$2E,$44
db $03,$2F,$44
db $05,$2F,$45
db $03,$30,$45
db $04,$30,$46
db $04,$31,$46
db $04,$31,$47
db $05,$32,$47
db $03,$32,$48
db $05,$33,$48
db $02,$33,$49
db $07,$34,$49
db $01,$34,$4A
db $07,$35,$4A
db $06,$36,$4B
db $02,$36,$4C
db $04,$37,$4C
db $03,$37,$4D
db $03,$38,$4D
db $05,$38,$4E
db $06,$39,$4F
db $01,$39,$50
db $05,$3A,$50
db $02,$3A,$51
db $04,$3B,$51
db $04,$3B,$52
db $02,$3C,$52
db $05,$3C,$53
db $01,$3D,$53
db $06,$3D,$54
db $06,$3E,$55
db $02,$3E,$56
db $03,$3F,$56
db $4F,$3F,$57
db $00

Table2:
db $07,$8C
db $0D,$8D
db $0B,$8C
db $0C,$8B
db $0D,$8A
db $0E,$89
db $0D,$88
db $0B,$87
db $0C,$86
db $0C,$85
db $0B,$84
db $0C,$83
db $53,$82
db $00

Table3:
db $04,$24,$43
db $08,$25,$43
db $05,$26,$43
db $03,$26,$42
db $08,$27,$42
db $08,$28,$42
db $08,$29,$42
db $08,$2A,$42
db $04,$2B,$42
db $03,$2C,$42
db $05,$2D,$43
db $04,$2E,$43
db $04,$2E,$44
db $06,$2F,$44
db $03,$2F,$45
db $07,$30,$45
db $02,$30,$46
db $08,$31,$46
db $01,$31,$47
db $08,$32,$47
db $01,$33,$47
db $08,$33,$48
db $03,$34,$48
db $06,$34,$49
db $04,$35,$49
db $04,$35,$4A
db $07,$36,$4A
db $02,$36,$4B
db $52,$37,$4B
db $00

Table4:
db $19,$8B
db $1E,$8C
db $0D,$8D
db $0B,$8C
db $0B,$8B
db $0D,$8A
db $0D,$89
db $0E,$88
db $0F,$87
db $4F,$86
db $00

Table5:
db $0D,$20,$40
db $02,$20,$41
db $17,$21,$41
db $07,$21,$42
db $13,$22,$42
db $0A,$22,$43
db $0F,$23,$43
db $0F,$23,$44
db $0B,$24,$44
db $12,$24,$45
db $08,$25,$45
db $16,$25,$46
db $3D,$26,$46
db $00

Table6:
db $0D,$82
db $0E,$83
db $0F,$84
db $0E,$85
db $0E,$86
db $0F,$87
db $0E,$88
db $0F,$89
db $0E,$8A
db $0E,$8B
db $0F,$8C
db $43,$8D
db $00