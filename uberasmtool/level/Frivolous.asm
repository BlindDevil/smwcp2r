;Frivolous Fires ASM (level 24).
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

Table1:
db $02,$2E,$4A
db $02,$2F,$4A
db $02,$30,$4A
db $01,$30,$4B
db $02,$31,$4B
db $03,$32,$4B
db $02,$33,$4B
db $03,$34,$4B
db $02,$35,$4B
db $01,$36,$4B
db $02,$36,$4C
db $02,$37,$4C
db $05,$38,$4C
db $04,$39,$4C
db $03,$3A,$4C
db $01,$3A,$4D
db $05,$3B,$4D
db $03,$3C,$4E
db $01,$3C,$4F
db $02,$3D,$4F
db $02,$3D,$50
db $01,$3E,$50
db $03,$3E,$51
db $02,$3E,$52
db $01,$3F,$52
db $04,$3F,$53
db $03,$3F,$54
db $04,$3F,$55
db $03,$3F,$56
db $04,$3F,$57
db $04,$3F,$58
db $02,$3F,$59
db $01,$3E,$59
db $04,$3E,$5A
db $04,$3E,$5B
db $03,$3E,$5C
db $04,$3E,$5D
db $01,$3E,$5E
db $7E,$3D,$5E
db $00

Table2:
db $03,$92
db $04,$93
db $04,$94
db $05,$95
db $04,$96
db $04,$97
db $0F,$98
db $09,$97
db $05,$96
db $06,$95
db $05,$94
db $05,$93
db $05,$92
db $04,$91
db $05,$90
db $04,$8F
db $05,$8E
db $04,$8D
db $80,$8C
db $00