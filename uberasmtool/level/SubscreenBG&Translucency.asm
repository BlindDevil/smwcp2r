;Code that sends layer 2 to subscreen, and makes layer 3 transparency affect layers 1, 2 and sprites.
;Also works around keyhole windowing.

init:
LDA #$15		;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13		;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D
RTL

nmi:
LDA $1434|!addr		;load keyhole animation timer
BEQ ++			;if zero, return.
DEC			;decrement by one
BEQ +

LDX #$14		;load layers/sprites value
LDA #$03		;load layers/sprites value
BRA LOL			;branch ahead

+
LDX #$04		;load layers/sprites value
LDA #$13		;load layers/sprites value

LOL:
STX $212C		;store to mainscreen.
STA $212D		;store to subscreen.

++
RTL