;Fringe Forest ASM.
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient
;init and nmi: parallax HDMA
;main: parallax logic

!hdmaparallaxtbls = $7F9E00		;no need to change this, this address is nice lol
!parallax_autoscroll = $0F5E|!addr	;16-bit incrementing address for layer 3 clouds

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$00			;load value
STA !hdmaparallaxtbls+6		;store to parallax table 1 (layer 3).
LDA #$78			;load value
STA !hdmaparallaxtbls		;store to parallax table 1 (layer 3).
STA !hdmaparallaxtbls+3		;store to parallax table 1 (layer 3).

LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

nmi:
REP #$20			;kinda Variable 3 H-scroll (and also handles some parallax)
LDA !parallax_autoscroll
STA !hdmaparallaxtbls+1

LDA $1A
LSR
CLC
ADC $1A
LSR #3
STA $1E
STA $1466|!addr
STA !hdmaparallaxtbls+4

LDA #$00BA			;no V-scroll (fixed Y-pos)
STA $20
STA $1468|!addr
SEP #$20

REP #$20
LDA #$1102			;mode 2 on $2111 (a.k.a. affect layer 3 X-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5
RTL				;return.

main:
LDA $0100|!addr
CMP #$14
BNE +

LDA $14
AND #$07
ORA $9D
ORA $13D4|!addr
BNE +

REP #$20
INC !parallax_autoscroll
SEP #$20

+
RTL			;return.

Table1:
db $0A,$54,$9C
db $0C,$55,$9D
db $0B,$56,$9D
db $0B,$57,$9D
db $0C,$58,$9D
db $08,$59,$9D
db $03,$59,$9E
db $0C,$5A,$9E
db $0B,$5B,$9E
db $0B,$5C,$9E
db $0C,$5D,$9E
db $02,$5E,$9E
db $01,$5E,$9F
db $02,$5F,$9F
db $01,$5A,$9E
db $01,$53,$9C
db $05,$4D,$9B
db $03,$4E,$9B
db $03,$4E,$9C
db $06,$4F,$9C
db $06,$50,$9C
db $06,$51,$9C
db $06,$52,$9C
db $03,$53,$9C
db $03,$53,$9D
db $06,$54,$9D
db $06,$55,$9D
db $06,$56,$9D
db $06,$57,$9D
db $02,$58,$9D
db $03,$58,$9E
db $06,$59,$9E
db $07,$5A,$9E
db $08,$5B,$9E
db $07,$5C,$9E
db $02,$5D,$9E
db $05,$5D,$9F
db $00

Table2:
db $0A,$28
db $08,$29
db $07,$2A
db $08,$2B
db $07,$2C
db $08,$2D
db $07,$2E
db $07,$2F
db $08,$30
db $07,$31
db $08,$32
db $07,$33
db $07,$34
db $08,$35
db $07,$36
db $01,$37
db $01,$39
db $01,$3C
db $01,$3F
db $01,$36
db $01,$2D
db $04,$24
db $07,$25
db $06,$26
db $07,$27
db $06,$28
db $07,$29
db $06,$2A
db $06,$2B
db $07,$2C
db $06,$2D
db $07,$2E
db $06,$2F
db $0D,$30
db $11,$31
db $00