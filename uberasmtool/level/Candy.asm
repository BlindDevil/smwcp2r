;Candy Calamity ASM (levels 134 and 4D).
;Includes:
;load: SMWC coin custom trigger init
;init: 2-channel HDMA gradient

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JML HDMA_Color2			;execute HDMA (A goes 8-bit after this routine) and return.

Table1:
db $04,$2E,$42
db $04,$2E,$43
db $08,$2F,$43
db $06,$2F,$44
db $07,$30,$44
db $08,$30,$45
db $04,$31,$45
db $0A,$31,$46
db $02,$32,$46
db $0D,$32,$47
db $0C,$33,$48
db $02,$33,$49
db $0A,$34,$49
db $04,$34,$4A
db $09,$35,$4A
db $06,$35,$4B
db $06,$36,$4B
db $08,$36,$4C
db $04,$37,$4C
db $0B,$37,$4D
db $02,$38,$4D
db $0C,$38,$4E
db $0C,$39,$4F
db $02,$39,$50
db $0B,$3A,$50
db $04,$3A,$51
db $08,$3B,$51
db $06,$3B,$52
db $06,$3C,$52
db $09,$3C,$53
db $04,$3D,$53
db $0A,$3D,$54
db $00

Table2:
db $07,$92
db $11,$93
db $12,$94
db $11,$95
db $12,$96
db $11,$97
db $12,$98
db $11,$99
db $12,$9A
db $11,$9B
db $12,$9C
db $11,$9D
db $12,$9E
db $07,$9F
db $00