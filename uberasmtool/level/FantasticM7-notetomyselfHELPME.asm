;Fantastic Elastic ASM (level DD).
;Includes:
;init: set up all mode 7, 2-channel HDMA gradient
;main: handle mode 7 rotation and HDMA positioning

!Rotation = $0DC3
!Table1RAM = $7F9E00
!Table2RAM = $7F9E80

!FERRIS = $958000	;ferris wheel gfx location

init:
	LDA #$07			;\ Set BG Mode to 7.
	STA $3E				;/
	LDA #$80            ; \ Increase on $2119 write.
	STA $2115           ; /
	REP #$10
	LDX #$0000			; \ Set where to write in VRAM...
	STX $2116			; / ...and since Mode 7 is layer 1 we can just put $0000 here.
	LDA #$01            ;\ Set mode to...
	STA $4300           ;/ ...2 regs write once.
	LDA #$18            ;\ 
	STA $4301           ;/ Writing to $2118 AND $2119.
	LDX.w #!FERRIS       	;\  Adress where our data is.
	STX $4302          				 ; | 
	LDA.b #!FERRIS>>16  	 ; | Bank where our data is.
	STA $4304          				 ;/
	LDX #$8000          ;\ Size of our data.
	STX $4305           ;/
	SEP #$10
	LDA.b #$01	   ;\ Start DMA transfer on channel 0.
	STA.w $420B	   ;/

	stz $211a
	REP #$20
	LDA #$0080 : STA $2A	; \ Set effect center
	LDA #$0080 : STA $2C	; /
	LDA #$0100
	STA $36			; Set angle
	LDA #$0C0C : STA $38	; Set size
	LDA #$0008 : STA $3A	; \ Set layer position
	LDA #$FF98 : STA $3C	; /
	SEP #$20

	LDX.b #T1End-Table1-1

.loop1
	LDA Table1,x
	STA !Table1RAM,x
	DEX
	CPX #$FF
	BNE .loop1

	LDX.b #T2End-Table2-2

.loop2
	LDA Table2,x
	STA !Table2RAM,x
	DEX
	CPX #$FF
	BNE .loop2


LDA.b #!Table1RAM>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #!Table1RAM		;load address of first table
STA $00				;store to scratch RAM.
LDA.w #!Table2RAM		;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)
RTL

main:
LDA $71
CMP #$09
BEQ ++

LDA $77
AND #$04
BEQ +

REP #$20
LDA $96
CMP #$01F3
BCC +
CMP #$0338
BCS +

SEP #$20
LDA #$80
STA $1404	;force scroll at player

+
REP #$20
LDA $96
CMP #$01F2
BCS +++

++
SEP #$20
STZ $1412	;no vertical scroll
REP #$20

+++
LDA $1C
LSR #3
STA $00
SEP #$20

LDA #$5E
SEC
SBC $00
CLC
ADC #$09
STA !Table1RAM
STA !Table2RAM

	LDA $13D4
	ORA $9D
	BNE nope

	stz $149f
	REP #$20
	LDA #$FFC0
	STA $2A
	LDA #$FFC0
	STA $2C
	SEP #$20
	REP #$20
	LDA !Rotation
	STA $36
	SEP #$20
nope:
	RTL

Table1:
db $09,$20,$40
db $1C,$20,$40
db $01,$20,$41
db $35,$21,$41
db $03,$21,$42
db $1D,$22,$42
db $03,$23,$42
db $03,$24,$42
db $02,$25,$42
db $02,$26,$42
db $07,$27,$42
db $07,$28,$42
db $01,$28,$41
db $09,$29,$41
db $06,$2A,$41
db $01,$2B,$41
db $01,$2B,$42
db $03,$2C,$42
db $02,$2D,$43
db $02,$2E,$43
db $01,$2E,$44
db $01,$32,$4C
db $01,$32,$54
db $01,$2A,$4B
db $01,$24,$43
db $02,$24,$44
db $04,$25,$44
db $02,$25,$45
db $04,$26,$45
db $01,$26,$46
db $04,$27,$46
db $01,$27,$47
db $05,$28,$47
db $06,$29,$48
db $05,$2A,$49
db $01,$2B,$49
db $04,$2B,$4A
db $02,$2C,$4A
db $03,$2C,$4B
db $03,$2D,$4B
db $09,$2D,$4C
db $00
T1End:

Table2:
db $09,$80
db $09,$80
db $12,$81
db $12,$82
db $12,$83
db $12,$84
db $12,$85
db $16,$86
db $04,$87
db $03,$86
db $04,$85
db $05,$84
db $05,$83
db $06,$82
db $05,$81
db $02,$82
db $01,$83
db $02,$84
db $01,$85
db $02,$86
db $01,$87
db $02,$88
db $01,$93
db $01,$9B
db $01,$94
db $04,$8C
db $08,$8D
db $07,$8E
db $07,$8F
db $08,$91
db $07,$92
db $07,$93
db $09,$94
db $00
T2End: