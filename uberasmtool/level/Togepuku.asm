;Frantic Fugumanen ASM (levels 15, 45 and 4B).
;Includes:
;load: SMWC coin custom trigger init
;init: translucent layer 3 handler, message box text DMA for level 15, wave HDMA init and processing for level 4B
;main: spawn cluster leaves
;nmi: wave HDMA processing for levels 45 and 4B

!hdmaparallaxtbls = $7F9E00		;no need to change this, this address is nice lol
!waveindex = $0F5E|!addr		;index for wave scroll

load:
JML Load_CDM16intoCT		;initialize custom trigger exanimation flags D, E and F for SMWC coins

init:
LDA #$80			;initialize RAM wave HDMA table - set scanlines and end of table
STA !hdmaparallaxtbls
LDA #$47
STA !hdmaparallaxtbls+3
LDA #$08
STA !hdmaparallaxtbls+6
STA !hdmaparallaxtbls+9
STA !hdmaparallaxtbls+12
LDA #$00
STA !hdmaparallaxtbls+15

LDA #$15			;layer 1, layer 3 and sprites on main screen
STA $212C
LDA #$13			;subscreen/color math for layer 1, layer 2 and sprites.
STA $212D

LDA $13BF|!addr
CMP #$17
BEQ yep

LDA $010B|!addr			;load low byte of current sublevel
CMP #$15			;compare to value
BNE +				;if not equal, return.

REP #$20			;16-bit A
LDA #$0200			;load amount of bytes to transfer
STA $00				;store to scratch RAM.
LDX.b #MsgDMA2>>16		;load bank byte of text tilemap table into X
STX $09				;store X to scratch RAM.

LDA.w #MsgDMA2			;load absolute address of text tilemap table 1
STA $07				;store to scratch RAM.
LDA #$5CA0			;load VRAM destination
STA $0A				;store to scratch RAM.
JSL DMA_UseNoIndex		;execute DMA

+
LDA.b #Table1>>16		;load bank of one of the tables
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
REP #$20			;16-bit A
LDA.w #Table1			;load address of first table
STA $00				;store to scratch RAM.
LDA.w #Table2			;load address of second table
STA $03				;store to scratch RAM.
JSL HDMA_Color2			;execute HDMA (A goes 8-bit after this routine)

LDA $010B|!addr			;load low byte of current sublevel
CMP #$15			;compare to value
BNE yep				;if not equal, execute HDMA.
nop:
RTL				;return.

yep:
REP #$20
LDA #$0E02			;mode 2 on $210E (a.k.a. affect layer 1 Y-wise)
STA $4350
LDA.w #!hdmaparallaxtbls	;address of table
STA $4352
SEP #$20
LDA.b #!hdmaparallaxtbls>>16	;bank of table
STA $4354
LDA #$20			;\ Enable HDMA
TSB $0D9F|!addr			;/ on channel 5

nmi:
LDA $010B|!addr			;load low byte of current sublevel
CMP #$15			;compare to value
BEQ nop				;if equal, return.

LDA $14
LSR #3
AND #$07
STA !waveindex

REP #$20
LDA !waveindex
ASL
TAX

LDA $1C
STA !hdmaparallaxtbls+1
STA !hdmaparallaxtbls+4

JSR .inx1
STA !hdmaparallaxtbls+7
JSR .inx1
STA !hdmaparallaxtbls+10
JSR .inx1
STA !hdmaparallaxtbls+13

SEP #$20
RTL

.inx1
LDA $1C
CLC
ADC Index,x
INX #2
RTS

main:
LDA #$0D		;load sprite number to spawn
JML clusterspawn_run	;spawn cluster leafy leaves

Table1:
db $05,$27,$55
db $04,$27,$56
db $03,$28,$56
db $03,$28,$57
db $04,$29,$57
db $02,$29,$58
db $05,$2A,$58
db $06,$2B,$59
db $01,$2C,$59
db $05,$2C,$5A
db $04,$2D,$5A
db $02,$2D,$5B
db $05,$2E,$5B
db $02,$2F,$5B
db $04,$2F,$5C
db $06,$30,$5C
db $05,$31,$5D
db $06,$32,$5D
db $01,$33,$5D
db $04,$33,$5E
db $05,$34,$5E
db $06,$35,$5E
db $01,$36,$5E
db $04,$36,$5F
db $05,$37,$5F
db $7E,$38,$5F
db $00

Table2:
db $0C,$9D
db $2B,$9E
db $80,$9F
db $29,$9F
db $00

Index:
dw $FFFF,$0000,$0001,$0002,$0002,$0001,$0000,$FFFF
dw $FFFF,$0000				;extension beyond

MsgDMA2:
db $FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$BC,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$1C,$3D,$0D,$3D,$0E,$3D,$13,$3D
db $08,$3D,$02,$3D,$04,$3D,$1C,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$05,$3D,$48,$3D,$52,$3D,$47,$3D,$48,$3D,$4D,$3D,$46,$3D
db $FE,$3C,$4E,$3D,$45,$3D,$FE,$3C,$0F,$3D,$4E,$3D,$51,$3D,$42,$3D
db $54,$3D,$4F,$3D,$54,$3D,$45,$3D,$45,$3D,$44,$3D,$51,$3D,$52,$3D
db $FE,$3C,$48,$3D,$52,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$53,$3D,$44,$3D,$4C,$3D,$4F,$3D,$4E,$3D,$51,$3D,$40,$3D
db $51,$3D,$48,$3D,$4B,$3D,$58,$3D,$FE,$3C,$47,$3D,$40,$3D,$4B,$3D
db $53,$3D,$44,$3D,$43,$3D,$1D,$3D,$FE,$3C,$40,$3D,$52,$3D,$FE,$3C
db $4C,$3D,$4E,$3D,$52,$3D,$53,$3D,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$47,$3D,$40,$3D,$55,$3D,$44,$3D,$FE,$3C,$43,$3D,$44,$3D
db $55,$3D,$44,$3D,$4B,$3D,$4E,$3D,$4F,$3D,$44,$3D,$43,$3D,$FE,$3C
db $40,$3D,$4D,$3D,$FE,$BC,$48,$3D,$4C,$3D,$4C,$3D,$54,$3D,$4D,$3D
db $48,$3D,$53,$3D,$58,$3D,$FE,$3C,$53,$3D,$4E,$3D,$FE,$3C,$FE,$3C
db $FE,$3C,$45,$3D,$48,$3D,$51,$3D,$44,$3D,$1B,$3D,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$BC,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C
db $FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C
db $FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$3C
db $FE,$BC,$FE,$3C,$FE,$3C,$FE,$3C,$FE,$BC,$FE,$3C,$FE,$3C,$FE,$3C