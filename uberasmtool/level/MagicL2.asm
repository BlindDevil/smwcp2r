init:
LDA #$02			;load bit value for layer 2
STA $212C			;store to mainscreen register.
LDA #$17			;load bit value for everything, even layer 2 and sprites, too
STA $212D			;store to subscreen register.
RTL				;return.

main:
JML MovePlayerAlongL2_run	;make the player stand still relative to layer 2