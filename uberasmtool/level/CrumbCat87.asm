;Crumbling Catacombs ASM (level 87).
;Includes:
;main: map16 specific shattering effects

main:
	LDA $D2
	CMP #$07
	BNE End87
	LDA $18C5
	CMP #$01
	BEQ End87
	LDA #$01
	STA $18C5
	JSL Shatter101_main
	End87:
	RTL