lorom
org $108000; Note that since global code is a single file, all code below should return with RTS.

;blindnote: uberasm defines won't work here! fuk u vilela
;so we have to redefine stuff, here we go:

;blindedit: as of v1.4 we finally can call library codes and use uberasm defines. thank goodness
if read1($00FFD5) == $23
	!SA_1 = 1
	!base1 = $3000
	!base2 = $6000
	!bankA = $00
	!bankB = $000000
else
	!SA_1 = 0
	!base1 = $0000
	!base2 = $0000
	!bankA = $80
	!bankB = $800000
endif

;Defines below.
!titleram1 = $62		;trigger the number 2 in titlescreen
!titleram2 = $0F5E|!base2	;trigger the copyright text in titlescreen

!titlepointer = $0F5F|!base2	;title animation pointer
!titletimer = $0F60|!base2	;title animation timer

nmi:
JSL ItIsAJSLJustInCaseWeGottaMoveOurCodeOutsideUberASMAndIDontReallyWantToMakeThisLabelLongerThanItAlreadyIs

load:
init:
RTS

main:
JSR PlayTimeHandler		;handle our play time counter

LDA $0100|!base2		;load game mode
CMP #$05			;compare to value
BNE +				;if not equal, branch.

JMP TitleInit			;go to titlescreen init code

+
CMP #$06			;compare to value
BNE +				;if not equal, branch.

JMP TitleInit			;go to titlescreen init code

+
CMP #$07			;compare to value
BNE +				;if not equal, branch.

JMP TitleMain			;go to titlescreen main code

BNE +				;if not equal, branch.

JMP TitleMain			;go to titlescreen main code

+
CMP #$0D			;compare to value
BNE +				;if not equal, branch.

LDA #$30			;load value
STA $0F2F|!addr			;store to base indicators X-pos address.
RTS				;return.

+
CMP #$0E			;compare to value
BNE +				;if not equal, branch.

JSL MainForMap			;do stuff for the map (would be used in gamemode E but I'd have to duplicate some defines)

+
RTS

NoInputsAllowed:
REP #$20			;16-bit A
STZ $15				;disable controller data 1.
STZ $17				;disable controller data 2.
SEP #$20			;8-bit A
RTS				;return.

TitleInit:
LDA #$01			;load value (display layer 1 only)
STA $212C : STA $212D		;store to main/subscreen registers.

LDA #$20			;load amount of frames
STA !titletimer			;store to title animation timer.

JSR NoInputsAllowed		;call routine to disable controller inputs
RTS				;return.

TitleMain:
LDA !titletimer			;load titlescreen animation timer
BEQ +				;if equal zero, don't decrement it anymore.

DEC !titletimer			;decrement timer by one

+
LDA !titlepointer		;load titlescreen animation pointer
CMP #$08			;compare to value
BCS .ret			;if higher, return. (failsafe - avoid crashes)

XBA				;flip high/low bytes of A
JSR NoInputsAllowed		;call routine to disable controller inputs
XBA				;flip high/low bytes of A
JSL $0086DF|!bankB		;call 2-byte pointer subroutine
dw .title0		;$00 - wait
dw .title1		;$01 - move layer 1 upwards
dw .title2		;$02 - wait/execute thunder
dw .title3		;$03 - wait/revert thunder, show number 2, play sfx
dw .title2		;$04 - wait/execute thunder
dw .title5		;$05 - wait/revert thunder, show layer 2, play sfx, activate HDMA gradient
dw .title2		;$06 - wait/execute thunder
dw .title7		;$07 - wait/revert thunder, show layer 2, play sfx, activate HDMA gradient
dw .ret			;$08 - return (once again failsafe)

.title0
LDA !titletimer			;load titlescreen animation timer
BNE .ret			;if not equal zero, return.

LDA #$2C			;load amount of frames
STA !titletimer			;store to titlescreen animation timer.
INC !titlepointer		;increment titlescreen animation pointer by one

.ret
RTS				;return.

.title1
LDA !titletimer			;load titlescreen animation timer
BNE +				;if not equal zero, move layer 1 upwards.

LDA #$10			;load amount of frames
STA !titletimer			;store to titlescreen animation timer.
INC !titlepointer		;increment titlescreen animation pointer by one
RTS				;return.

+
LDA $13				;load frame counter
AND #$03			;preserve bits 0 and 1
BEQ .ret			;if none are set, return.

REP #$20			;16-bit A
INC $1464|!base2		;increment layer 1 Y-pos, next frame, by one
SEP #$20			;8-bit A
RTS				;return.

.title2
LDA !titletimer			;load titlescreen animation timer
BNE .ret			;if not equal zero, return.

JSR .whiteflash			;make screen flash
LDA #$0C			;load amount of frames
STA !titletimer			;store to titlescreen animation timer.
INC !titlepointer		;increment titlescreen animation pointer by one
RTS				;return.

.whiteflash
REP #$20			;16-bit A
LDA #$77BD			;load color value
STA $0701|!base2		;store to back area color.
STZ $212C			;hide all layers.
SEP #$20			;8-bit A
STZ $0D9F|!base2		;disable all HDMA.
RTS				;return.

.title3
LDA !titletimer			;load titlescreen animation timer
BNE .ret			;if not equal zero, return.

REP #$20			;16-bit A
LDA #$1111			;load value (display layer 1 and sprites only)
STA $212C			;store to main/subscreen registers.
STZ $0701|!base2		;reset back area color to black
SEP #$20			;8-bit A

INC !titleram1			;increment address - display number 2

LDA #$18			;load SFX value
STA $1DFC|!base2		;store to address to play it.

LDA #$40			;load amount of frames
STA !titletimer			;store to titlescreen animation timer.
INC !titlepointer		;increment titlescreen animation pointer by one
RTS				;return.

.title5
LDA !titletimer			;load titlescreen animation timer
BNE .ret			;if not equal zero, return.

.dopostthunder2
REP #$20			;16-bit A
LDA #$1717			;load value (display all layers and sprites)
STA $212C			;store to main/subscreen registers.
STZ $0701|!base2		;reset back area color to black
JSR TitleHDMA			;activate HDMA gradient for titlescreen (A goes 8-bit after this)

LDA #$18			;load SFX value
STA $1DFC|!base2		;store to address to play it.

LDA #$18			;load amount of frames
STA !titletimer			;store to titlescreen animation timer.
INC !titlepointer		;increment titlescreen animation pointer by one

.ret2
RTS				;return.

.title7
LDA !titletimer			;load titlescreen animation timer
BNE .ret2			;if not equal zero, return.

INC !titleram2			;increment address - display copyright text
BRA .dopostthunder2		;restore layers and HDMA after the thunder, then finish the animation

PlayTimeHandler:
;insert progress check here

LDA $0100|!base2		;load current game mode
CMP #$0E			;check if on overworld
BEQ RollItBuddy			;if yes, update the timer.
CMP #$14			;check if on level
BEQ RollItBuddy			;if yes, update the timer.
CMP #$33			;check if on stats screen
BEQ RollItBuddy			;if yes, update the timer.

WeVe100edDaGame:
RTS				;return.

RollItBuddy:
LDA $7FB59E			;load play time's frames
INC				;increment it by one
CMP #$3C			;compare to value
BCC NotASecond			;if it didn't match one second, skip ahead.

LDA $7FB59B			;load play time's seconds
INC				;increment by one
CMP #$3C			;compare to value
BCC NotAMinute			;if it didn't match one minute, skip ahead.

LDA $7FB59C			;load play time's minutes
INC				;increment by one
CMP #$3C			;compare to value
BCC NotAnHour			;if it didn't match one hour, skip ahead.

LDA $7FB59D			;load play time's hours
CMP #$63			;compare to value
BEQ StopClock			;if equal, we won't do anything anymore. it's the end of time.
INC				;increment by one
STA $7FB59D			;store to play time's hours.

LDA #$00			;load value

NotAnHour:
STA $7FB59C			;store to play time's minutes.

LDA #$00			;load value

NotAMinute:
STA $7FB59B			;store to play time's seconds.

LDA #$00			;load value

NotASecond:
STA $7FB59E			;store to play time's frames.

StopClock:
RTS				;return.

;blindnote again: this ABSOLUTELY SUCKS, library files can't be accessed from global code file as well
;I hate you so much, vilela
;well if there's any need on using HDMA elsewhere, call GlobHDMA with the same inputs as the library file.
;blindedit: they work now but rn i'm lazy to update this

GlobHDMA3:
REP #$20		;\
LDA #$3200		; | Use Mode 0 on register 2132
STA $4330		; | 4330 = Mode, 4331 = Register for Channel 3
STA $4340		; | 4340 = Mode, 4341 = Register for Channel 4
STA $4350		; | 4350 = Mode, 4351 = Register for Channel 5
LDA $00			; | Address of red HDMA table
STA $4332		; | 4332 = Low-Byte of table, 4333 = High-Byte of table for Channel 3
LDX $02			; | Address of red HDMA table, get bank byte
STX $4334		; | 4334 = Bank-Byte of table for Channel 3
LDA $03			; | Address of green HDMA table
STA $4342		; | 4342 = Low-Byte of table, 4343 = High-Byte of table for Channel 4
LDX $05			; | Address of green HDMA table, get bank byte
STX $4344		; | 4344 = Bank-Byte of table for Channel 4
LDA $06			; | Address of blue HDMA table
STA $4352		; | 4352 = Low-Byte of table, 4353 = High-Byte of table for Channel 5
LDX $08			; | Address of blue HDMA table, get bank byte
STX $4354		; | 4354 = Bank-Byte of table for Channel 5
SEP #$20		;/
LDA #$38
TSB $0D9F|!base2
RTS

TitleHDMA:
LDA #.THDMA_Red			;load address value of red HDMA table
STA $00				;store to scratch RAM.
LDA #.THDMA_Green		;load address value of red HDMA table
STA $03				;store to scratch RAM.
LDA #.THDMA_Blue		;load address value of red HDMA table
STA $06				;store to scratch RAM.
SEP #$20			;8-bit A
PHK				;push program bank
PLA				;pull it back into A
STA $02				;store to scratch RAM.
STA $05				;store to scratch RAM.
STA $08				;store to scratch RAM.
JSR GlobHDMA3			;call HDMA (3 channels) handling routine
RTS				;return.

.THDMA_Red
db $60,$26
db $03,$27
db $03,$28
db $03,$29
db $03,$2A
db $03,$2B
db $03,$2C
db $03,$2D
db $03,$2E
db $03,$2F
db $03,$30
db $04,$31
db $03,$32
db $03,$33
db $03,$34
db $03,$35
db $03,$36
db $04,$37
db $03,$38
db $03,$39
db $03,$3A
db $03,$3B
db $04,$3C
db $03,$3D
db $03,$3E
db $34,$3F
db $00

.THDMA_Green
db $74,$45
db $17,$44
db $18,$43
db $3C,$42
db $00

.THDMA_Blue
db $68,$87
db $10,$86
db $11,$85
db $13,$84
db $43,$83
db $00

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;Below we find NMI stuff meant to be run during OW-related gamemodes.
;Dealing with OAM kinda sucks, and SMW erases our sprites if we only use gamemode E for them.

;Tilemaps:
!FrameL = $4C		;frame where coins/exits appear, left tile (16x16)
!FrameR = $44		;frame where coins/exits appear, right tile (16x16)
!FrameProp = $34	;palette/properties for frame

!Outline1 = $6C		;outlined smwc coin, frames 1-3 (8x8)
!Outline2 = $6D 	;outlined smwc coin, frames 2-4 (8x8)
!Coin = $6E		;collected smwc coin (8x8)
!CoinProp = $30		;palette/properties for coin

!ExitN = $7D		;exit not found (8x8)
!ExitY = $6F		;exit found (8x8)
!ExitProp = $32		;palette/properties for exit

;OAM slot defines. They aren't all used, but serve as a guide.
;hint: tab is used
	!OAM03 = $70		;OAM slot for coin 1
!OAM04 = $74		;OAM slot for coin 2
!OAM05 = $78		;OAM slot for coin 3

	!OAM06 = $40		;OAM slot for exit 1
!OAM07 = $44		;OAM slot for exit 2

	!OAM08 = $84		;OAM slot for coins frame 1
!OAM09 = $88		;OAM slot for coins frame 2
!OAM10 = $8C		;OAM slot for coins frame 3
!OAM11 = $90		;OAM slot for exits frame 1
!OAM12 = $94		;OAM slot for exits frame 2
!OAM13 = $98		;OAM slot for exits frame 3

FramesXDisp:
db $C0,$D0,$E0		;top frame (SMWC coins)
db $D4,$E4,$E4		;bottom frame (exits, used when the level has 1 exit)
db $C0,$D0,$E0		;top frame (SMWC coins)
db $CA,$DA,$E0		;bottom frame (exits, used when the level has 2 exits)

FramesYDisp:
db $2B,$2B,$2B
db $3E,$3E,$3E

FramesTiles:
db !FrameL,!FrameR,!FrameR
db !FrameL,!FrameR,!FrameR

CoinTiles:
db !Outline1,!Outline2,!Coin

CoinXDisp:
db $D1,$DB,$E5

!CoinYDisp = $2F

ExitXDisp:
db $E5,$DB

!ExitYDisp = $42

OutlineProps:
db $00,$00,$C0,$40

;other used ram addresses:
;> $0F63-$0F65: tile number for each SMWC coin
;> $0F66-$0F68: tile flip/properties for each SMWC coin
;> $0F69-$0F6A: tile number for each exit indicator
;> $18C5: flags for displaying indicators and world name (format: -----WSI | W: world name. S: secret exit present. I: indicators.)
;> $0F2F: base X-pos of indicators
;> $18C7: SMWC coin flags/exits of current level (format: eetttppp | e: exits. t: temporary coins (blink). p: permanent coins (full).)
;> $18C8: world name display timer

ItIsAJSLJustInCaseWeGottaMoveOurCodeOutsideUberASMAndIDontReallyWantToMakeThisLabelLongerThanItAlreadyIs:
LDA $0100		;load current game mode
CMP #$0E		;check if on overworld
BEQ .runinds		;if yes, run indicators OAM drawing code
CMP #$0F		;check if fading out from overworld
BNE .ret		;if not equal, return.

.runinds
PHB			;preserve data bank
PHK			;push program bank
PLB			;pull as new data bank

LDA $0F2F|!addr		;load base X-disp value for indicators
CMP #$30		;compare to value
BCS +			;if higher or equal, skip ahead.

JSR DrawFrames		;draw frames where indicators will be displayed
JSR DrawCoins		;draw SMWC coins indicator
JSR DrawExits		;draw exits indicator

+
;world name thing go here

PLB			;restore original data bank

.ret
RTL			;return.

;The frames that show up below indicators themselves.
DrawFrames:
LDY #!OAM08		;get OAM slot number into Y
LDX #$05		;loop count (draw 6 tiles)

STZ $00			;reset scratch RAM.
LDA $18C5|!addr		;load flags
AND #$02		;check if secret exit flag is set
BEQ .loop		;if not, skip ahead.

LDA #$06		;load value
STA $00			;store to scratch RAM.

.loop
PHX			;preserve loop count
TXA			;transfer X to A
CLC			;clear carry
ADC $00			;add value from scratch RAM
TAX
LDA FramesXDisp,x	;load X-disp from table according to index
CLC			;clear carry
ADC $0F2F|!addr		;add base X-disp value
BPL .forceout		;if positive, force it to be #$F0.
CMP #$F0		;compare to value
BCC +			;if lower, keep the value untouched.

.forceout
LDA #$F0		;load value

+
STA $0200|!addr,y	;store to OAM.
PLX			;restore loop count

LDA FramesYDisp,x	;load Y-disp for tile according to index
STA $0201|!addr,y	;store to OAM.

LDA FramesTiles,x	;load tile number from table according to index
STA $0202|!addr,y	;store to OAM.

LDA #!FrameProp		;load palette/properties value
STA $0203|!addr,y	;store to OAM.

PHY			;preserve OAM index
TYA			;transfer Y to A
LSR #2			;divide by 2 twice
TAY			;transfer to Y
LDA #$02		;load tile size (#$00 = 8x8, #$02 = 16x16)
STA $0420|!addr,y	;store to OAM.
PLY			;restore OAM index

INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL .loop		;loop while X is positive.
RTS			;return.

;The SMWC coin indicators.
DrawCoins:
LDY #!OAM03		;get OAM slot number into Y
LDX #$02		;loop count (draw 3 tiles)

.loop
LDA CoinXDisp,x		;load X-disp from table according to index
CLC			;clear carry
ADC $0F2F|!addr		;add base X-disp value
BPL .forceout		;if positive, force it to be #$F0.
CMP #$F0		;compare to value
BCC +			;if lower, keep the value untouched.

.forceout
LDA #$F0		;load value

+
STA $0200|!addr,y	;store to OAM.

LDA #!CoinYDisp		;load Y-disp for tile
STA $0201|!addr,y	;store to OAM.

LDA.l $0F63|!addr,x	;load tile number from RAM according to index
STA $0202|!addr,y	;store to OAM.

LDA #!CoinProp		;load palette/properties value (palette only)
ORA.l $0F66|!addr,x	;set flips from RAM according to index
STA $0203|!addr,y	;store to OAM.

PHY			;preserve OAM index
TYA			;transfer Y to A
LSR #2			;divide by 2 twice
TAY			;transfer to Y
LDA #$00		;load tile size (#$00 = 8x8, #$02 = 16x16)
STA $0420|!addr,y	;store to OAM.
PLY			;restore OAM index

INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL .loop		;loop while X is positive.
RTS			;return.

;The exit indicators.
DrawExits:
LDY #!OAM06		;get OAM slot number into Y
LDX #$01		;loop count (draw 2 tiles)

STZ $00			;reset scratch RAM.
LDA $18C5|!addr		;load flags
AND #$02		;check if secret exit flag is set
BNE .loop		;if it is, skip ahead.

LDA #$0B		;load value
STA $00			;store to scratch RAM.

.loop
LDA ExitXDisp,x		;load X-disp from table according to index
CLC			;clear carry
ADC $0F2F|!addr		;add base X-disp value
ADC $00			;add (no) secret exit X-disp value
BPL .forceout		;if positive, force it to be #$F0.
CMP #$F0		;compare to value
BCC +			;if lower, keep the value untouched.

.forceout
LDA #$F0		;load value

+
STA $0200|!addr,y	;store to OAM.

LDA #!ExitYDisp		;load Y-disp for tile
STA $0201|!addr,y	;store to OAM.

LDA.l $0F69|!addr,x	;load tile number from RAM according to index
STA $0202|!addr,y	;store to OAM.

LDA #!ExitProp		;load palette/properties value
STA $0203|!addr,y	;store to OAM.

PHY			;preserve OAM index
TYA			;transfer Y to A
LSR #2			;divide by 2 twice
TAY			;transfer to Y
LDA #$00		;load tile size (#$00 = 8x8, #$02 = 16x16)
STA $0420|!addr,y	;store to OAM.
PLY			;restore OAM index

INY #4			;increment OAM index four times
DEX			;decrement X by one
BPL .loop		;loop while X is positive.
RTS			;return.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;GFX handlers for indicators in the overworld.
MainForMap:
PHB			;preserve data bank
PHK			;push program bank
PLB			;pull as new data bank
JSR ManageDisplayInd	;manage displaying indicators
JSR SMWCCoinInd		;handle SMWC coin tile indicators
JSR ExitInd		;handle exit tile indicators
PLB			;restore original data bank
RTL			;return.

SMWCCoinInd:
LDA $18C7|!addr		;load coins and exits flags
AND #$3F		;preserve all coin bits

LDX #$00		;loop count

.pcoinloop
PHA			;preserve bitflags
AND #$01		;preserve bit 0 only
CMP #$01		;check if bit 0 is set
BNE ..pnotset		;if not set, display it as empty.

LDA #!Coin		;load coin tilemap
STA $0F63|!addr,x	;store to indexed address.
STZ $0F66|!addr,x	;reset indexed address used for flips.
BRA ..pender		;branch ahead

..pnotset
PLA			;restore bitflags into A
PHA			;preserve them again
AND #$08		;preserve bit 3 only
CMP #$08		;check if bit 3 is set
BNE ..emptycoin		;if not, draw coin outline.

LDA $13			;load frame counter
LSR #3			;divide by 2 three times
AND #$01		;preserve bit 0 only
PHX			;preserve loop count
TAX			;transfer A to X
LDA CoinTiles+1,x	;load tile number from table according to index
PLX			;restore loop count
STA $0F63|!addr,x	;store to indexed address.
STZ $0F66|!addr,x	;reset indexed address used for flips.
BRA ..pender		;branch ahead

..emptycoin
LDA $13			;load frame counter
LSR #3			;divide by 2 three times
AND #$03		;preserve bits 0 and 1
STA $00			;store to scratch RAM.
PHX			;preserve loop count
TAX			;transfer A to X
LDA OutlineProps,x	;load flip properties table according to index
PLX			;restore loop count
STA $0F66|!addr,x	;store to indexed address used for flips.

PHX			;preserve loop count
LDA $00			;load value from scratch RAM
AND #$01		;preserve bit 0 only
TAX			;transfer to X
LDA CoinTiles,x		;load tile number from table according to index
PLX			;restore loop count
STA $0F63|!addr,x	;store to indexed address.

..pender
PLA			;restore bitflags
LSR			;shift bits to the right
INX			;increment X by one
CPX #$03		;compare X to value
BNE .pcoinloop		;keep looping if not equal.
RTS			;return.

ExitInd:
LDA $18C7|!addr		;load coins and exits flags
AND #$C0		;preserve all exit bits

LDX #$00		;loop count

.exitloop
PHA			;preserve bitflags
AND #$40		;preserve bit 6 only
BEQ ..noexit		;if not set, no exit found so draw the indicator off.

LDA #!ExitY		;load tile number
BRA ..eender		;branch ahead

..noexit
LDA #!ExitN		;load tile number

..eender
STA $0F69|!addr,x	;store to indexed address.

PLA			;restore bitflags
LSR			;shift bits to the right
INX			;increment X by one
CPX #$02		;compare X to value
BNE .exitloop		;loop if not equal.

RTS			;return.

ManageDisplayInd:
LDA $13D9|!addr		;load pointer to processes running on the overworld
CMP #$03		;compare to value
BEQ .FlagOnNSetLvl	;if equal, display the indicators and update level and flags for it
CMP #$05		;compare to value
BEQ .FlagOn		;if equal, display the indicators.
CMP #$08		;compare to value
BEQ .FlagOn		;if equal, display the indicators.

.FlagOff
LDA #$01		;load indicators flag value
TRB $18C5|!addr		;clear it from address.
BRA MoveInd		;branch to move our indicators

.FlagOnNSetLvl
JSL OWStuff_GetLvlFromCoord	;get current level number from coordinates
TAX				;transfer to X
LDA $7FB539,x			;load SMWC coin flags from respective level
AND #$07			;preserve permanent coin bits
STA $18C7|!addr			;store to mirror.
LDA $7FB539,x			;load SMWC coin flags from respective level again
AND #$70			;preserve temporary coin bits
LSR				;shift bits right
TSB $18C7|!addr			;set them to mirror.

JSR CheckOnExits		;check on events and exits for levels
BRA MoveInd			;branch ahead

.FlagOn
LDA #$01		;load indicators flag value
TSB $18C5|!addr		;set it to address.

MoveInd:
LDA $18C5|!addr		;load flags
AND #$01		;check if display indicators flag is set
BEQ MoveBack		;if not, move them back.

LDA $0F2F|!addr		;load base X-pos for indicators
BEQ .done		;if equal, stop decrementing.
CMP #$03		;compare to value
BCC .spd1		;if lower, move 1 pixel every 2 frames.
CMP #$06		;compare to value
BCC .spd2		;if lower, move 1 pixel per frame.

DEC $0F2F|!addr		;decrement base X-pos for indicators by one...
DEC $0F2F|!addr		;...twice (move 3 pixels per frame)
BRA .spd2		;branch ahead

.spd1
LDA $13			;load frame counter
AND #$01		;preserve bit 0 only
BNE .done		;if it's set, skip ahead.

.spd2
DEC $0F2F|!addr		;decrement base X-pos for indicators by one

.done
RTS			;return.

MoveBack:
LDA $0F2F|!addr		;load base X-pos for indicators
CMP #$34		;compare to value
BCS .done		;if higher, stop incrementing.
CMP #$02		;compare to value
BCC .spd1		;if lower, move 1 pixel every 2 frames.
CMP #$04		;compare to value
BCC .spd2		;if lower, move 1 pixel per frame.

INC $0F2F|!addr		;increment base X-pos for indicators by one...
INC $0F2F|!addr		;...twice (move 3 pixels per frame)
BRA .spd2		;branch ahead

.spd1
LDA $13			;load frame counter
AND #$01		;preserve bit 0 only
BNE .done		;if it's set, skip ahead.

.spd2
INC $0F2F|!addr		;increment base X-pos for indicators by one

.done
RTS			;return.

CheckOnExits:
LDY #$00		;load value into Y (defines normal or secret exit for check)

LDA #$02		;load value for secret exit present flag
TRB $18C5|!addr		;clear it from flags.

LDA ExitsPerLevel,x	;load amount of exits the level has
BEQ .noexits		;if none, no reason to display anything.
DEC			;decrement A by one
BEQ .oneexit		;if equal #$01, level has one exit, so branch there.

.twoexits
INY			;increment Y by one
LDA #$02		;load value for secret exit present flag
TSB $18C5|!addr		;set it to flags.

.oneexit
JSR CheckBeaten		;check if the respective event was beaten (carry set = yes. carry clear = no.)
DEY			;decrement Y by one
BPL .oneexit		;loop while it's positive.
RTS			;return.

.noexits
LDA #$01		;load indicators flag value
TRB $18C5|!addr		;clear it from address. we don't want to display anything for a level with no exits or events.
RTS			;return.

CheckBeaten:
PHX			;preserve x
PHY			;preserve Y
TYA			;transfer Y to A
CLC			;clear carry
ADC EventsPerLevel,x	;add event number to it (Y=0, normal exit. Y=1, secret exit)
STA $00			;store to scratch RAM.
LSR #3			;divide by 2 three times
TAY			;transfer to Y
LDA $00			;load event number from scratch RAM
AND #$07		;preserve bits 0, 1 and 2
TAX			;transfer to X

LDA $1F02|!addr,y	;load overworld event flags according to index
AND ReverseAND,x	;check if respective indexed event is passed
BEQ .notpassed		;if not passed, skip ahead.

PLY			;restore Y

LDA ReverseAND,y	;load respective bit value from table according to index
TSB $18C7|!addr		;set them to mirror.

PLX			;restore X
RTS			;return.

.notpassed
PLY			;restore Y

LDA ReverseAND,y	;load respective bit value from table according to index
TRB $18C7|!addr		;clear them from mirror.

PLX			;restore X
RTS			;return.

;NUMBER OF EXITS PER LEVEL
;Here you will determine how many exits every level has. Possible values: $00, $01 and
;$02. Other values will work the same as having two exits.
ExitsPerLevel:
	db $00,$00,$01,$02,$01,$01,$01,$02		;levels 000-007
	db $01,$02,$01,$02,$01,$01,$01,$02		;levels 008-00F
	db $01,$01,$02,$01,$01,$02,$01,$01		;levels 010-017
	db $01,$01,$02,$01,$01,$01,$01,$01		;levels 018-01F
	db $02,$01,$01,$01,$01				;levels 020-024
	db     $01,$02,$02,$02,$01,$00,$00		;levels 101-107
	db $00,$00,$00,$00,$00,$00,$00,$02		;levels 108-10F
	db $01,$01,$01,$02,$02,$01,$01,$01		;levels 110-117
	db $02,$02,$00,$01,$02,$01,$01,$02		;levels 118-11F
	db $01,$01,$01,$01,$01,$01,$02,$02		;levels 120-127
	db $01,$01,$01,$01,$01,$02,$02,$01		;levels 128-12F
	db $01,$01,$01,$01,$01,$01,$01,$01		;levels 130-137
	db $01,$01,$01,$01				;levels 138-13B

;EVENT NUMBERS PER LEVEL
;Here you will determine the regular events every level triggers. When a level
;uses a secret exit, it'll read the regular exit event number plus one when
;applicable. Reminder that $FF means 'no event'.
EventsPerLevel:
	db $FF,$FF,$00,$01,$03,$09,$04,$05		;levels 000-007
	db $17,$07,$0E,$0F,$13,$0D,$0A,$0B		;levels 008-00F
	db $12,$11,$14,$1C,$16,$18,$1A,$1D		;levels 010-017
	db $1B,$1E,$1F,$21,$22,$23,$31,$24		;levels 018-01F
	db $25,$2A,$65,$66,$27				;levels 020-024
	db     $2E,$2F,$2B,$28,$49,$FF,$FF		;levels 101-107
	db $FF,$FF,$FF,$FF,$FF,$FF,$FF,$32		;levels 108-10F
	db $34,$51,$3A,$38,$35,$37,$3B,$67		;levels 110-117
	db $3C,$46,$FF,$45,$3E,$40,$76,$41		;levels 118-11F
	db $43,$44,$48,$50,$52,$4D,$4E,$4A		;levels 120-127
	db $4B,$4C,$53,$54,$55,$56,$58,$5A		;levels 128-12F
	db $5B,$5C,$5D,$60,$5F,$63,$64,$62		;levels 130-137
	db $5E,$61,$77,$75				;levels 138-13B

ReverseAND:
db $80,$40,$20,$10,$08,$04,$02,$01