incsrc "asm/work/library.asm"
incsrc "../../other/macro_library.asm"
!level_nmi	= 0
!overworld_nmi	= 0
!gamemode_nmi	= 1
!global_nmi	= 1

!sprite_RAM	= $7FAC80

autoclean $B0D7C6
autoclean $ADFE31
autoclean $99FAD1
autoclean $AFFC8A
autoclean $989BDD
autoclean $B0D890
autoclean $99890D
autoclean $9EFF52
autoclean $ADFDFE
autoclean $95C6EA
autoclean $9CFF53
autoclean $91C20E
autoclean $989BDA
autoclean $B0D7D1
autoclean $9BFF8B
autoclean $B0FAD7
autoclean $B0FA91
autoclean $B0FA68
autoclean $99ED59
autoclean $99D27D
autoclean $AFFB56
autoclean $99FF33
autoclean $99E59B
autoclean $91F3B2
autoclean $AFFA2B
autoclean $99FF1B
autoclean $B8BE68
autoclean $B88000
autoclean $B8AE60
autoclean $B7F536
autoclean $99E042
autoclean $99CE29
autoclean $ADEC73
autoclean $B0A5B3
autoclean $99A071
autoclean $99D0EA
autoclean $99C77C
autoclean $A7B4A5
autoclean $99A369
autoclean $A3FEE6
autoclean $95C159
autoclean $99A31B
autoclean $99A2C5
autoclean $999FFF
autoclean $ADE28D
autoclean $ADE18D
autoclean $99DF57
autoclean $999F8B
autoclean $ABB10A
autoclean $999F4D
autoclean $AFC475
autoclean $999F09

!previous_mode = !sprite_RAM+(!sprite_slots*3)

incsrc level.asm
incsrc overworld.asm
incsrc gamemode.asm
incsrc global.asm
incsrc sprites.asm
incsrc statusbar.asm


print freespaceuse
