#SPC
{
	#title "Lament For The Wilted"
	#author "Red Chameleon"
	#comment "Forest/Swamp Ghost House theme"
	#game "SMWCP2"
}

#samples
{
	#optimized
	#AMM
	"CPanFlute.brr"
	"shekere.brr"
	"guiro.brr"
	"CBass.brr"
	"malon.brr"
}

#instruments
{
"CPanFlute.brr"	$FA $E8 $00 $04 $00 ;@30
"shekere.brr"	$00 $00 $28 $04 $C3 ;@31
"guiro.brr"	$8F $F3 $00 $04 $C3 ;@32
"CBass.brr"	$AE $89 $7F $04 $00 ;@33
"malon.brr"	$F8 $E3 $7F $03 $09 ;@34
}

#amk 2

$EF$FF$20$20
$F1$06$50$01

w200 t36

#0 @33 o3 q7F v220 y10,1,1
c2^8<g8d+8>c8<
a+2^8g+8g8d+8
f2^8g8g+4
g1>
[c4.c4<g8d+8>c8<
a+4.a+4g+8g8d+8
f4.f4d+8d8f8
a+4.a+4g+8g8a+8]
g+4.g+4g+8d+8g+8
a+4.a+4a+8>d+8<a+8>
c4.c4<g8d+8>c8<
g4.g4.d4>
*
f4.f4d8f8d+8
d4<a+8>d4d+8d8<a+8>
c4.c4.<g4>
c1<
[r4g+8g+8r8d+8g+8d+8
r4a+8a+8r8f8a+8f8]
r4>c8c8r8<g8>c8<a+8
g4d4g4a+4
*
r4>f8f8r8c8f8c8
g+4g4d+4d4<
[ [[g+4d+8g+4g+8>d+8<g+8]]2
a+4f8a+4f8a+8f8>d+8d8c8<a+4.g4 ]2

#1 @1 $ED$75$E3 o4 v180 y12,1,0 q7F p17,56
g1f1c1<g+1
g1d1c1d1
d+1f2.g+4
g1>d1
g1f1c1f1
g+1f1g+1g1< $ED$72$E3 v200
g+1a+1>c1<g1
g+1a+1f1g+1> $ED$75$E3
[c1d+1d1f1]2

#2 @1 $ED$75$E3 o4 v180 y12,1,0 q7F p17,56
d+1d1<g+1f1
d+1a+1g+1a+1
g+1a+2.>d+4
c1<a+1>
d+1d1<g+1>d1
f1d1c1c1 @34 $ED$73$E3 $DF o4 v210 y6 q7F
c1<a+1>c1d1
c1<a+1g1g+1 @1 $ED$75$E3 o3 v180 y12,1,0 q7F p17,56
[g+1>c1<a+1>d1<]2

#3 @34 $ED$78$EA o3 v190 y6 q7F
g2>c2<f1d+2g+2g1>
c2.d4 $DD$26$12<a+ ^2.
>c4 $DD$26$12<g+ ^2.
d+4 $DD$26$12f ^1
d+1f2. a+4 $DD$26$12g ^1
^2. @3 $ED$77$E3 o4 v220 p14,64 a+4 $DD$30$09>c
^2. o5e8 $DD$01$09f ^8 $DD$18$09d ^2^8 c8 $DD$14$09<a+ ^8 $DD$10$09>c ^8 $DD$10$09<a+ ^4
g+8 $DD$14$09g ^4 a+8 $DD$16$09>f ^4 $DD$24$09d ^2. d8d+8
e8 $DD$03$0Cf ^8 d8 $DD$14$09f ^4 d8 $DD$14$09f ^8 $DD$10$09d+ ^8 c8 $DD$14$09<a+ ^8 >c8 $DD$14$09d ^4 d+8d8<a+8 $DD$14$09>c ^1^4
@3 o4 $DF $ED$7C$F0 y12 v200 [q1Fc8c8q16c8 q1F<a+8g+8a+8>
c8q16c8 q1Fd8d8q16d8 q1Fc8d8<f8
q16f8q13f8 >q1Fd8d8q16d8 q1Fc8d8]<g8
q16g8q13g8> q1Fd8d8q16d8 q1Fc8d8d+8
q16d8q13d8 * o4q1Ff8
q16f8q13f8 q1Ff8f8q16f8 q1Fd+8d8c8
q16c8q13c8 q1Fc8c8q16c8 <q1Fa+8g8q16g8 @34 $DF o3 v225 y6 q7F
[g+4. $DD$40$12g ^4. $DD$3B$12d+ ^4^1
f4. $DD$40$12d+ ^4. $DD$3B$12d ^4^1]2

#4 @30 o3 v120 y8,1,1 q7F p30,16,32
(30)[r1]3 r2. a+4>
[c2. e8 $DD$01$09f ^8 d2^8 c8<a+8>c8<
a+4g+8g4] d+8c4f2.f8g8
g+4f8g+4f8g+8a+8>c8<a+8g+8g4>d+8d8d+8
d1..<a+4>
* <a+8>f4d2.d8d+8
f4d8f4d8f8d+8c8<a+8>c8d4d+8d8<a+8>
c1^4 @3 o4 $DF $ED$7C$F0 r128 y12 v200 [q1Fd+8d+8q16d+8 q1Fd8c8d8
d+8q16d+8 q1Ff8f8q16f8 q1Fd+8f8<a+8
q16a+8q13a+8 >q1Ff8f8q16f8 q1Fd+8f8]c8
q16c8q13c8 q1Ff8f8q16f8 q1Fd+8f8g8
q16g8q13g8 * o4q1Fg+8
q16g+8q13g+8 q1Fg+8g+8q16g+8 q1Fg8f8d+8
q16d+8q13d+8 q1Fd+8d+8q16d+8 q1Fd8c8q16c16...>
$ED$7A$F4 [ [[q7Fd8c8 q7Bd8c8 q77d8c8 q73d8c8]]2 [[q7Fd8<a+8> q7Bd8<a+8> q77d8<a+8> q73d8<a+8>]]2 ]2

#5 @12 v170 y9,1,1 o4
(51)[c4c8f4f8c4]3 c1
(51)15 c1
(51)7 c1
(51)8

#6 @30 o4 v120 y8,1,1 q7F p30,16,32
(30)11 r2. d4
d+2. g8 $DD$01$09g+ ^8 f2^8 d+8d8d+8
d4c8<a+4>d8a+4f2.f8g8
g+4f8g+4f8g+8g8d+8d8d+8f4g8f8d8
d+1^1
o5 v170 p14,64
[@3$ED$72$E3 d4 $DD$05$13d+ ^2. @3$ED$72$E3 c4 $DD$05$13d ^2.]
@3$ED$72$E3 d4 $DD$05$13d+ ^2. @3$ED$72$E3 e4 $DD$05$13f ^2.
*
<@3$ED$72$E3 a4 $DD$05$13a+ ^2. @3$ED$72$E3 b4 $DD$05$13>c ^2.
$ED$7A$F4 [ [[q7Ff8d+8 q7Bf8d+8 q77f8d+8 q73f8d+8]]2 [[q7Ff8d8 q7Bf8d8 q77f8d8 q73f8d8]]2 ]2

#7 v180 y7,1,1 o4
(30)4
[@31q7Fc8q78c8q7Fc8q7Cc8 q7F@32c4 @31c8q78c8]15 r1
*7
@31q7Fc8q78c8q7Fc8q7Cc8 q7F@32c2
*8