;Ravamped by Dark Mario Bros

#amk=1
#SPC
{
	#title "In the Eastern Woods"
	#author "Buster Beetle"
	#comment "Oriental Level 3"
	#game "SMW Central Production 2"
}

#samples
{
	#default
	#AMM
"./ITEW/Bass Drum.brr"
"./ITEW/Electric Snare.brr"
"./ITEW/shamisen.brr"
"./ITEW/finger bass.brr"
"./ITEW/test.brr"
"./ITEW/Gong.brr"
}

#instruments
{
"./ITEW/Bass Drum.brr" 		$FF $D0 $E0 $06 $D2 ;@30
"./ITEW/Electric Snare.brr" 	$FF $F4 $E0 $08 $98 ;@31
"./ITEW/shamisen.brr" 		$DD $F0 $00 $05 $00 ;@32
"./ITEW/finger bass.brr"	$8F $52 $00 $08 $00 ;@33
"./ITEW/test.brr" 		$FF $C5 $00 $04 $00 ;@34
"./ITEW/Gong.brr" 		$FF $CD $00 $02 $00 ;@35
}

$EF $2D $33 $33
$F1 $04 $4C $01
$F2 $32 $4D $4D

#0 t76 w200 y9
o2
@14
$ED $0F $52
v225
p5,10
[e8r8  a8r8b8r8g8r8
e8r8a8r8b8r8a8r8
e8r8a8r8b8r8g8r8
>d8r8c8r8<b8r8a8r8]10
(12)[e8r8a8r8b8r8g8r8]4(13)[d8r8g8r8a8r8f8r8]4
(12)4(13)4
#1 y11
o2
@33 ;@8 $ED $0F $52
v195
p10,25
(6)[e8d8e8g8e8d8r4
e8d8e8g8e8r4^8
e8d8e8g8e8d8r4
c8r8c8r8c8r8d8r8]2
e8d8e8g8e8g8r4
e8d8e8g8e8r4^8
e8d8e8g8b8g8r4
g8f+8g8e8g8r8f+8r8
e8d8e8r8d8r4^8
e8d8e8r8d8r8g8r8
e8d8e8g8e8r8d8r8
c8r8c8r8b8r8a8r8
(7)[e8d8e8d8e8r8g8r8]1
e8d8e8d8e8r8d8r8
(7)1
c8r8c8r8c8r8d8r8(7)1
e8d8e8d8e8r8d8r8(7)1
b8r8a8r8g8r8f+8r8
(9)[g8r8e8r8g8r8e8d8
g8r8e8r8g8r8g8r8]1
(8)[g8r8f+8r8g8r8a8r8]2
(9)1(8)2(6)2

(6)1(15)[d8c8d8f8d8c8r4
d8c8d8f8d8r4^8
d8c8d8f8d8c8r4
<a+8r8a+8r8a+8r8>c8r8]1
(6)(15)

#2
o4
@32
p15,30
v255
[r2]32
<b2^4a8g8
f+2^4e8f+8
g2e2
d2c2
b2^4a8g8
f+2^4>d8c8
<b2e4c8e8
b8r8a8r8g8r8f+8r8
[g4f+4g2]2
b2a2
g2f+2
[g4f+4g2]2
b2a4g4
f+4e4f+4g4
e1^2
r1^1^2
g2e2
d1
g2e2
d2c2

[e2r2^2^2e2r2d4^8<b4^8a4>
d2r2^2^2d2r2]e4^8d4^8g4*e4^8<b4^8e4>

#3
o4
@34
$DE $18 $0B $35 ;@0 $ED $5A $C0
v215 ;p16,20
[e2^4d4]2
e2g2
a2b2
*2
g2$F4$01a2
a2$F4$01^2
*2
b2a2
g4f+4e4d4
*1
e2^4$F4$01a4
a2$F4$01^2
f+1
*2
e2g2
a2b2
e2^4d4
e2^4d4
g2$F4$01a2
a2$F4$01^2
*2
e4g4f+4d4
g4a4a4d4
*2
g2>d4c4
<b2f+2
g2^4f+4
g2^4f+4
g2^4>d4
c4<b4a4f+4
g2^4f+4
g2^4f+4
e1
f+1

(10)[e8r8e4r4c-8d8e8r8e4r2]2
(11)[d8r8d4r4c-8d8c-8r8]c-4r2
*r4b8a8g8d8
(10)2(11)c-4r2
(11)r4<g8a8>c8d8

#5
o4
@16 $ED $7E $F0  $DE $18 $0F $30 ;p20,25 ; $7F $EF
v0/o4e2^2^2^2[r2]12
v255 e2^4g4
f+4e4d2
e2f+2
g2a2
g2f+2
g2a2
e1
f+2d2
e2^4g4
f+4e4d2
e2f+2
g2a2
g2^4b4
a4g4f+2
g2e4g4
a2f+2
g2^4f+4
g2f+4g4
b1
a1
g2^4f+4
g2f+4g4
>d1
<a2g4f+4
e1
f+4g4a4f+4
e1
f+2d2
e1
f+2d2
e2^4d8e8
f+4g4a4f+4

[e2^4d4e2^4d4e2g2a2b2
d2^4c4d2^4c4d2f2g2a2]2


#4
"j=@30c"
"k=@31c"
o4 v215
j8r2^4^8
j8r2^4^8
j8r1^2^4^8
j8r2^4^8
j8r4^8j8r4^8
j8r4^8j8r8k8r8
j8r4^8j8r4^8

(1)[j8r8j8r8j8r8j8r8]5
j8r8j8r8k8r8k8r8(1)1

j8r8j8j8k8j8k8r8
j8r8k8r8j8r8k8r8
j8r8k8r8j8r8k8j8
(2)[j8j8k8j8]4
(3)[j8r16j16k8j8j8r16j16k8j8
j8r16j16k8j8j8r16j16k8k8]1
[j8r16j16k8j8j8r16j16k8j8
j8r16j16k8j8j8r16j16k16k16k16k16]2
(3)1
(2)4
j8r8k8r8j8r8k8r8
j8r8k8r8j8r8k8j8
(4)[j8r8j8r8k8r8j8r8]1
j8r8j8j8k8j8k8r8
(4)
j8r8j8r8k8r4^8
(4)2
j8r8j8r8j8r8j8r8
j16r4^8^16j16r2^2    
[r2]32

#6 @35 o4

v0/f2^2^2^2v235[r2]28f2^2^2^2*28
f2^2^2^2*12 
f2^2^2^2 *4 
f2^2^2^2 *4
f2^2^2^2 *4 
f2^2^2^2 *4 *28