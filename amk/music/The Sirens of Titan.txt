;Revamped by Dark Mario Bros

#amk=1

#samples
{
	#default
	#AMM
}

#instruments
{
@21 $FF $E0 $FF $07 $40 ;@30
@29 $FF $E0 $B8 $04 $40 ;@31
@23 $FD $A0 $00 $0E $40 ;@32
}

#SPC
{
	#author "Buster Beetle"
	#comment "Abstract/Dream Level 1"
	#game "SMW Central Production 2"
	#title "The Sirens of Titan"
}
$EF $1E $25 $25 
$F1 $08 $4C $01 

#0 t54 w255 l16
@14 $ED$2E$B2 v220
r4^8/
[o2ccccr<a+8>fr2ccccr<a+>r8d+dc<a+>r4
ccccr<a+8>frd+rdr<a+>r8]<g+g+g+a+ra+r8>cccdr<a+>r8*
<g+ra+r>crd+rfrd+rdr<a+r>

[<g+g+g+gg+g+g+>cdddd+fd+c<a+>]crc<a+>crc<a+>crc<a+>cd+r8
*crcd+crc<a+>crcgfd+d<a+>

[ccccgfd+cdddda+agd]ccccgfd+dccccfd+d<a+>
*d+d+d+d+g+gfd+ddddgfd+<a+>

#1 y8 l8
@11 $ED $7E $D3 ;@11 $ED$79$6A
r4^8/
o4cr4gr2cr4frd+rdcr4grg+ra+gfd+fd+dcd
cr4grfrd+crfrd+rdrcr4g+gfd+dd+r4d16d+16f4d4
<g+>r4crd+rfgfr4gfr4grfrd+rdrd+dfdd+da+f
d+r4frgrg+grfrd+rdrcr4grg+ra+gfd+fd+dcd

#2 @0 $ED $7E $A0 v175 l16 q4f
r4^8/
o4(5)[y11cr<a+>cr<a+>cr<a+>cr<a+>drd+r]
(6)[y9cr<a+>cr<a+>cr<a+>cr<ga+>r8.](5)
(7)[y9cr<a+>cr<a+>cr<a+>cr<a+>dr<a+>r]
(5)(6)(5)(7)(5)(6)(5)(7)(5)(6)(5)(7)

#3 @4 $ED $7E $D2 ;$ED $29 $8B 
v125 y12 l8 
r4^8/
o4(1)[cr4dd+dd+f]cr4dd+d<a+>r
(1)g.g+g.f.gd.
(1)cr4dd+d<a+>r
(1)c.<a+>c.d.d+d.
(2)[d+4rd16d+16f.d.<a+>dr16d+.<a+>d.r16d+r]
d+4rd16d+16fr16a+r16dgr16g+.gf.g.d(2)
c.r.g+gfd+dcr16g+g.f.gd.

#4 @4 $ED $7E $D2 ;$ED $29 $8B 
v115 y8
r4^8/
o3(3)[gr4aa+aa+>c<]gr4aa+afr
(3)>d.d+d.c.d<a.
(3)gr4aa+afr
(3)g.fg.a.a+a.
(4)[a+4ra16a+16>c.<a.far16a+.fa.r16a+r]
a+4ra16a+16>cr16fr16<a>dr16d+.dc.d.<a(4)
g.r.>d+dc<a+agr16>d+d.c.d<a.

#5 y10  l8
"j=@30v255c"
"k=@31v200e"
j16j16kr/
o3[jrkr16jr16jkr]8
[j16j16jkj16jj16jkj16j16
jjkjjjkj]4

#6 y10 v145 l16
"m=@32c"
r4^8/
o3[l16m8m8mmmmm8m8mmmm]8
[l8mmmmmmmm]8