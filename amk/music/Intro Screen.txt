#amk=1

#samples {
   #default
   "CCymbal.brr"
   "COboe.brr"
}

#SPC
{
   #title	"SMWC presents..."
   #author	"Torchkas"
   #game	"SMW Central Production 2"
   #comment	"COME ON AND SLAM"
}

#instruments
{
   @1 $83 $E0 $69 $03 $00
   @8 $8E $E0 $69 $1E $00
   @3 $8F $F3 $69 $03 $00
   @3 $8A $F3 $69 $03 $00
   @13 $AF $76 $69 $03 $00
   @13 $AB $76 $69 $03 $00
}

#0
w175t53?0
$EF $FF $00 $00
$FB $84 $60 $01
y10v152@30
o4b1$DD $48 $18 $AD
^2$E8 $60 $2D ^2

#1
y7v152@30
o4e1
^2$E8 $60 $2D ^2

#2
y13v152@30
o3g1$DD $48 $18 $A4
^2$E8 $60 $2D ^2

#3
y12v152@30
o3c1$DD $48 $18 $9D
^2$E8 $60 $2D ^2

#4
y10v248@31$FA $03 $40
o1c2$DD $00 $18 $8C ^4$DD $00 $18 $80 ^4$DD $18 $18 $8C $E8 $C0 $2D ^1 

#5
y7v185@32
$E8 $C0 $FF
o4g4e4b4g4<$ED $0F $F5 a16
@34
v180$E8 $30 $FF y14$DC $24 $06
o4e16.>c16.y6$DC $78 $0A a8.v205a8.v160a8.v105a8.
r1

#6
y7v185@32
$E8 $C0 $FF
r16$F2 $50 $1B $1Br16o4b4g4e4e8
@34
v180$E8 $30 $FF y14$DC $36 $06
o3a16.>g16.>e8^64
@35y14$DC $78 $0A v180a8.v135a8.v90a8.v55a8.

#7
y12v125@33
r16$E8 $C0 $C3 o4g8b8e8g8b8e8g8e16.
@34
v180$E8 $30 $FF y14$DC $36 $06
o4c16.a16.>g1