#amk 2

#SPC
{
	#title "Suspicious Silence"
	#author "Kipernal"
	#comment "Forest/Swamp Level 3"
	#game "SMW Central Production 2"
}

#samples
{
	"default/00 SMW @0.brr"
	"default/01 SMW @1.brr"
	"default/02 SMW @2.brr"
	"default/03 SMW @3.brr"
	"default/04 SMW @4.brr"
	"default/05 SMW @8.brr"
	"default/06 SMW @22.brr"
	"EMPTY.brr"
	"default/08 SMW @6.brr"
	"default/09 SMW @7.brr"
	"EMPTY.brr"
	"default/0B SMW @10.brr"
	"default/0C SMW @13.brr"
	"EMPTY.brr"
	"optimized/0E SMW @29.brr"
	"default/0F SMW @21.brr"
	"default/10 SMW @12.brr"
	"EMPTY.brr"
	"optimized/12 SMW @15.brr"
	"optimized/13 SMW Thunder.brr"
	#AMM
	#sky_and_dream
	"CChoir.brr"
	"shekere.brr"
}

#instruments {
	@8			$AE $B0 $00 $1E $00 ;@30
	@3			$BF $8C $00 $03 $00 ;@31
	@3			$8A $EC $00 $03 $00 ;@31
"shekere.brr"	$00 $00 $60 $04 $C3 ;@30
}

#0 w210 

$EF $FF $25 $25
$F1 $07 $50 $01
$F4 $02
y11@6   t24 o3$DE $00 $0F $22
 (1)[v139 $ED $6D $F5 e4^8^16 $F4 $01 $ED $6D $E2 d16  $ED $6D $F5 e2   $F4 $01   e4^8^16 $F4 $01 $ED $6D $E2 d16 $ED $6D $F5 e8^16  $F4 $01 $ED $6D $E2 g4^16]2
/ (1)

     $ED $6D $F5 e4^8^16 $F4 $01 $ED $6D $E2 d16  $ED $6D $F5 e2   $F4 $01   e4^8^16 $F4 $01 $ED $6D $E2 d16 $ED $6D $F5 g8^16 $ED $6D $E2 $F4 $01 a8^16b8 
 $ED $6D $EE e2g2a2b2
 >e2d2<a2e2
(3)[$ED $6D $F5 e8^16 $ED $6D $E2 e4^16 $ED $6D $F5 e8^16 $ED $6D $E2 e8e8^16    $ED $6D $F5 a8^16 $ED $6D $E2 a4^16  $ED $6D $F5 b8^16 $ED $6D $EE b8] a8^16
    (3) >d8^16
e4d4<a4d4b4a4e2
 v131 e32 v134 ^16 v136 ^32 v139 ^32 v142 ^16 v145 ^32 v148 g32 v151 ^16 v154 ^32 v157 ^32 v160 ^16 v162 ^32 v165 b32 v168 ^16 v171 ^32 v174 ^32 v177 ^16 v180 ^32 v183 >d32 v186 ^16 v189 ^32 v191 ^32 v194 ^16 v197 ^32e1 ;d+

#1
y9@6  o3$DE $00 $0E $22
(2)[v139 <$ED $6D $F5 b4^8^16 $ED $6D $E2 $F4 $01 a16 $ED $6D $F5 b2  $F4 $01    b4^8^16 $ED $6D $E2 $F4 $01 a16 $ED $6D $F5 b8^16> $F4 $01 $ED $6D $E2 d4^16]2
/ (2)

    <$ED $6D $F5 b4^8^16 $ED $6D $E2 $F4 $01 a16 $ED $6D $F5 b2  $F4 $01    b4^8^16 $ED $6D $E2 $F4 $01 a16>$ED $6D $F5 d8^16 $ED $6D $E2 $F4 $01 e8^16f+8 
$ED $6D $EE <b2>d2e2f+2
 b2a2e2<b2
 (4)[$ED $6D $F5 b8^16 $ED $6D $E2 b4^16 $ED $6D $F5 b8^16 $ED $6D $E2 b8b8^16>  $ED $6D $F5 e8^16 $ED $6D $E2 e4^16] $ED $6D $F5 e8^16 $ED $6D $E2 e8e8^16
<(4) $ED $6D $F5 > f+8^16 $ED $6D $E2 f+8a8^16
b4a4e4<b4>f+4e4<b2
 v131 b32 v134 ^16 v136 ^32 v139 ^32 v142 ^16 v145 ^32 v148 >d32 v151 ^16 v154 ^32 v157 ^32 v160 ^16 v162 ^32 v165 e32 v168 ^16 v171 ^32 v174 ^32 v177 ^16 v180 ^32 v183 f+32 v186 ^16 v189 ^32 v191 ^32 v194 ^16 v197 ^32b2^4^4 ;g

#2
@30o2v255 e8^16d8^16<b8>e8^16g8^16d8e8^16d8^16<b8>e8^16g8^16d8
e8^16d8^16<b8>e8^16g8^16d8e8^16d8^16<b8>e8^16g8^16d8
/@30 e8^16d8^16<b8>e8^16g8^16d8e8^16d8^16<b8>e8^16g8^16d8
e8^16d8^16<b8>e8^16g8^16d8e8^16d8^16<b8>g8^16e8^16g8
e16d16<b16g16d8^16>e16g16e16d16<b16g8^16>g16a16g16e16d16<b8^16>a16b16a16f+16d+16<b8^16>b16
>e16d16<b16g16e16d16<b16>d16e16g16b16>d16e8^16d16e16d16<b16g16e16d16<b16>d16e2
[e4d4<b4^16>d8^16a4g4e4^16a8^16]2
e4d4a4b4b4a4e4d4
>e16^32g16^32e16d16^32<b16^32>d16<a16^32b16^32a16d16^32<b16^32>d16b16^32>d16^32<b16a16^32d16^32d+16e4^4

#3
r1^1
^1^1
/y8@31o3v225 (5)[e16g8a4e16e16g8a8 $F4 $01 a16&a+32&a32 $F4 $01 g16e16g8a8e16d16e2^16
          e16g8a4e16e16g8a8 $F4 $01 a16&a+32&a32 $F4 $01 g16e16g8a8e16>e16 $F4 $01 d32d32&d+4^8^16 ^32 $F4 $01 d+32 
e8d16<b8g16e16g16a16 $F4 $01 b32b32&a4^8 $F4 $01 a8g16e8d16<b16> $F4 $01 d32d32&d+4^8^16 $F4 $01 d+16
e8d16<b8g16e16g16a16 $F4 $01 b32b32&a8^16 $F4 $01 >d16e16d16e8d16e8d16e16d16e2
e4^16d16e16d16e4g4a4^8g16e16g16e16 $F4 $01 d32d32&e4^16 $F4 $01]
   e4^16d16e16d16e4g4a4^8>d16< $F4 $01 b32&a32&b2 $F4 $01
>e8^16d8^16<b8a2b8^16a8^16g8e4^8^16d16
e8^16g8^16a8^16b8^16a8d8e8^16g8^16d8e4^4

#4
r1^1
^1^1
/
r16y11@32$EE $30v140
(5)
r1^1^1^1^1^1^1^1^1^1^1^1^1^1^1^1

#5
r1^1
v140@33
[y10q7fo4c16q7cy12c16q7ay8c16q78y10c16q76y12c16q74y8c16r8
y10q7fc16q7cy12c16q7ay8c16q78y10c16q76y12c16q74y8c16r32q76y12c16q79y8c32]2
/
*99

#6
r1^1
^1^1
/w210^1^1
^1^1
^1^1
^1^1
v0 ^1^1
^1^1
^1^1
^2^4@31 v90 o4d32 v110 ^32 v131 ^32 v151 ^32 v174 d+32 v194 ^32 v215 ^32 v235 ^32 v255 e4o2d8d+8e4 $E1 $04 $00 ^4

#7
r1^1
^1^1
/^1^1
^1^1
^1^1
^2^8@31 v162 o2b16>d16e16g16e16d16<b8^16a8^16g8e2
@15 ("CChoir.brr", $0A) $ED $44 $A3 o4v104 d1g1
d1g2a2
$ED $45 $A3 d4c4g4c4a4>c4<a2
d4f4a4>c8c+8d2^4^4