(last update: 2020-11-15)

NOTE: Due to various edits to many files, a few of them might have bits of info off, but
most comments should still be valid.

----------

60: Industries smoke ExAnimation + OW Switch Palace + Oriental Sea

61: Digsite Dangers quicksand

80: New SMWC Logo/(c) 2011-2021 SMW Central (title screen)/Girder Grassland BG (layer 2 portion) - there's still some room for other stuff

81: Super Mario World Central Production thingy + Refinery BG

82: Animated 2 sprite

83: Magi's Bubbly Clouds ~ Dynamic Disarray BG + Stratus Skyworks FG

84: Norveg Co. title art (part 1)

85: Norveg Co. title art (part 2)

86: SMWCP - Super Mario World Central Production (part 1)

87: SMWCP - Super Mario World Central Production (part 2) + Cave BG + some little cave FG tiles

88: Overworld border tilemap

89: Girder Grassland - "vanilla" grass portion

8A: Actual Girders (variation 1 - grass), Boss door

8B: Generic buttons and indicators and switches and selective blocks... and more forest FG related thingies 

8C: Girder Grassland BG + Ponpon Palace BG, layer 3 version (layer 3 portion)

8D: Girder Grassland (layer 3 tilemap assembly)

8E: ExAnimation (SMWC coin/outline, water/lava falls, On/Off blocks, P-Switch blocks, colored ice blocks, layer 3 switch palace blocks)

8F: GFX02 but with lava trail instead of Wiggler body

90: Oriental/Sky submaps GFX

91: Carnival submap GFX

92: Extra overworld animations (carnival/fire-ice submaps)

93: Factory submap GFX

94: Overworld border tilemap + Layer 3 clouds

95: Fire-Ice submap GFX

96: Cave tileset

97: Mine tileset/decorations

98: Forest BG

99: Far Clouds (layer 3 tilemap assembly)

9A: Far Clouds, Floating Isles, Oriental Castle BGs, tree trunks, crystal/ice BG (layer 3 portions) - there's still some room for other stuff

9B: Light beacons (layer 3 portion)

9C: Light beacons (layer 3 tilemap assembly)

9D: Grassland tileset used in level 5

9E: Oriental Hills BG, Oriental decorations

9F: Switch Palace/Bonus Room BG, Sideshow Showdown progress icons and sprite HUD

A0: Switch Palace/Bonus Room FG, Frostflow Freezer giant snowball

A1: ExAnimation (SMWC coin/outline, valves, sparkles, oriental castle moon reflection, mine lamp)

A2: Industries BG

A3: Switch Palace FG stuff

A4: Bonus Magician sprite - there's still some room for other stuff

A5: Reclaimed Refinery FG, part 1

A6: Reclaimed Refinery FG, part 2 - there's still some room for other stuff

A7: Reclaimed Refinery light beacons + messages (layer 3 tilemap assembly)

A8: Forest BG GFX + Rollercoaster tracks + Crystalline Cave BG GFX + Columns and bricks (layer 3 portion)

A9: Forest BG + Floral Fears message (layer 3 tilemap assembly)

AA: Truntec boss FG

AB: Truntec boss, reskinned Bob-omb and stuff

AC: Beautiful Magi BG - optimized!

AD: Forest trunks, canopy, bridges FG

AE: Beautiful Magi BG (layer 3 tilemap assembly)

AF: Crystalline Cave BG

B0: Crystalline Cave BG (layer 3 tilemap assembly)

B1: Treehouses FG and stuff

B2: Watery/Sandy Falls Cave BG

B3: Mine-themed SP4 set

B4: Manic Mine messages (layer 3 tilemap assembly)

B5: Detailed layer 3 cave BG GFX

B6: Detailed layer 3 cave BG (layer 3 tilemap assembly)

B7: Modified SP4 for Arboreal Ascent #1

B8: Tree trunks (layer 3 tilemap assembly)

B9: Inside Tree, part 1

BA: Inside Tree, part 2

BB: Some Inside Tree, cave and fire-ice stuff

BC: Modified SP3 for Arboreal Ascent

BD: Modified SP4 for Arboreal Ascent #2

BE: Flowey Ghost House FG

BF: Forest BG GFX (clone for layer 2)

C0: SMB3 fog + Floral Fears message (layer 3 tilemap assembly)

C1: Forest Ghost House BG and likely some carnival stuff?

C2: Swamp FG tileset, part 1

C3: Swamp FG tileset, part 2

C4: Swamp FG tileset, part 3

C5: Swamp level SP4 (Pirangler, bubble, Blargg, Crow, etc)

C6: ExAnimation (SMWC coin/outline, swamp leaves, bubbly swamp, sumo flames, small ash, burning canopy)

C7: Swamp level SP4 #2 (Gas bubble, Crow, etc)

C8: Blazing Forest FG stuff - there's still some room for other stuff

C9: Blazing Forest SP4 set

CA: Floating Isle FG (Radiatus Ruins), part 1

CB: Floating Isle FG (Radiatus Ruins), part 2

CC: Forest Castle plants, waterfalls, munchy munchers, brambles FG

CD: Briar Mire/Fire-Ice/whatever SP4 set (Dinos, skull platform, firebar, frostee, etc.)

CE: Forest BG + Columns and bricks (layer 3 tilemap assembly)

CF: Another SP4 set (firebars, Thwomp, Nipper, Magikoopa, etc.)

D0: Alraune ~ Forest bossy SP3

D1: Alraune ~ Forest bossy SP4

D2: Sunrise forest FG

D3: Bute SP3

D4: Fire-Ice BG

D5: Oriental Castle FG

D6: Fire-Ice BG part 2 (could be merged into 1?), Fire-Ice FG stuff

D7: Fire-Ice cave, Aurora borealis, Digsite Dangers BG (layer 3 portion)

D8: Fire-Ice cave + Akarui Avenue message (layer 3 tilemap assembly)

D9: Oriental forest FG (springs)

DA: Oriental SP3 (ninja chuck, air meter, etc.)

DB: Oriental SP4 (stone statue head, yinyang rock, etc.)

DC: Neglected Oriental FG decorations

DD: ExAnimation (SMWC coin, Springs, Frostflow Freezer Struggling Koopa) - there's still some room for other stuff

DE: Ponpon FG (also includes ExAnimation, which is pretty economic)

DF: Ponpon SP3

E0: Ponpon Palace BG, layer 3 version (layer 3 tilemap assembly)

E1: Ponpon's big drum

E2: Oriental lakeside BG, part 1

E3: Oriental lakeside BG, part 2

E4: Oriental lakeside FG

E5: Oriental lakeside SP4 (Jiang Shi, etc)

E6: Translucent Tide (layer 3 tilemap assembly)

E7: Hydrostatic/Factory-ish thingies, Blimp BG

E8: Actual Girders, submerged version, plus sand, plus reef stuff

E9: Rocky Desert FG

EA: Snorkel Ninji SP4

EB: Underwater BG - there's still room for other stuff

EC: Goldfish + Togepuku Tussle message (layer 3 tilemap assembly)

ED: Koura Crevice light beacons + messages (layer 3 tilemap assembly)

EE: Koura Crevice SP4 (Yoshi box, etc.)

EF: "Touhou" Temple FG

F0: Tourou Temple BG

F1: Tourou Temple SP4 set (beheads, etc.)

F2: Tourou Temple inside BG (layer 3 tilemap assembly)

F3: Tourou Temple Dragon Ship (part 1) and Behead Statue

F4: Tourou Temple Dragon Ship (part 2)

F5: Tourou Temple lily BG (layer 3 GFX)

F6: Tourou Temple lily BG (layer 3 tilemap assembly)

F7: Stupid Tanuki boss part 1

F8: Stupid Tanuki boss part 2

F9: Stupid Tanuki boss part 3

FA: Beautiful Magi BG + Tides (layer 3 tilemap assembly)

FB: Beach Ball Brawl girders and ball launchers - there's still some room for other stuff

FC: Beach ball, Diggin' Chuck and stuff SP4

FD: New Ice-Floe Inferno/Shivering Cinders BG

FE: Ice-Floe Inferno (ice) FG part 1

FF: Ice-Floe Inferno (fire) FG part 2

100: Ice-Floe Inferno (misc) FG part 3 and generic buttons and indicators and switches and selective blocks...

101: Fire-Ice SP4 sprite set (slidy boy, firebar, spiny, lakitu, dino-torch, blargg, etc.)

102: Ice-Floe/Cinders layer 3 BG tileset (layer 3 GFX)

103: Shivering Cinders BG (layer 3 tilemap assembly)

104: Ice-Floe Inferno BG + Messages (layer 3 tilemap assembly)

105: Shivering Cinders FG part 1

106: Shivering Cinders FG part 2

107: Hammer, Boomerang, Fire and Ice Bros SP3

108: Volcanic Panic BG

109: Modified GFX16

10A: Volcanic Panic SP3

10B: Volcanic Panic SP4

10C: Akarui Avenue FG lamps

10D: Onsen Overhang's revised FG set

10E: Abandoned Factory/Rusted Retribution BG (layer 3 tilemap assembly)

10F: Abandoned Factory/Rusted Retribution BG (layer 3 GFX)

110: Crumbling Catacombs FG, part 3 + Sandfall ExAnimation

111: Crumbling Catacombs FG, part 1

112: Crumbling Catacombs cave BG (layer 3 tilemap assembly)

113: Crumbling Catacombs bricks BG (layer 3 tilemap assembly)

114: Crumbling Catacombs cave layer 2 stuff

115: Crumbling Catacombs bricks and layer 3 cave BG (layer 3 tilemap) - there's still room for other stuff

116: Crumbling Catacombs FG, part 2

117: Crumbling Catacombs SP4 (spiny, blargg, panser, etc.)

118: Desert BG, part 1

119: Desert BG, part 2 + Desert FG parts

11A: Hydrostatic Halt FG2

11B: Hydrostatic Halt BG, part 1

11C: Hydrostatic Halt BG, part 2

11D: Hydrostatic Halt BG, part 3

11E: Girders (underwater, used in Hydrostatic Halt), small parts of the Old Beach Ball Brawl BG

11F: EREHPS EHT RAEF ExAnimation - there's still some room for other stuff

120: Old Beach Ball Brawl BG, part 1

121: Old Beach Ball Brawl BG, part 2

122: Old Beach Ball Brawl BG, part 3

123: Coral Corridor FG - there's still some room for other stuff

124: Coral Corridor/Stone Sea Sanctuary external BG - there's still some room for other stuff

125: Crocodile Crossing FG, part 1 + BG portion

126: Crocodile Crossing FG, part 2

127: Crocodile Crossing FG, part 3 + BG portion

128: Crocodile Crossing SP3 (sandcroc croco reskin, etc.)

129: Crocodile Crossing SP4 (spiny reskin, croco blargg, etc.)

12A: Crocodile Crossing Cave/Water FG

12B: Crocodile Crossing - Croco Jungle FG, part 1 - there's still some room for other stuff

12C: Crocodile Crossing - Croco Jungle FG, part 2

12D: ExAnimation (Crocodile, switch blocks, P-blocks, SMWC coin, etc) - there's still some room for other stuff

12E: Crocodile Crossing FG, final sublevel

12F: Layer 3 water, calm edition (layer 3 tilemap assembly)

130: Seaside Spikes FG

131: Stone Sea Sanctuary FG

132: Akarui Avenue/Onsen Overhang sun and clouds (layer 3 tilemap assembly)

133: Stone Sea Sanctuary jellyfish and stuff (FG)

134: Stone Sea Sanctuary SP3 set (crumbled Dry Bones, Cheep-Cheep, etc.)

135: Stone Sea Sanctuary SP4 set (Super Koopa, Ball 'n' Chain, Bony Beetle, etc.)

136: Stone Sea Sanctuary SP3 set 2 (Motor, etc.)

137: Jelectro + projectiles - SP3 set

138: Updated Shenmi Frica-Sea FG

139: Shenmi SP4 set (porcupuffer, jiang shi, reskinned gau)

13A: Shenmi Vases and Columns

13B: Shenmi Temple FG, part 2 

13C: Shenmi Underwater BG

13D: Shenmi Temple FG, part 1

13E: SMB3 fog, no message (layer 3 tilemap assembly)

13F: Jewel Jubilation/Scorching Sepulcher ExAnimation (ropes, rocks, multi-palette animation etc.)

140: Jewel Jubilation FG, part 1

141: Jewel Jubilation FG, part 2

142: Jewel Jubilation SP2 set (microgoomba)

143: Jewel Jubilation SP3 set (fire chomp, etc.)

144: Jewel Jubilation SP4 set (annoying fire-spitting flame, etc.)

145: Sunset Split BG, part 1

146: Sunset Split FG

147: Myrkky Mainline FG, Opulent Oasis cursed machine placeholder

148: Opulent Oasis FG

149: Carnival/Ferris Wheel + Tents BG (downgraded, part 1)

14A: Playground Bound FG, part 1

14B: Playground Bound FG, part 2

14C: Carnival SP1 set

14D: Carnival SP2 set

14E: Playground Bound SP3 set (snifit, etc.)

14F: Playground Bound SP4 (mega mole as an ice cream car)

150: Scorching Sepulcher SP3 set (thermometer)

151: Emerald Escapade/Smoldering Shrine FG

152: Emerald Escapade SP4 set (evil cat mummy, etc.)

153: Emerald Escapade SP3 set (dry bones, sandcroc)

154: Emerald Escapade exterior BG (layer 3 tilemap assembly)

155: Lava Lift Lair FG

156: Crystalline Citadel FG, part 1

157: Crystalline Citadel BG - there's still room for other stuff

158: Crystalline Citadel/Emerald Escapade layer 3 BG (layer 3 tilemap assembly)

159: Emerald Escapade SP2 set (arrows, jewel that unlocks blocks)

15A: Digsite Dangers quicksand, part 2, button switch, Dynamic Disarray propeller - there's still room for other stuff

15B: Digsite Dangers SP4 set (evil cat mummy, etc.)

15C: Digsite Dangers indoors BG (layer 3 tilemap assembly)

15D: Balloon Bonanza FG

15E: Old Madame Mau

15F: Carnival Caper FG + BG (downgraded, part 2)

160: Carnival Caper SP3 set (torpedo ted reskin, etc)

161: Carnival/Mushroom Grassland FG set

162: Circus tents, wooden platforms FG

163: Balloon Bonanza SP4 set (balloon, etc.)

164: Balloon Bonanza BG (might be replaced)

165: Playground Bound FG, part 1 (letters, Mario, etc.)

166: Playground Bound FG, part 2 (moar letters, numbers, megadmin, etc.)

167: Playground Bound SP2 set (HUD icons)

168: Coaster Commotion BG

169: Wooden Coaster FG, part 1

16A: Wooden Coaster FG, part 2

16B: Coaster Commotion SP4 set (player coaster, train and enemies)

16C: Coaster Commotion SP3 set (sleepy thing)

16D: Coaster Commotion SP4 set 2 (magikoopa, sleepy thing, nonsense enemy)

16E: Recolored GFX2A for Coaster Commotion because the BG sucks with palette optimization

16F: Fantastic Elastic/Rusted Retribution BG, Fantastic Elastic elastic floor - there's still room for other stuff

170: Fantastic Elastic/Rusted Retribution FG

171: Fantastic Elastic SP4 set (derpy happy thwomp, etc.)

172: Hartfowl boss SP2

173: Hartfowl boss SP3

174: Hartfowl boss SP4

175: Briar Mire FG part 2: extra decorations

176: Callous Cliffs layer 1 clouds, Volcanic Panic ledges - PLENTY OF ROOM HERE

177: Callous Cliffs layer 3 clouds (layer 3 tilemap assembly)

178: Honeydew Hills BG, part 1

179: Cloudy Screen (layer 3 tilemap assembly)

17A: Cumulus Chapel FG (mecha)

17B: Tropopause Trail/Stormy Stronghold SP3 set (parabeetles, etc.)

17C: Aerotastic Assault/Stormy Stronghold SP4 set (Jeptak, etc.)

17D: Tropopause Trail/Cumulus Chapel World BG

17E: New Clouds FG

17F: Dynamic Disarray SP4 set (sky pop, etc.)

180: Goonie. Extinct. Too bad.

181: Cumulus Chapel SP3 set (plane, giant skull)

182: Cumulus Chapel SP4 set (bomb, etc.)

183: Tropopause Trail/Cumulus Chapel World (layer 3 GFX)

184: Dangling Danger SP4 set (Roviclaw)

185: Rusted Retribution SP4 set (ghostly spike, dark room spotlight, etc.)

186: Floating Isle FG (Radiatus Ruins), part 3

187: Tropopause Trail World (layer 3 tilemap assembly)

188: Cumulus Chapel World (layer 3 tilemap assembly)

189: Aerotastic Assault/Dangling Danger (intro)/Stratus Skyworks BG

18A: Artillery Auberge/Reverse Universe (jolly) BG (layer 3 tilemap assembly)

18B: Artillery Auberge FG (cannons)

18C: Gliding Garrison/Dangling Danger FG, part 1

18D: Gliding Garrison/Dangling Danger FG, part 2

18E: Dangling Danger SP4 set (roviclaw, etc.)

18F: Anti-flicker measure for Velocity Valves, in case it decides to happen again (layer 3 tilemap assembly)

190: (temporary) DrT's Smoggy Steppe/Stratus Skyworks FG - there's still some room for other stuff (reserved for him, though)

191: Carnival Caper/New Hartfowl + SMWC Coin ExAnimation - there's still some room for other stuff

192: Playground Bound BG/Fantastic Elastic outside + Lava Lift Lair stuff (layer 3 GFX)

193: Playground Bound BG/Fantastic Elastic outside (layer 3 tilemap assembly)

194: Lava Lift Lair BG, layer 2 portion

195: Lava Lift Lair BG (layer 3 tilemap assembly)

196: Frigid-Fried Frenzy BG, layer 2 portion

197: EREHPS EHT RAEF falling zone BG (layer 3 GFX) - there's still some room for other stuff

198: Frigid-Fried Frenzy BG (layer 3 tilemap assembly)

199: Frostflow Freezer SP3 set (snowball, ice skates)

19A: Frostflow Freezer SP4 set (reskinned firebar, recolored sparky, icicle, etc.)

19B: Crystalline Citadel FG, part 2

19C: Crystalline Citadel SP4 set (Panser, Frostee)

19D: Pyro-Cryo Castle BG

19E: Pyro-Cryo Castle FG

19F: Pyro-Cryo Castle/Rusted Retribution ExAnimation (waterfalls, lava, solid lava, level palette, conveyor, etc.)

1A0: Frank boss SP3

1A1: Frank boss SP4

1A2: Rusted Retribution FG

1A3: Rusted girders

1A4: Incineration Station BG

1A5: Incineration Station FG, part 1

1A6: Incineration Station FG, part 2

1A7: Petroleum Passage ExAnimation (background cogwheels, conveyor belts, etc.)

1A8: Elite Cyberkoopa (SP3)

1A9: Incineration Station BG + messages (layer 3 tilemap assembly)

1AA: Petroleum Passage BG

1AB: KooPhD (SP4)

1AC: Incineration Station ExAnimation (BG, flames)

1AD: Petroleum Passage decorations and spikes, (temp) Musical Mechanisms tileset, part 1

1AE: Musical Mechanisms ExAnimation (highly subject to change: electric rays, digital beat counter, etc.)

1AF: Myrkky Mainline BG (layer 3 GFX) - there's still some room for other stuff

1B0: Myrkky Mainline BG (layer 3 tilemap assembly)

1B1: Myrkky Mainline SP4 set (thwomp, dolphin, ugly old spiny, etc.)

1B2: Perilous Paths FG (main area), part 1

1B3: Perilous Paths FG (main area), part 2

1B4: Perilous Paths FG (main area), part 3

1B5: Monotone Monochrome FG, part 1 + Recolored forest BG (Reverse Universe)

1B6: Monotone Monochrome FG, part 2 + Perilous Paths mainframe computer ExAnimation

1B7: Perilous Paths grid BG, part 1 (layer 3 GFX)

1B8: Perilous Paths grid BG, part 2 (layer 3 GFX)

1B9: Perilous Paths grid BG (layer 3 tilemap assembly)

1BA: Kipernal's Mecha FG, part 1 (it sucks tbh) - there's still some room for other stuff

1BB: Kipernal's Mecha FG, part 2 (it sucks tbh) - there's still some room for other stuff

1BC: Perilous Paths SP4 set (platform control: default castle SP4 with rotodisc)

1BD: Reverse Universe FG

1BE: Reversed Reverse Universe BG + message (layer 3 tilemap assembly)

1BF: Reverse Universe SP1 set

1C0: Reverse Universe SP3 set

1C1: Reverse Universe SP4 set

1C2: Candy Calamity BG + message (layer 3 tilemap assembly)

1C3: Candy Calamity BG, part 1

1C4: Candy Calamity BG, part 2 + FG, part 1

1C5: Candy Calamity FG, part 2

1C6: Crystal Conception BG

1C7: Crystal Conception FG, part 1

1C8: Crystal Conception FG, part 2

1C9: Crystal Conception SP3 set

1CA: Crystal Conception ExAnimation (crystals, pillars, etc.) - there's still room for other stuff

1CB: Crystal Conception BG (layer 3 GFX)

1CC: Crystal Conception/EREHPS EHT RAEF BG (layer 3 tilemap assembly)

1CD: Bedazzling Bastion FG + BG

1CE: Frivolous Fires FG decorations

1CF: Frivolous Fires SP4 set (spiny, rock platform, etc.)

1D0: EREHPS EHT RAEF SP3 set 1 - intro

1D1: EREHPS EHT RAEF SP4 set 1 - intro

1D2: EREHPS EHT RAEF falling zone BG (layer 3 tilemap assembly)

1D3: EREHPS EHT RAEF BG freefall sublevel effect (layer 3 tilemap assembly)

1D4: EREHPS EHT RAEF SP3 set 2 (reskinned Elite Koopa aka Pioluigi)

1D5: Asteroid Antics ExAnimation (lasers, poisonous goo) - there's still some room for other stuff

1D6: Sideshow Showdown/Coaster Commotion layer 3 BG (layer 3 tilemap assembly)

1D7: Overworld - ExAnimation

1D8: Terrifying Timbers SP4 set (fake gravestone)

1D9: Cheater room text (layer 3 tilemap assembly)

1DA: Hindenbird BG (old, optimized)

1DB: Hindenbird Boss SP3 set

1DC: Hindenbird Boss SP4 set