PRINT "MAIN ",pc
	PHB		;\
	PHK		; | Change the data bank to the one our code is running from.
	PLB		; | This is a good practice.
	JSR SPRITECODE	; | Jump to the sprite's function.
	PLB		; | Restore old data bank.
PRINT "INIT ",pc
	RTL		;/ And RETURN.

;===================================
;Sprite Function
;===================================

Tilemap:
db $BC,$BC,$BC,$BE	;gold, silver, bronze, dq

Properties:
db $39,$3B,$3D,$39

SPRITECODE:
LDA #$00
%SubOffScreen()
%GetDrawInfo()

PHX
LDA !7FAB40,x
TAX

LDA $7F9A91,x
BNE +
PLX
RTS		;if zero no graphics for you

+
DEC		;decrement to read correct byte from tables
STA $02

REP #$20
LDA $00
STA $0300|!Base2,y
SEP #$20

LDX $02
LDA Tilemap,x
STA $0302|!Base2,y
LDA Properties,x
STA $0303|!Base2,y
PLX

LDY #$02
LDA #$00
JSL $01B7B3|!BankB
RTS