PROPERTIES: db $CD,$8D
TILEMAP: db $80,$82
TILEMAP1234: db $84,$88

ShellSpd:
db $28,$D8

print "INIT ",pc
LDA !D8,x
SEC
SBC #$02
STA !D8,x
LDA !14D4,x
SBC #$00
STA !14D4,x

%SubHorzPos()
TYA
STA !157C,x
STZ !1FD6,x
RTL

print "MAIN ",pc
PHB		
PHK		
PLB		
JSR SPRITECODE	
PLB		
RTL	

FLIP:
LDA !157C,x
EOR #$01
STA !157C,x
BRA COMEBACKNOW

RETURN: 
RTS

CHECKFORPROX:
STZ !AA,x
JSR PROXIMITY
BEQ LALALACOMEBACK3
INC !1FD6,x
BRA COMEBACKNOW

SPRITECODE:
	LDA !14C8,x			
	CMP #$08			
	BNE RETURN
	LDA $9D				
	BNE RETURN			
	
	LDA #$00
	%SubOffScreen()

	LDA !1588,x 
	AND #$03
	BNE FLIP	
COMEBACKNOW:
LDA !1FD6,x
BEQ CHECKFORPROX
; dropping
STZ !B6,x
BRA UPDATEDSPEED

LALALACOMEBACK3:
	LDY !157C,x	
	LDA XXXSPD,y
	STA !B6,x	
UPDATEDSPEED:
LDA !1588,x
AND #$04
BNE HITTHEFLOOR
JSR YOMAMA
JSL $01802A|!BankB
	JSL $018032|!BankB			
	JSL $01A7DC|!BankB
	BCC NOTHINGHAPPENED
	LDA #$09
	STA !14C8,x
	LDA #$11
	STA !9E,x
	JSL $07F7D2|!BankB
;	LDA #$02
;	STA !14C8,x
;	LDA #$D0
;	STA !AA,x
;	LDA #$08
;	STA !B6,x
;	JSL $01802A|!BankB
	NOTHINGHAPPENED:
RTS

HITTHEFLOOR:
JSR ASKUHDUAKHDUAHSDHJHDSKJAH		;nice labels bro
RTS

SPEEDXY: db $10,$F0
YYYYS: db $F0,$F0
XXXSPD: db $08,$F7
;====================================================================================================

YOMAMA:
	%GetDrawInfo()
	
	LDA !157C,x
	STA $02			

	REP #$20
	LDA $00			
	STA $0300|!Base2,y		
	SEP #$20		

LDA !1FD6,x
BNE HHHUHUIKKUJK
	PHX
	LDA $14
	LSR
	LSR
	LSR
	AND #$01
	TAX
	LDA TILEMAP,x
	STA $0302|!Base2,y

	LDX $02			
	LDA PROPERTIES,x
	ORA $64
	STA $0303|!Base2,y		
	PLX	
BRA THISISTHEUPLAODER
HHHUHUIKKUJK:
	PHX
	LDA $14
	LSR
	LSR
	LSR
	AND #$01
	TAX
	LDA TILEMAP1234,x
	STA $0302|!Base2,y

	LDX $02			
	LDA PROPERTIES,x
	ORA $64
	STA $0303|!Base2,y		
	PLX

THISISTHEUPLAODER:
	LDY #$02		
	LDA #$00		

	JSL $01B7B3|!BankB
	RTS

PROXIMITY:		; If sprite is TOO close to Mario, RETURN.
	LDA !14E0,x
	XBA
	LDA !E4,x
	REP #$20 ; Get sprite's X position.
	SEC
	SBC $94	; Subtract Mario's to get the difference range.
	SEP #$20
	PHA
	%SubHorzPos() ; Determine sprite range.
	PLA
	EOR INVERTABILITY,y ; Apply inversion based on direction.
	CMP #$09 ; Range.
	BCS OUTOFRANGE
	LDA #$01
	RTS

OUTOFRANGE:
	LDA !E4,x
	CMP $94
	BNE RANGEOUT
	LDA #$01
	RTS

RANGEOUT:
	LDA #$00
	RTS

INVERTABILITY:
	db $FF,$00
	
ASKUHDUAKHDUAHSDHJHDSKJAH:
;	LDA $14
;	AND #$7F
;	ORA $9D
;	BNE RETURN2
;	JSL $02A9DE
;	BMI RETURN2
;	PHX
;	TYX
	LDA #$01
	STA !14C8,x
	LDA #$11
	STA !9E,x
	JSL $07F7D2|!BankB

;blindedit: added code so it turns into a spinning buzzy beetle shell state
	LDA #$0A
	STA !14C8,x
	%SubHorzPos()
	LDA ShellSpd,y
	STA !B6,x
	LDA !15F6,x
	ORA #$80
	STA !15F6,x

;	TXY
;	PLX
;	LDA $E4,x
;	STA $00E4,y
;	LDA $14E0,x
;	STA $14E0,y
;	LDA $D8,x
;	STA $00D8,y
;	LDA $14D4,x
;	STA $14D4,y
;RETURN2:
	RTS