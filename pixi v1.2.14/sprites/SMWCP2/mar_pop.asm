;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; 16x32 Marine Pop -                                              ;
;           Small Marine Pop sprite that uses Mario's collision.  ;
;           Tries to avoid collision errors.                      ;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


!TEMP = $09

;!TORPEDO SPRITE NUMBER!
!PROJECTILE = $A8

;generated xspeed
!GENSPEEDX = $50

;gen offsets
!GENPOSX	=	$0004
!GENPOSY	=	$000E

!IN_FLIGHT = $1510

PRINT "INIT ",pc
;LDA $94
;STA $E4,x
;LDA $95
;STA $14E0,x
;LDA $96
;STA $D8,x
;LDA $97
;STA $14D4,x
stz !IN_FLIGHT,x
RTL

PRINT "MAIN ",pc
PHB
PHK
PLB
JSR SPRITE_ROUTINE
PLB
RTL

SPRITE_ROUTINE:
lda !IN_FLIGHT,x
bne FLYING
    jsr SUB_GFX_PRE_FLIGHT
    jsl $81A7DC
    bcc NOTTOUCHINGMARIO
        LDA $94
        STA $E4,x
        LDA $95
        STA $14E0,x
        LDA $96
        STA $D8,x
        LDA $97
        STA $14D4,x
        lda #$01
        sta !IN_FLIGHT,x
    NOTTOUCHINGMARIO:
    rts
FLYING:
JSR SUB_GFX
LDA #$01		;Load 1 into the accumulator
STA $1404		;Flag for free will scrolling
LDA #$1C
STA $78

LDA $9D
BNE RETURN_2

LDA $1DF9		;block swimming sound
CMP #$0E
BNE NOSWIMSOUND
STZ $1DF9		;zero sound

NOSWIMSOUND:
LDA $1DFA		;block jumping sound
CMP #$01
BNE NOJUMPSOUND
STZ $1DFA		;zero sound

NOJUMPSOUND:
STZ $148F		;mario never caries anything
STZ $13E8		;disable cape spin
STZ $73

LDA $1DF9		;block cape spin sound
CMP #$0F
BNE NOCAPESOUND
STZ $1DF9		;zero sound

NOCAPESOUND:
LDA $1DF9		;block spin jump sound
CMP #$02
BNE NOSPINSOUND
STZ $1DF9		;zero sound

NOSPINSOUND:
PHY
LDY #$00		;loop index
EXTENDEDLOOP:
LDA $170B,y
CMP #$05		;test mario fireball
BNE NOTFIREBALL
LDA #$00		;zero slot
STA $170B,y
NOTFIREBALL:
INY			;next ex sprite
CPY #$0A		;10 sprites
BNE EXTENDEDLOOP	;loop
PLY

LDA #$01
STA $76
LDA #$20
STA $13E0

LDA $15
AND #$01
BEQ NOT_RIGHT

LDA #$15
STA $7B
LDA $77
AND #$01
BNE NOT_HORZ
LDA $77
AND #$10
BNE NOT_HORZ
BRA CHEK_VERT

NOT_RIGHT:
LDA $15
AND #$02
BEQ NOT_HORZ
BRA MOVE

RETURN_2:
BRA RETURN

MOVE:
LDA #$EC
STA $7B
LDA $77
AND #$02
BNE NOT_HORZ
LDA $77
AND #$10
BNE NOT_HORZ
BRA CHEK_VERT

NOT_HORZ:
STZ $7B

CHEK_VERT:
LDA $15
AND #$04
BEQ NOT_DOWN

LDA #$15
STA $7D
BRA MOV_MAR

NOT_DOWN:
LDA $15
AND #$08
BEQ NOT_VERT

LDA $97
BMI NOT_VERT		;block off the player - disallow going too far above the screen

LDA #$EC
STA $7D
BRA MOV_MAR

NOT_VERT:
STZ $7D

MOV_MAR:
LDA $94
STA $E4,x
LDA $95
STA $14E0,x
LDA $96
CLC
ADC #$01
STA $D8,x
LDA $97
ADC #$00
STA $14D4,x

LDA $13
AND #$03
BEQ CNTU_FIRE
INC $C2,x
LDA $C2,x
CMP #$03
BNE CNTU_FIRE
STZ $C2,x

CNTU_FIRE:
LDA $16
AND #$40
BNE SHOOT
RETURN:
RTS

SHOOT:
LDA $1558,x
BNE RETURN
PHX
PHY
LDY #$00
LDX #$0B
DUPELOOP:
LDA $7FAB9E,x
CMP #!PROJECTILE  ;Assuming you are using TRASM
BNE NOTDUPE
LDA $14C8,x
CMP #$08
BNE NOTDUPE
INY
CPY #$03
BCS DONTSPAWNTORP
NOTDUPE:
DEX
BPL DUPELOOP
PLY
PLX

LDA #$08	;shoot delay
STA $1558,x

;generate projectileGenCust
	JSL $02A9DE		;grab free sprite slot low priority
	BMI NOFIRE		;RETURN if none left before anything else...

	LDA #$06		;make fireball sound
	STA $1DFC

	JSR SETUPCUST		;run sprite generation routine
	NOFIRE:
	BRA RETURN

SETUPCUST:
	LDA #$08
	STA $14C8,y		;normal status, run init routine
	LDA #!PROJECTILE

	PHX
	TYX
	STA $7FAB9E,x		;into sprite type table for custom sprites
	PLX
	
;set positions accordingly		
;restore gen sprite index

	LDA $14E0,x		;create base 16bit pos
	XBA
	LDA $E4,x

FACINGRIGHT:
	LDA $14E0,x		;base xpos
	XBA
	LDA $E4,x
	REP #$20
	CLC
	ADC.w #!GENPOSX		;add
	SEP #$20
	STA $00E4,y		;store
	XBA
	STA $14E0,y
	
	LDA $14D4,x		;base ypos
	XBA
	LDA $D8,x
	REP #$20
	CLC
	ADC.w #!GENPOSY		;add
	SEP #$20
	STA $00D8,y		;store
	XBA
	STA $14D4,y

	TYX			;new sprite slot into X
	JSL $07F7D2		;create sprite
	JSL $0187A7
        LDA #$88		;set as custom sprite
        STA $7FAB10,X

;set projectile speed according to Mario
	LDX $15E9		;restore sprite index

	LDA #!GENSPEEDX
	STA $00B6,y		;set xspeed

PHX
PHY
DONTSPAWNTORP:
PLY
PLX
	RTS			;RETURN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; GRAPHICS ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!EXTRA_BITS = $7FAB10
TILES:
    db $EC,$EE

XOFF:
    db $F8,$08

YOFF:
    db $10,$10

PROPERTIES:
    db $00,$00

;TILETOGET: 
;	db $CC,$CC,$CC,$CC
;TILETOGET2:
;	db $Ec,$CE,$EE,$CE
;TILETOGET3:
;	db $EC,$BC,$EE,$EC
;TILETOGET4:
;	db $AA,$AC,$EC,$AC

    SUB_GFX_PRE_FLIGHT:
        %GetDrawInfo()
        lda $15F6,x
        sta $06
        phx
        ldx #$01
        GFX_LOOP1:
            lda XOFF,x
            clc
            adc $00
            sta $0300,y
            lda YOFF,x
            clc
            adc $01
            sec
            sbc #$10
            sta $0301,y
            lda TILES,x
            sta $0302,y
            LDA $06
            ORA $64
            STA $0303,y
            iny
            iny
            iny
            iny
            dex
            bpl GFX_LOOP1
        plx
        ldy #$02
        lda #$01
        jsl $01B7B3
        rts

	SUB_GFX:
	%GetDrawInfo()
	LDY #$0C
    lda $15F6,x
    sta $06
    phx
    ldx #$01
    GFX_LOOP2:
        stz $07
        lda XOFF,x
        clc
        adc $00
        sta $0200,y
        lda $19
        beq NOTBIG
            lda $0200,y
            inc
            sta $0200,y
        NOTBIG:
        lda $0200,y
        cmp #$F8
        bcc NOTOFFSCREEN
        OFFSCREEN:
            lda #$01
            sta $07
        NOTOFFSCREEN:
        lda YOFF,x
        clc
        adc $01
        sta $0201,y
        lda TILES,x
        sta $0202,y
        LDA $06
        ORA $64
        STA $0203,y
        phy
        tya
        lsr
        lsr
        tay
        phx
        ldx $15E9
        lda #$02
        ora $15A0,x
        ora $07
        sta $0420,y
        plx
        ply
        iny
        iny
        iny
        iny
        dex
        bpl GFX_LOOP2
    plx
    ;lda #$01
    ;ldy #$02
    ;jsl $01B7B3
    rts
	;LDA $157C,x
	;STA $02
	;LDA !EXTRA_BITS,x
	;AND #$04
	;BNE OTHER_TILE

	;LDA $00
	;STA $0200,y
	;LDA $01
	;STA $0201,y

	;PHY
	;LDY $19
	;LDA TILETOGET,y
	;BRA SET1

	;OTHER_TILE
	;LDA $00
	;CLC
	;ADC #$10
	;STA $0200,y
	;LDA $01
	;CLC
	;ADC #$10
	;STA $0201,y
	;PHY
	;LDY $C2,x
	;LDA TILETOGET4,y
	;SET1
	;PLY
	;STA $0202,y
	;LDA $15F6,x
	;ORA $64
	;STA $0203,y

	;PHY
	;TYA ;
	;LSR ;
	;LSR ; Set to 16x16 tile
	;TAY ;
	;LDA #$02 ;
	;ORA $15A0,x ; horizontal OFFSCREEN flag
	;STA $0420,y ;
	;PLY
	;INY
	;INY
	;INY
	;INY


	;LDA !EXTRA_BITS,x
	;AND #$04
	;BNE OTHER_TILE2

	;LDA $00
	;STA $0200,y
	;LDA $01
	;CLC
	;ADC #$10
	;STA $0201,y
	;PHY
	;LDY $C2,x
	;LDA TILETOGET2,y
	;BRA SET2

	;OTHER_TILE2
	;LDA $00
	;STA $0200,y
	;LDA $01
	;CLC
	;ADC #$10
	;STA $0201,y
	;PHY
	;LDY $C2,x
	;LDA TILETOGET3,y

	;SET2
	;PLY
	;STA $0202,y
	;LDA $15F6,x
	;ORA $64
	;STA $0203,y

	;PHY
	;TYA ;
	;LSR ;
	;LSR ; Set to 16x16 tile
	;TAY ;
	;LDA #$02 ;
	;ORA $15A0,x ; horizontal OFFSCREEN flag
	;STA $0420,y ;
	;PLY
	;RTS