PRINT "MAIN ",pc
	PHB		;\
	PHK		; | Change the data bank to the one our code is running from.
	PLB		; | This is a good practice.
	JSR SPRITECODE	; | Jump to the sprite's function.
	PLB		; | Restore old data bank.
PRINT "INIT ",pc
	RTL		;/ And RETURN.

;===================================
;Sprite Function
;===================================

SPRITECODE:	JSR GRAPHICS
		LDA #$00
		%SubOffScreen()

		LDA $14C8,x		;\
		CMP #$08		; | If sprite dead,
		BNE RETURN		;/ RETURN.
		LDA $9D			;\
		BNE RETURN		;/ If locked, RETURN.

		JSL $81A7DC
		BCC NOCONTACT

HURT_BALL:	JSL $80F5B7

DESTROYSPRITE:	LDA #$04
		STA $14C8,x
		LDA #$1B
		STA $1540,x
RETURN: 	RTS

NOCONTACT:
		JSL $81801A
		JSL $818022
		RTS

;===================================
;GRAPHICS Code
;===================================

TILEMAP: 	db $08,$0A,$0C,$0E
GRAPHICS:	%GetDrawInfo()	 	; Before actually coding the GRAPHICS, we need a routine that will get the current sprite's value 
			 	 	; into the OAM.
			  		; The OAM being a place where the tile data for the current sprite will be stored.
		LDA !1528,x
		STA $03

		LDA $157C,x
		STA $02			; Store direction to $02 for use with property routine later.

		LDA $00			;\
		STA $0300,y		;/ Draw the X position of the sprite

		LDA $01			;\
		STA $0301,y		;/ Draw the Y position of the sprite

		PHX
		LDA $14
		LSR
		LSR
		LSR
		AND #$01
		CLC
		ADC $03
		TAX
		LDA TILEMAP,x
		STA $0302,y
		PLX			; Pull back X.

		LDA #$03	
		ORA $64
		STA $0303,y		; Store to properties byte.

		LDY #$02		; Y ends with the tile size .. 02 means it's 16x16
		LDA #$00		; A -> number of tiles drawn - 1.
					; I drew only 1 tile so I put in 1-1 = 00.
	
		JSL $81B7B3		; Call the routine that draws the sprite.
		RTS			; Never forget this!