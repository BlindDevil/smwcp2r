;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Desert boss for SMWCP2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!SandSpriteNumber = $B6		;sand block sprite number
!YarnSpriteNumber = $B5	;ball of yarn sprite number
!SandDropHeight = $0220		;sand block drop height.
!HP = $03		;was 5, then 4, so now it's 3

Phases:;phase table
db $00,$08,$08,$00;each entry corresponds to an HP value
db $00;00 = first, 08 = second phase

IdleTime:		;amount of frames to idle before attacking
db $00,$40,$46,$4A;each entry corresponds to each amount of HP
db $50;

RandomAttacks:	;the first 8 entries are used with the RNG
db $01,$01,$04,$04;to determine what attack to use
db $05,$04,$05,$05;
RandomAttacksPHASE2:		;these 8 entries are for the second phase
db $04,$04,$07,$07;
db $04,$01,$01,$05;

TimerSet:		;DO NOT CHANGE VALUES, ONLY MOVE THEM.
db $04,$04,$10,$10;attack 01 must use value $04, attack 04 must use value $0E,
db $10,$10,$10,$10;and attack 05 must use value $10.
TimerSetPHASE2:	;^, except attack 07 must use value $22
db $0E,$0E,$1E,$1E;
db $0E,$04,$04,$10;

RepeatSet:		;as the name says, this is the number of times to repeat the attack
db $01,$01,$0B,$0B;it's fine to change this
db $0B,$0B,$0B,$0B;
RepeatSetPHASE2:;
db $12,$12,$0C,$0C;
db $12,$01,$01,$00;

SandTimerSet:	;sand block timer table
db $00,$08,$08,$1A;each entry corresponds to a certain HP
db $1C;it is the number of frames before another block is dropped

HurtTime:		;how long the boss is supposed to idle after getting hurt, indexed by HP
db $26,$48,$52,$5C;obviously this should be longer than the values in IdleTime
db $60;first value is reserved for the final blow, do not edit it at all.

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
LDA #$40
STA $154C,x
LDA $14D4,x
STA $160E,x
LDA $D8,x
STA $1602,x
LDA #$08
STA $15AC,x
LDA #!HP
STA $1528,x
RTL

print "MAIN ",pc
PHB
PHK
PLB
JSR SpriteCode
PLB
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Sprite Code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpriteCode:
LDY $1528,x
LDA Phases,y
STA $1534,x

REP #$20
LDA $96
CMP #$02A8
BCC +
SEP #$20
STZ $1412

+
SEP #$20
%SubHorzPos()
TYA
STA $157C,x

JSR GraphicsRoutine

LDA $14C8,x
CMP #$08
BNE .Return

LDA $9D
BNE .Return

JSR CheckShellContact
JSR CheckMarioContact
JSL $819138

LDA $C2,x
JSL $8086DF

.States
dw Idle
dw Burrowing
dw Underground
dw PoppingOut
dw DroppingSand
dw ThrowingSprite
dw Hurt
dw Magic
dw OMGDyingOMG

.Return
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Graphics Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Properties:
db $40,$00

Tilemap:
db $E1,$C1,$A1,$E0,$C0,$A0;idle frame 1
db $E4,$C4,$A4,$E3,$C3,$A3;idle frame 2
db $E7,$C7,$A7,$E6,$C6,$A6;idle frame 3
db $E4,$C4,$A4,$E3,$C3,$A3;idle frame 2
db $EA,$CA,$AA,$E9,$C9,$A9;staff frame 1
db $ED,$CD,$AD,$EC,$CC,$AC;staff frame 2
db $41,$21,$01,$40,$20,$00;throwing frame 1
db $44,$24,$04,$43,$23,$03;throwing frame 2
db $49,$06,$06,$48,$06,$06;mound of sand
db $49,$06,$06,$48,$06,$06 ;db $E4,$E2,$E0,$00,$00,$00		;hurt frame
db $4C,$06,$06,$4B,$06,$06;mound of sand 2

XDisps:
db $00,$00,$00
db $08,$08,$08

db $08,$08,$08
db $00,$00,$00

YDisps:
db $00,$F0,$E0
db $00,$F0,$E0

GraphicsRoutine:
STZ $06

LDA $157C,x
STA $03
ASL
CLC
ADC $03
ASL
STA $08
LDA $15F6,x
STA $04

LDA $1504,x
ASL
STA $02
LDA $1504,x
CLC
ADC $02
ASL
STA $05

%GetDrawInfo()

LDX #$05

.GFXLoop
PHX
TXA
CLC
ADC $08
TAX
LDA $00
CLC
ADC XDisps,x
PLX
STA $0300,y

LDA $01
CLC
ADC YDisps,x
STA $0301,y

PHX
TXA
CLC
ADC $05
TAX
LDA Tilemap,x
STA $0302,y
PLX

PHY
LDY $03
LDA Properties,y
ORA #$30
ORA $04
PLY
STA $0303,y

INY #4
INC $06
DEX
BPL .GFXLoop

LDX $15E9
LDY #$02
LDA $06
DEC A
JSL $81B7B3

RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;	Sprite state codes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Idle:
LDA $154C,x
CMP #$0F
BCS .NotDead

LDA $1528,x
BNE .NotDead

LDA #$08
STA $C2,x
RTS

.NotDead
LDA $15AC,x
BNE .Skip

INC $1504,x
LDA $1504,x
CMP #$04
BNE .SkipZero

STZ $1504,x

.SkipZero
LDA #$08
STA $15AC,x

.Skip
LDA $154C,x
BNE .DontSwitchState

PHX
LDX #$07
JSR GetRand
PLX
CLC
ADC $1534,x
TAY

LDA RandomAttacks,y
CMP #$05
BNE .DontCheck

STZ $00
PHY
LDY #$0B

.CountLoop
LDA $14C8,y
BEQ .Next
INC $00

.Next
DEY
BPL .CountLoop
PLY

LDA $00
CMP #$03
BCC .SameAttack

LDA #$04
BRA .DontCheck

.SameAttack
LDA #$05

.DontCheck
STA $C2,x
LDA TimerSet,y
STA $154C,x
LDA RepeatSet,y
STA $151C,x
STZ $15AC,x

.DontSwitchState
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Burrowing:
LDA $154C,x
BEQ .StopMovingDown

LDA $D8,x
CLC
ADC #$03
STA $D8,x

LDA $14D4,x
ADC #$00
STA $14D4,x
RTS

.StopMovingDown
JSR ShatterEffect
LDA #$10
STA $1887

LDA #$08
STA $1504,x
LDA #$20
STA $154C,x
INC $C2,x
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeedPopOut:
db $18,$E8

Imgs:
db $08,$0A

Underground:
LDA $B6,x
BEQ +

LDA $14
LSR
LSR
AND #$01
TAY
LDA Imgs,y
STA $1504,x

+
LDA $154C,x
BNE .NoXSpeeds

LDA $14E0,x
XBA
LDA $E4,x
REP #$20
SEC
SBC $94
CLC
ADC #$000C
CMP #$0018
SEP #$20
BCS .SkipPopOut

LDA #$B8
STA $AA,x
STZ $B6,x
STZ $1504,x
INC $C2,x
JSR ShatterEffect
LDA #$10
STA $1887

.NoXSpeeds
RTS

.SkipPopOut
JSL $818022
%SubHorzPos()
CPY #$01
BEQ Left

LDA $B6,x
CMP #$18
BCS .Skip

INC $B6,x
INC $B6,x

.Skip
RTS

Left:
LDA $B6,x
BEQ .Go
CMP #$E8
BCC .Skip

.Go
DEC $B6,x
DEC $B6,x

.Skip
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PoppingOut:
LDA $AA,x
BMI .SkipCheck

LDA $14D4,x
CMP $160E,x
BNE .SkipCheck

LDA $D8,x
CMP $1602,x
BNE .SkipCheck

LDA $151C,x
BNE .Repeat

JSR StartIdling
RTS

.Repeat
LDA #$01
STA $C2,x
LDA #$04
STA $154C,x
DEC $151C,x
STZ $B6,x

.SkipCheck
JSL $81802A
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

DroppingSand:
LDA $154C,x
BEQ .DropSand

LDA #$04
STA $1504,x

LDA $151C,x
BNE .SkipEnd
LDA $154C,x
CMP #$01
BNE .SkipEnd

JSR StartIdling
STZ $1594,x

.SkipEnd
RTS

.DropSand
LDA $1887
BNE +

LDA #$10
STA $1887

+
LDA #$05
STA $1504,x

LDA $151C,x
BNE .Skip

LDA #$11
STA $154C,x

.Return
RTS

.Skip
LDA $15AC,x
BNE .Return

JSL $02A9DE
BMI .Return
PHX
TYX
LDA #$08
STA $14C8,x
LDA #!SandSpriteNumber
STA $7FAB9E,x
JSL $07F7D2
JSL $0187A7
LDA #$08
STA $7FAB10,x
LDA $94
STA $E4,x
LDA $95
STA $14E0,x
LDA.b #!SandDropHeight
STA $D8,x
LDA.b #!SandDropHeight>>8
STA $14D4,x
PLX

PHX
LDX #$0F
JSR GetRand
PLX
CMP #$00
BNE .NoInc

LDA $1594,x
BNE .NONZERO

INC $1594,x
LDA #$00
BRA .NoInc

.NONZERO
LDA #$FF

.NoInc
STA $00C2,y
LDA $151C,x
STA $151C,y

DEC $151C,x

; If this is the last block and none are carryable, make this one carryable.
lda $151C,x
bne +
	lda $1594,x
	bne +
	lda.b #$00
		sta $00C2,y
+

LDA #$21
STA $1DF9
LDY $1528,x
LDA SandTimerSet,y
STA $15AC,x
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

YarnXSpeeds:
db $28,$D8

ThrowingSprite:
LDA $154C,x
BEQ .ThrowSprite

LDA #$06
STA $1504,x
RTS

.ThrowSprite
LDA #$07
STA $1504,x

LDA $15AC,x
BEQ .Skip
CMP #$01
BNE .Return

JSR StartIdling

.Return
RTS

.Skip
%SubHorzPos()
STY $06

JSL $02A9DE
BMI .Return
PHX
TYX
LDA #$08
STA $14C8,x
LDA #!YarnSpriteNumber
STA $7FAB9E,x
JSL $07F7D2
JSL $0187A7
LDA #$08
STA $7FAB10,x

LDA $06
STA $157C,y
PHY
TAY
LDA YarnXSpeeds,y
PLY
STA $00B6,y

PLX
LDA $14E0,x
STA $14E0,y
LDA $E4,x
STA $00E4,y

LDA $D8,x
STA $00D8,y
LDA $14D4,x
STA $14D4,y

LDA #$10
STA $15AC,x

LDA #$C0
STA $00AA,y
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Hurt:
STZ $B6,x
JSL $81802A

LDA $154C,x
BNE .SkipReset

JSR StartIdling
LDY $1528,x
LDA HurtTime,y
STA $154C,x

.SkipReset
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

XSpeedMagic:
db $10,$F0

Magic:
LDA $154C,x
BEQ .DontRaise
CMP #$14
BCS .StaffFrame

STZ $AA,x
LDA #$05
STA $1504,x
LDA $D8,x
SEC
SBC #$04
STA $D8,x
LDA $14D4,x
SBC #$00
STA $14D4,x
LDA #$20
STA $163E,x
RTS

.StaffFrame
LDA #$04
STA $1504,x

.Return
RTS

.DontRaise
JSR Float

LDA $151C,x
BMI .End
LDA $163E,x
BNE .Return

JSR SpawnMagic
PEI ($94)
REP #$20
LDA $94
CLC
ADC #$0020
STA $94
SEP #$20
JSR SpawnMagic
REP #$20
LDA $94
SEC
SBC #$0040
STA $94
SEP #$20
JSR SpawnMagic
PLA
STA $94
PLA
STA $95

LDA $151C,x
BPL .Skip

LDA #$E0
STA $AA,x

.Skip
LDA #$10
STA $1DF9
RTS

.End
LDA #$20
CMP $AA,x
BPL +
STA $AA,x

+
JSL $01802A
LDA $1588,x
AND #$04
BEQ .NotYet

JSR StartIdling
STZ $1594,x
STZ $1540,x
STZ $AA,x

.NotYet
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

OMGDyingOMG:
LDA $154C,x
BNE .Return

JSL $87FC3B
LDA #$FF
STA $1493
INC $13C6
LDA #$03
STA $1DFB
LDA #$04
STA $14C8,x
LDA #$20
STA $1540,x

.Return
RTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

ShatterEffect:
LDA $D8,x
CLC
ADC #$08
STA $98
LDA $14D4,x
ADC #$00
STA $99
LDA $E4,x
STA $9A
LDA $14E0,x
STA $9B

PHB
LDA #$02
PHA
PLB
LDA #$00
JSL $828663
PLB
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Float:
JSL $81801A

LDA $1540,x
BNE .DontSwitch

LDA $1594,x
EOR #$01
STA $1594,x

LDA $1337
LDA #$18
STA $1540,x

.DontSwitch
LDA $1594,x
BEQ .Down

LDA $AA,x
BPL .Set1
CMP #$F8
BCC .NoSet1

.Set1
DEC $AA,x

.NoSet1
RTS

.Down
LDA $AA,x
BMI .Set2
CMP #$09
BCS .NoSet2

.Set2
INC $AA,x

.NoSet2
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

StartIdling:
STZ $B6,x
STZ $C2,x
LDY $1528,x
LDA IdleTime,y
STA $154C,x
STZ $1504,x
LDA #$08
STA $15AC,x
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

CheckShellContact:
LDA $C2,x
CMP #$02
BEQ .Return

LDY #$0B

.ContactLoop
LDA $14C8,y
CMP #$09
BCC .Next
CMP #$0B
BEQ .Next

JSL $83B69F
PHX
TYX
JSL $83B6E5
PLX
JSL $83B72B
BCC .Next

PHX
TYX
JSR ShatterEffect
STZ $14C8,x
PLX

LDA #$06
CMP $C2,x
BEQ .Return
STA $C2,x

DEC $1528,x
LDA #$28
STA $1DFC
LDA #$08
STA $1504,x
LDA #$50
STA $154C,x

.Next
DEY
BPL .ContactLoop

.Return
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

CheckMarioContact:
LDA $C2,x
CMP #$02
BEQ .NoContact
CMP #$06
BEQ .NoContact

JSL $81A7DC
BCC .NoContact

LDA $0E
CMP #$E6
BPL .Hurt

LDA $140D
BEQ .Hurt

JSL $81AA33
JSL $81AB99
LDA #$02
STA $1DF9
RTS

.Hurt
JSL $80F5B7

.NoContact
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SpawnMagic:
JSL $82A9DE
BMI .Return
PHX
TYX
LDA #$08
STA $14C8,x
LDA #$20
STA $9E,x
JSL $07F7D2
PLX
LDA $E4,x
STA $00E4,y
LDA $14E0,x
STA $14E0,y
LDA $D8,x
STA $00D8,y
LDA $14D4,x
STA $14D4,y
LDA #$40
STA $163E,x
DEC $151C,x
JSR Aiming

.Return
RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Aiming:
LDA #$30
STA $01
PHX		;\ preserve sprite indexes of Magikoopa and magic
PHY		;/
%SubVertPos()	; $0E = vertical distance to Mario
STY $02		; $02 = vertical direction to Mario
LDA $0E		;\ $0C = vertical distance to Mario, positive
BPL CODE_01BF7C	; |
EOR #$FF	; |
CLC		; |
ADC #$01	; |

CODE_01BF7C:
STA $0C		;/
%SubHorzPos(); $0F = horizontal distance to Mario
STY $03		; $03 = horizontal direction to Mario
LDA $0F		;\ $0D = horizontal distance to Mario, positive
BPL CODE_01BF8C	; |
EOR #$FF	; |
CLC		; |
ADC #$01	; |

CODE_01BF8C:
STA $0D		;/
LDY #$00
LDA $0D		;\ if vertical distance less than horizontal distance,
CMP $0C		; |
BCS CODE_01BF9F	;/ branch
INY		; set y register
PHA		;\ switch $0C and $0D
LDA $0C		; |
STA $0D		; |
PLA		; |
STA $0C		;/

CODE_01BF9F:
LDA #$00	;\ zero out $00 and $0B
STA $0B		; | ...what's wrong with STZ?
STA $00		;/
LDX $01		;\ divide $0C by $0D?

CODE_01BFA7:
LDA $0B		; |\ if $0C + loop counter is less than $0D,
CLC		; | |
ADC $0C		; | |
CMP $0D		; | |
BCC CODE_01BFB4	; |/ branch
SBC $0D		; | else, subtract $0D
INC $00		; | and increase $00

CODE_01BFB4:
STA $0B		; |
DEX		; |\ if still cycles left to run,
BNE CODE_01BFA7	;/ / go to start of loop
TYA		;\ if $0C and $0D was not switched,
BEQ CODE_01BFC6	;/ branch
LDA $00		;\ else, switch $00 and $01
PHA		; |
LDA $01		; |
STA $00		; |
PLA		; |
STA $01		;/

CODE_01BFC6:
LDA $00		;\ if horizontal distance was inverted,
LDY $02		; | invert $00
BEQ CODE_01BFD3	; |
EOR #$FF	; |
CLC		; |
ADC #$01	; |
STA $00		;/

CODE_01BFD3:
LDA $01		;\ if vertical distance was inverted,
LDY $03		; | invert $01
BEQ CODE_01BFE0	; |
EOR #$FF	; |
CLC		; |
ADC #$01	; |
STA $01		;/

CODE_01BFE0:
PLY		;\ retrieve Magikoopa and magic sprite indexes
PLX		;/
LDA $00
STA $00AA,y
LDA $01
STA $00B6,y
RTS		; return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

GetRand:
JSL $81ACF9
INX
STA $4202
STX $4203
NOP #4
LDA $4217
RTS