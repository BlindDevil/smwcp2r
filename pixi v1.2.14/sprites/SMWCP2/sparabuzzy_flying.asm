!initxlo = $1504
!initxhi = $1510
!initylo = $151C
!inityhi = $1528

!xlo = $E4
!xhi = $14E0
!ylo = $D8
!yhi = $14D4

!linearType = $1626		;00 is none, 01 is clockwise, 02 is counterclockwise
!rotType    = $160E		;00 is none, 01 is horizontal,02 is vertical.
!rotSpeed   = $187B
!rotRadius  = $157C

!anglelo = $1534
!anglehi = $1570



;;Stuff you can change |||
;                      vvv

!rotationSpeed   = #$02		;(No clockwise/counterclockwise conversions are necessary this time.)

!horizontalSpeed = #$02
!verticalSpeed   = #$02

tiles:		db $60,$62,$44,$08,$08,$44,$62,$60

!property 	= #$2F

!radius1	= #$30	;The clockwise radius to use. The current value is 2 blocks.
!radius2	= #$30	;The counterclockwise radius to use. The current value is 3 blocks.

!hDistance	= #$30	;The distance flown horizontally. The actual value is twice this.
!vDistance	= #$30	;The distance flown vertically.   The actual value is twice this.


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc

LDA !xlo,x
STA !initxlo,x
LDA !xhi,x
STA !initxhi,x
LDA !ylo,x
STA !initylo,x
LDA !yhi,x
STA !inityhi,x

STZ !anglelo,x
STZ !anglehi,x

LDY #$00
LDA $7FAB10,x
AND #$04
BEQ DoesNotRotate
LDY #$01
DoesNotRotate:
STY $00

LDY #$00
LDA !xlo,x
LSR #4
AND #$01
BEQ IsClockwiseHorizontal
LDY #$01
IsClockwiseHorizontal:
STY $01

LDA $00
BNE IsLinear
LDA !rotationSpeed
STA !rotSpeed,x

LDA $01
BNE IsClockwise
LDA #$02
STA !rotType,x
LDA !radius2
STA !rotRadius,x
RTL
IsClockwise:
LDA #$01
STA !rotType,x
LDA !radius1
STA !rotRadius,x

RTL


IsLinear:
LDA $01
BNE IsHorizontal
LDA #$02
STA !linearType,x
LDA !vDistance
STA !rotRadius,x
LDA !verticalSpeed
STA !rotSpeed,x
RTL
IsHorizontal:
LDA #$01
STA !linearType,x
LDA !hDistance
STA !rotRadius,x
LDA !horizontalSpeed
STA !rotSpeed,x
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR Main
PLB
RTL


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Quit:
JSR Graphics
RTS

Main:
LDA #$03
%SubOffScreen()
LDA $13BF
CMP #$17
BNE NoAdjust
LDA $166E,x
AND #$EF
STA $166E,x
NoAdjust:

LDA $14C8,x
CMP #$08
BNE Quit
LDA $9D
BNE Quit







JSL $81803A

STA !1FD6,x

LDA #$00
XBA
LDA !rotSpeed,x
REP #$20
STA $02
SEP #$20
LDY !rotType,x
CPY #$02
BNE isClockwise
REP #$20
LDA #$0200
SEC
SBC $02
isClockwise:
REP #$20
STA $02
SEP #$20








LDA !anglelo,x
XBA
LDA !anglehi,x
REP #$20
CLC
ADC $02
SEP #$20
STA !anglehi,x
XBA
STA !anglelo,x

LDA !rotRadius,x
STA $00

LDA !linearType,x
CMP #$01
BEQ skipRotation1

LDA !anglelo,x
XBA
LDA !anglehi,x
REP #$20
STA $01
SEP #$20

JSR SIN



LDA !inityhi,x
XBA
LDA !initylo,x
REP #$20
CLC
ADC $03
SEP #$20
STA !ylo,x
XBA
STA !yhi,x


skipRotation1:
LDA !linearType,x
CMP #$02
BEQ skipRotation2

LDA !anglelo,x
XBA
LDA !anglehi,x
REP #$20
CLC
ADC #$0080
STA $01

SEP #$20
JSR SIN



LDA !initxhi,x
XBA
LDA !initxlo,x
REP #$20
CLC
ADC $03
SEP #$20
STA !xlo,x
XBA
STA !xhi,x

skipRotation2:

JSR Graphics
RTS


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Graphics:		
			;LDA $14
			;AND #$04
			;BEQ NoIncrement
			LDA $14
			AND #$1C
			LSR #2
			STA $1594,x
			
			NoIncrement:
		
			PHY
			LDY #$01
			LDA $94
			SEC : SBC $E4,x
			STA $0F
			LDA $95
			SBC $14E0,x
			BPL LABELTHING
			DEY
			LABELTHING:
			TYA
			ASL #6
			STA $0E
			LDA !property
			ORA $0E
			STA $0E
			PLY

			%GetDrawInfo() 

			LDA $00
			STA $0300,y

			LDA $01
			STA $0301,y
			
			LDA $1594,x
			PHX
			TAX
			LDA tiles,x
			PLX
			STA $0302,y
	
			LDA $0E
			STA $0303,y

			INY #4

			LDY #$02                ; \ we've already set 460 so use FF
			LDA #$00                ; | A = number of tiles drawn - 1
			JSL $81B7B3             ; / don't draw if offscreen
QuitGraphics:		RTS                     ; return

;-------------------------------;SIN JSL
SIN:	PHP			;From: Support.asm's JSL.asm
	PHX			;By: 
				;Comment Translation+Addition By: Fakescaper
	TDC			;LDA #$0000
	LDA $01			;This determines the Ypos if you're using it for sprite movement
	REP #$30		;16-bit AXY
	ASL A			;$00     = Radius
	TAX			;$01/$02 = Angle ($0000-$01FF)
	LDA $07F7DB,x		;SMW's 16-bit CircleCoords table
	STA $03			;
				;
	SEP #$30		;8bit AXY
	LDA $02			;
	PHA			;
	LDA $03			;
	STA $4202		;
	LDA $00			;
	LDX $04			;
	 BNE .IF1_SIN		;
	STA $4203		;
	ASL $4216		;
	LDA $4217		;
	ADC #$00		;
.IF1_SIN			;
	LSR $02			;
	 BCC .IF_SIN_PLUS	;
				;
	EOR #$FF		;XOR
	INC A			;
	STA $03			;
	 BEQ .IF0_SIN		;
	LDA #$FF		;
	STA $04			;
	 BRA .END_SIN		;
				;
.IF_SIN_PLUS			;
	STA $03			;
.IF0_SIN			;
	STZ $04			;
.END_SIN			;
	PLA			;
	STA $02			;$02
	PLX			;
	PLP			;
	RTS			;Return
;-------------------------------;