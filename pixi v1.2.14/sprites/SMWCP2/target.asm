;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!DIR = $03
!PROPRAM = $04
!RAM_TargetsBroken = $7F9A90
!EXTRA_BITS = $7FAB10

PRINT "MAIN ",pc
	PHB
	PHK
	PLB		
	JSR Run
	PLB
PRINT "INIT ",pc
	RTL

XSPEEDTABLE:		db $20,$E0	;X speed table: Right, left
YSPEEDTABLE:		db $20,$E0	;Y speed acceleration table: down, up (?)

XACCELERATION:
YACCELERATION:		db $01,$FF	;Y Acceleration stuff

RETURN:
	RTS

Run:	
	JSR GFX

SKIP:
	LDA $14C8,x	; \ 
	CMP #$08	;  | if status != 8, RETURN
	BNE RETURN	; /
	LDA $9D		; \ if sprites locked, RETURN
	BNE RETURN	; /

		LDA $C2,x		;Load... something
		AND #$01		;AND #$01
		TAY			;Use as index for acceleration table

		LDA !EXTRA_BITS,x		; \ 
		AND #$04		;  |Become wavy if extra bit is set
	
		BEQ VERTICAL		; / (This was originally a sprite number check)

		LDA $B6,x
		CLC
		ADC XACCELERATION,y
		STA $B6,x
		CMP XSPEEDTABLE,y		;If it didn't reach the desired X speed
		BNE UPDATE		;Continue processing the sprite
		INC $C2,x		;If it reached the desired speed, change Y direction
		BRA UPDATE
VERTICAL:
		LDA $AA,x		;\
		CLC			; |Load Y speed
		ADC YACCELERATION,y	; |Accelerate it
		STA $AA,x		;/
		CMP YSPEEDTABLE,y		;If it didn't reach the desired Y speed
		BNE UPDATE		;Continue processing the sprite
		INC $C2,x		;If it reached the desired speed, change Y direction

UPDATE:


	JSL $81801A	;UPDATE ypos no gravity
	JSL $818022	;UPDATE xpos no gravity
	JSL $818032	;interact with other sprites
	JSR SUB_THROWN_SPRITE
RTS
	
;;;;;;;;;;;;;;;;;;;;;;;
;GFX routine
;;;;;;;;;;;;;;;;;;;;;;;

GFX:
	LDA #$00
	%SubOffScreen()
	%GetDrawInfo()

	LDA $157C,x	;direction...
	STA !DIR

	LDA $15F6,x	;properties...
	STA !PROPRAM

OAM_LOOP:

	LDA $00
	STA $0300,y	;xpos

	LDA $01
	STA $0301,y	;ypos

	PHX
	LDX $15E9
	LDA $1540,x	;general purpose timer
	PLX
	AND #$10
	BNE NORMAL

	INX
	INX
	LDA #$CC ;TILEMAP,x
	STA $0302,y
	DEX
	DEX
	BRA DUNN

NORMAL:
	LDA #$CC ;TILEMAP,x
	STA $0302,y	;chr

DUNN:
	;LDA !PROPRAM
	;ORA $64
	LDA #$09
	STA $0303,y	;prop

	LDY #$02	;16x16
	LDA #$00	;1 tile
	JSL $81B7B3	;reserve
	RTS

SUB_THROWN_SPRITE:

SPRITE_INTERACT:   
	LDY #$0B
LOOP:
	LDA $14C8,y		; \ if the sprite status is..
	CMP #$09		;  | ...shell-like
	BEQ PROCESS_SPRITE	; /
	CMP #$0A		; \ ...throwned shell-like
	BEQ PROCESS_SPRITE	; /

NEXT_SPRITE:	    
	DEY		;
	BPL LOOP	; ...otherwise, LOOP
NOBREAK:
	RTS		; RETURN

PROCESS_SPRITE:
	LDA $009E,y
	CMP #$80
	BEQ NOBREAK	    
	PHX		; push x
	TYX		; transfer x to y
	JSL $03B6E5	; get sprite clipping B routine
	PLX		; pull x
	JSL $03B69F	; get sprite clipping A routine
	JSL $03B72B	; check for contact routine
	BCC NEXT_SPRITE

	LDA #$03	; \ set sprite state
	STA $C2,x	; /
	LDA #$40	; \ set timer
	STA $1540,x	; /

	PHX                     ; push x
	TYX                     ; transfer x to y

	STZ $14C8,x             ; destroy the sprite

BLOCK_SETUP:	    
	LDA $E4,x	; \ setup block properties
	STA $9A		;  |
	LDA $14E0,x	;  |
	STA $9B		;  |
	LDA $D8,x	;  |
	STA $98		;  |
	LDA $14D4,x	;  |
	STA $99		; /

EXPLODING_BLOCK:	    
	
	PHB		; \ set the exploding block routine
	LDA #$02	;  |
	PHA		;  |
	PLB		;  |
	LDA #$FF		;  | $FF = set flashing palette
	JSL $028663	;  |
	PLB		; /
	PLX		; pull sprite index
	LDA !RAM_TargetsBroken
	INC	;do stuff here
	STA !RAM_TargetsBroken
	LDA #$04	;destroy
	STA $14C8,x
	LDA #$29	; \ sound effect
	STA $1DFC 	; /
REP #$10
LDA $7FA230
CLC
ADC #$02
STA $7FA230
SEP #$10
	RTS