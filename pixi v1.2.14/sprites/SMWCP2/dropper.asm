;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Dropper from Wario Land 1 - by ghettoyouth
;;
;;based on mikeyk's Thwomp disassembly
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    ; symbolic names for RAM addresses (don't change these)
                    !SPRITE_Y_SPEED  = !AA
                    !SPRITE_X_SPEED  = !B6
                    !SPRITE_STATE    = !C2
                    !SPRITE_Y_POS    = !D8
                    !ORIG_Y_POS      = !151C
                    !EXPRESSION      = !1528
                    !FREEZE_TIMER    = !1540
                    !SPR_OBJ_STATUS  = !1588
                    !H_OFFSCREEN     = !15A0
                    !V_OFFSCREEN     = !186C
                    
                    ; definitions of bits (don't change these)
                    !IS_ON_GROUND    = $04             
                    
                    ; sprite data
                    !SPRITE_GRAVITY  = $05
                    !MAX_Y_SPEED     = $FE 
                    !RISE_SPEED      = $F0
                    !TIME_TO_SHAKE   = $28
                    !SOUND_EFFECT    = $1A 
                    !TIME_ON_GROUND  = $40
                    !ANGRY_TILE      = $E6  

X_OFFSET:            db $00,$10,$10,$00,$08 
Y_OFFSET:            db $f0,$f0,$00,$00,$f8 
TILE_MAP:            db $8C,$8C,$8E,$8E,$AE 
PROPERTIES:          db $03,$43,$43,$03,$03
                                                  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "INIT ",pc
                    LDA !SPRITE_Y_POS,x  
                    STA !ORIG_Y_POS,x
                    LDA !E4,x  
                    CLC        
                    ADC #$08   
                    STA !E4,x  
                    RTL
      

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "MAIN ",pc                                    
                    PHB                     
                    PHK                     
                    PLB                     
                    JSR SPRITE_CODE_START   
                    PLB                     
                    RTL                     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main code 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

RETURN:              RTS  

SPRITE_CODE_START:   JSR SUB_GFX


                    LDA !14C8,x             ; RETURN if sprite status != 8
                    CMP #$08            	 
                    BNE RETURN           
                    
                    LDA $9D			        ; RETURN if sprites locked
                    BNE RETURN    
                    
                    LDA #$00
		%SubOffScreen()         
                    
                    JSL $01A7DC|!BankB             ; interact with mario 
                    
                    LDA !SPRITE_STATE,x     
                    CMP #$01
                    BEQ FALLING
                    CMP #$02
                    BEQ RISING

;-----------------------------------------------------------------------------------------
; state 0
;-----------------------------------------------------------------------------------------

HOVERING:            LDA !V_OFFSCREEN,x       ;fall if offscreen vertically
                    BNE SET_FALLING
                    
                    LDA !H_OFFSCREEN,x       ;RETURN if offscreen horizontally
                    BNE RETURN0
                    
                    
					inc !1602,x
					lda !1602,x
					cmp #$20
					bne RETURN0

SET_FALLING:         LDA #$02                    ;set expression    
                    STA !EXPRESSION,x  

                    INC !SPRITE_STATE,x          ;chage state to FALLING

                    ;LDA #$00
					stZ !1602,x
                    STZ !SPRITE_Y_SPEED,x        ;set initial speed

RETURN0:             RTS                     

;-----------------------------------------------------------------------------------------
; state 1
;-----------------------------------------------------------------------------------------

FALLING:             JSL $01801A|!BankB             ;apply speed
                    
                    LDA !SPRITE_Y_SPEED,x    ;increase speed if below the max
                    CMP #!MAX_Y_SPEED
                    BCS DONT_INC_SPEED
                    ADC #!SPRITE_GRAVITY
                    STA !SPRITE_Y_SPEED,x    
DONT_INC_SPEED:
                    JSL $019138|!BankB         ;interact with objects
                    
                    LDA !SPR_OBJ_STATUS,x    ;RETURN if not on the ground
					AND #!IS_ON_GROUND
                    BEQ RETURN1
                    
                    JSR SUB_9A04            ; ?? speed related
                    
                    LDA #!TIME_TO_SHAKE      ;shake ground
                    STA $1887|!Base2
                    
                    LDA #!SOUND_EFFECT       ;play sound effect
                    STA $1DFC|!Base2
                    
                    LDA #!TIME_ON_GROUND     ;set time to stay on ground
                    STA !FREEZE_TIMER,x  
                    
                    INC !SPRITE_STATE,x      ;go to RISING state
RETURN1:             RTS                     

;-----------------------------------------------------------------------------------------
; state 2
;-----------------------------------------------------------------------------------------

RISING:              LDA !FREEZE_TIMER,x      ;if we're still waiting on the ground, RETURN
                    BNE RETURN2
                    
                    STZ !EXPRESSION,x        ;reset expression
                    
                    LDA !SPRITE_Y_POS,x      ;check if the sprite is in original position
                    CMP !ORIG_Y_POS,x  
                    BNE RISE
                    
                    STZ !SPRITE_STATE,x      ;reset state to HOVERING
                    RTS                     

RISE:                LDA #!RISE_SPEED         ;set RISING speed and apply it
                    STA !SPRITE_Y_SPEED,x     
                    JSL $01801A|!BankB
RETURN2:             RTS                     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SUB_GFX:            %GetDrawInfo()
                    
                    LDA !EXPRESSION,x   
                    STA $02       
                    PHX                     
                    LDX #$03                
                    CMP #$00                        
                    BEQ LOOP_START
                    INX                     
LOOP_START:          LDA $00    
                    CLC                     
                    ADC X_OFFSET,x
                    STA $0300|!Base2,y

                    LDA $01    
                    CLC                     
                    ADC Y_OFFSET,x
                    STA $0301|!Base2,y

                    LDA PROPERTIES,x
                    ORA $64    
                    STA $0303|!Base2,y

                    LDA TILE_MAP,x
                    CPX #$04                
                    BNE NORMAL_TILE
                    PHX                     
                    LDX $02    
                    CPX #$02                
                    BNE NOT_ANGRY
                    LDA #!ANGRY_TILE               
NOT_ANGRY:           PLX                     
NORMAL_TILE:         STA $0302|!Base2,y

                    INY                     
                    INY                     
                    INY                     
                    INY                     
                    DEX                     
                    BPL LOOP_START

                    PLX                     
                                   
                    LDY #$02                ; \ 460 = 2 (all 16x16 tiles)
                    LDA #$04                ;  | A = (number of tiles drawn - 1)
                    JSL $01B7B3|!BankB      ; / don't draw if offscreen
                    RTS                     ; RETURN


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; speed related
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
                    
SUB_9A04:            LDA !SPR_OBJ_STATUS,x
                    BMI THWOMP_1
                    LDA #$00                
                    LDY !15B8,x
                    BEQ THWOMP_2
THWOMP_1:            LDA #$18
THWOMP_2:            STA !SPRITE_Y_SPEED,x  
                    rts