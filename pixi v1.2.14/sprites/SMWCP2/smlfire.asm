;gau/snake projecticle by smkdan (optimized by Blind Devil)

;Tilemap define:
!TILEMAP = $E0

print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
print "INIT ",pc
	RTL

Run:
	LDA #$00
	%SubOffScreen()
	JSR GFX			;draw sprite

	LDA !14C8,x
	CMP #$08          	 
	BNE Return           
	LDA $9D			;locked sprites?
	BNE Return

	JSL $01801A|!BankB	;update ypos no gravity
	JSL $018022|!BankB	;update xpos no gravity
	JSL $01A7DC|!BankB	;mario interact
Return:
	RTS        
			
;=====

GFX:
	%GetDrawInfo()
	STZ $04		;reset $04
	LDA !B6,x
	BMI GoingLeft
	LDA #$40
	TSB $04		;set flip

GoingLeft:
	REP #$20
	LDA $00			;dead simple GFX routine
	STA $0300|!Base2,y
	SEP #$20

	LDA #!TILEMAP
	STA $0302|!Base2,y

	LDA !15F6,x
	ORA $04
	ORA $64
	STA $0303|!Base2,y

	LDY #$02		;16x16 tile
        LDA #$00        	;1 tile
        JSL $01B7B3|!BankB	;reserve
	RTS