; *** modified for use in SMWCP2 ***
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Yoku Block, by imamelia
;;
;; Remember those disappearing blocks that appeared in most of the games in the
;; original Mega Man series? Well, now they're in SMW.  This sprite will be invisible
;; at first, and at a certain frame, it will appear.  It will then stay solid for a while,
;; then disappear again.
;;
;; Number of extra bytes: 2
;;
;; Extra byte 1:
;;
;; Bits 0-2: Tilemap index.  This lets you select one of 8 possible tiles for the sprite to use.
;; Bits 3-7: These indicate the frame number on which the sprite will first appear.
;;
;; Extra byte 2:
;;
;; Bits 0-3: These bits indicate how long the sprite will stay solid once it has appeared.
;;	Multiply this by 16 (0x10) to get the number of frames.
;; Bits 4-7: These bits indicate how long the sprite will stay invisible once it has disappeared.
;;	Multiply this by 16 (0x10) to get the number of frames.
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; defines and tables
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!VertLaserTile = $0B70	; the first of 8 tiles that "spawn" the vertical laser
!HorizLaserTile = $0B78	; the first of 8 tiles that "spawn" the horizontal laser

!SFX1 = $10			; This is the sound effect in $1DF9 that will play when the block or platform appears.
!SFX2 = $00			; This is the sound effect in $1DF9 that will play when the laser activates.
!SFX3 = $0E			; This is the sound effect in $1DFC that will play when the laser activates.

!Tilemap1 = $40		; tile (on the first page) that the block will use
Tilemap2:			;
db $60,$61,$62		; tiles (on the second page) that the platform will use
!Tilemap3 = $67		; tile (on the second page) that the vertical laser will use
!Tilemap4 = $69		; tile (on the second page) that the horizontal laser will use

; This indicates whether the sprite will act like a block or a platform when not placed on a certain Map16 tile.
; It is indexed by sublevel number.
SpriteType:
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 0-F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 10-1F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 20-2F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 30-3F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 40-4F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 50-5F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 60-6F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 70-7F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 80-8F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 90-9F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; A0-AF
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; B0-BF
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; C0-CF
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; D0-DF
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; E0-EF
db $00,$00,$00,$00,$00,$00,$01,$00,$00,$00,$00,$00,$00,$00,$00,$00	; F0-FF
db $00,$00,$00,$00,$00,$00,$01,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 100-10F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 110-11F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 120-12F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 130-13F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 140-14F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 150-15F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 160-16F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 170-17F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 180-18F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 190-19F
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 1A0-1AF
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 1B0-1BF
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 1C0-1CF
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 1D0-1DF
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 1E0-1EF
db $00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00,$00	; 1F0-1FF

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc

PHB
PHK
PLB

LDA $7FAB10,x		;
AND #$04			;
LSR #3				;
LDA $7FAB28,x		;
BCC $04				;
ASL #4				;
AND #$F0			;
STA $151C,x			; $151C,x - frame to appear on
CLC					;
ADC #$80			;
STA $1534,x			; $1534,x - frame to disappear on

LDA #$04			;
STA $C2,x			;

REP #$10				;
LDY $010B			; sublevel number
LDA SpriteType,y		;
SEP #$10				;
STA $1510,x			;
BEQ NoClipChange		;
LDA #$04			;
STA $1662,x			;
LDA $190F,x			;
ORA #$01			;
STA $190F,x			;
NoClipChange:		;

LDA $E4,x			;
STA $06				;
LDA $14E0,x			;
STA $07				;
LDA $D8,x			;
STA $08				;
LDA $14D4,x			;
STA $09				;

PHX					;
JSR Map16Check		;
SEP #$10				;
PLX					;

STA $0A				;
CMP #!VertLaserTile	;
BCC NoVertLaser		;
CMP #!VertLaserTile+8	;
BCC SetVertLaser		;
NoVertLaser:			;
CMP #!HorizLaserTile	;
BCC NoHorizLaser		;
CMP #!HorizLaserTile+8	;
BCC SetHorizLaser		;
NoHorizLaser:			;
SEP #$20				;

PLB
RTL

SetVertLaser:
LDA $08				;
CLC					;
ADC #$0010			;
SEP #$20				;
STA $D8,x			;
XBA					;
STA $14D4,x			;
LDA #$02			;
STA $1510,x			;
LDA $0A				;
SEC					;
SBC.b #!VertLaserTile	;
BRA EndLaserInit		;

SetHorizLaser:
LDA $06				;
CLC					;
ADC #$0010			;
SEP #$20				;
STA $E4,x			;
XBA					;
STA $14E0,x			;
LDA #$03			;
STA $1510,x			;
LDA $0A				;
SEC					;
SBC.b #!HorizLaserTile	;
EndLaserInit:			;
STA $160E,x			;
LDA $1534,x			;
SEC					;
SBC #$40				;
STA $1534,x			;
PLB					;
RTL					;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR YokuBlockMain
PLB
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

YokuBlockMain:

LDA $14C8,x			;
CMP #$08			; return if the sprite is not in normal status
BNE .Return			;

LDA #$00
%SubOffScreen()

LDA $1504,x			;
BEQ .NoDec			;
DEC $1504,x			;
.NoDec				;

LDA $C2,x			;
JSL $8086DF			;

dw S00_Invisible		; state 00 - invisible
dw S01_Appearing		; state 01 - appearing
dw S02_Solid			; state 02 - solid
dw S03_Disappearing	; state 03 - disappearing
dw S04_InvisibleInit	; state 04 - invisible at start

.Return				;
RTS					; more organized, at the expense of one useless byte


S00_Invisible:			;

LDA $14				;
CMP $151C,x			;
BEQ .Appear			;
RTS					;
.Appear				;
LDA #$01			;
STA $C2,x			;
RTS					;

S01_Appearing:		;

LDA $1510,x			;
CMP #$02			;
BCS .LaserSound		;
LDA #!SFX1			; sound effect to play (1)
STA $1DF9			;
BRA .Continue			;
.LaserSound			;
LDA #!SFX2			;
STA $1DF9			;
LDA #!SFX3			;
STA $1DFC			;
.Continue				;
INC $C2,x			; set the sprite state to 01
RTS					;

S02_Solid:			;

LDA $14				;
CMP $1534,x			;
BNE .NoDisappear		; if the state-change timer is down to 0, then the sprite will disappear
INC $C2,x			;
.NoDisappear			;
JSR SubGFX			; draw the sprite
JMP InteractionRt		;

S03_Disappearing:		;

STZ $C2,x			; reset the sprite state to 00
RTS					;

S04_InvisibleInit:		;

LDA $151C,x			; if the frame counter equals the frame on which the sprite is supposed to appear...
CMP $14				;
BEQ .Appear			;
SEC					;
SBC #$80				;
CMP $14				;
BPL .MakeSolid			;
RTS					;
.Appear				;
LDA #$01			;
STA $C2,x			;
RTS					;
.MakeSolid			;
LDA #$02			;
STA $C2,x			;
RTS					;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; graphics routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SubGFX:

%GetDrawInfo()	; set up some variables

LDA $1510,x		;
BEQ DrawBlock	;
CMP #$01		;
BEQ DrawPlatform	;
CMP #$02		;
BEQ DrawLaserV	;
JMP DrawLaserH	;

DrawBlock:		;

LDA $00			;
STA $0300,y		; tile X position
LDA $01			;
STA $0301,y		; tile Y position
LDA #!Tilemap1	;
STA $0302,y		; tile to use
LDA #$32		;
STA $0303,y		; tile properties

LDY #$02			; the tile was 16x16
LDA #$00		; there was only one tile
JSL $81B7B3		; "fix" the write to OAM
RTS				;

DrawPlatform:		;

LDA $00			;
STA $0300,y		; tile X position
CLC				;
ADC #$10		;
STA $0304,y		;
CLC				;
ADC #$10		;
STA $0308,y		;
LDA $01			;
STA $0301,y		; tile Y position
STA $0305,y		;
STA $0309,y		;
LDA.w Tilemap2	;
STA $0302,y		; tile to use
LDA.w Tilemap2+1	;
STA $0306,y		;
LDA.w Tilemap2+2	;
STA $030A,y		;
LDA #$31		;
STA $0303,y		; tile properties
STA $0307,y		;
STA $030B,y		;

LDY #$02			; the tile was 16x16
LDA #$02		; there was only one tile
JSL $81B7B3		; "fix" the write to OAM
RTS				;

DrawLaserV:		;

DEC $01			;
LDA $160E,x		;
TAX				;
.Loop			;
LDA $00			;
STA $0300,y		;
LDA $01			;
STA $0301,y		;
LDA #!Tilemap3	;
STA $0302,y		;
LDA #$3D		;
STA $0303,y		;
LDA $01			;
CLC				;
ADC #$10		;
STA $01			;
INY #4			;
DEX				;
BPL .Loop			;
LDX $15E9		;
LDY #$02			;
LDA $160E,x		;
JSL $81B7B3		;
RTS				;

DrawLaserH:		;

DEC $01			;
LDA $160E,x		;
TAX				;
.Loop			;
LDA $00			;
STA $0300,y		;
LDA $01			;
STA $0301,y		;
LDA #!Tilemap4	;
STA $0302,y		;
LDA #$3D		;
STA $0303,y		;
LDA $00			;
CLC				;
ADC #$10		;
STA $00			;
INY #4			;
DEX				;
BPL .Loop			;
LDX $15E9		;
LDY #$02			;
LDA $160E,x		;
JSL $81B7B3		;
RTS				;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; interaction routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

InteractionRt:

LDA $1510,x			;
CMP #$02			;
BCS LaserInteract		;
JSL $81B44F			; make the sprite act like a solid block
RTS					;

; here:
; - $14B0-$14B1 hold the X displacement
; - $14B2-$14B3 hold the Y displacement
; - $14B4-$14B5 hold the width
; - $14B6-$14B7 hold the height
LaserInteract:			;
BEQ .SetVertClip		; if the laser is a horizontal one, then the X-values are variable and the Y-values are constant
STZ $14B0			; X displacement = 0 pixels
LDA #$01			;
STA $14B2			; Y displacement = 1 pixel
LDA $160E,x			;
INC					; $160E,x holds the length of the laser (00-07 -> 1-8 tiles)
ASL #4				;
STA $14B4			; the width of the laser is determined by how many tiles it has
LDA #$0E				;
STA $14B6			; the height of the laser is always 14 pixels
BRA .Continue			;
.SetVertClip			; if the laser is a vertical one, then the X-values are constant and the Y-values are variable
LDA #$01			;
STA $14B0			; X displacement = 1 pixel
STZ $14B2			; Y displacement = 0 pixels
LDA #$0E				;
STA $14B4			; the width of the laser is always 14 pixels
LDA $160E,x			;
INC					; $160E,x holds the length of the laser (00-07 -> 1-8 tiles)
ASL #4				;
STA $14B6			; the height of the laser is determined by how many tiles it has
.Continue				;
STZ $14B1			; all high bytes are 0
STZ $14B3			;
STZ $14B5			;
STZ $14B7			;
JSR GetPlayerClipping	; set up the player's clipping values
JSR GetSpriteClippingA	; set up the sprite's clipping values
JSR CheckForContact	; check for contact between the two
BCC .End				; if there is none, return
LDA $1490			; if there was contact...
ORA $1497			; and the player isn't invincible...
BNE .End				;
JSL $80F5B7			; make the player take damage
.End					;
RTS					;

GetPlayerClipping:		; modified player clipping routine, based off and equivalent to $03B664

PHX					;
REP #$20				;
LDA $94				;
CLC					;
ADC #$0002			;
STA $00				; $00-$01 = player X position plus X displacement
LDA #$000C			;
STA $04				; $04-$05 = player clipping width
SEP #$20				;
LDX #$00				;
LDA $73				;
BNE .Inc1				;
LDA $19				;
BNE .Next1			;
.Inc1				;
INX					;
.Next1				;
LDA $187A			;
BEQ .Next2			;
INX #2				;
.Next2				;
LDA $83B660,x		;
STA $06				; $06-$07 = player clipping height
STZ $07				;
LDA $83B65C,x		;
REP #$20				;
AND #$00FF			;
CLC					;
ADC $96				;
STA $02				; $02-$03 = player Y position plus Y displacement
SEP #$20				;
PLX					;
RTS					;

GetSpriteClippingA:		; custom sprite clipping routine, equivalent to $03B69F

LDA $14E0,x			;
XBA					;
LDA $E4,x			;
REP #$20				;
CLC					;
ADC $14B0			;
STA $08				; $08-$09 = sprite X position plus X displacement
LDA $14B4			;
STA $0C				; $0C-$0D = sprite clipping width
SEP #$20				;
LDA $14D4,x			;
XBA					;
LDA $D8,x			;
REP #$20				;
CLC					;
ADC $14B2			;
STA $0A				; $0A-$0B = sprite Y position plus Y displacement
LDA $14B6			;
STA $0E				; $0E-$0F = sprite clipping height
SEP #$20				;
RTS					;

CheckForContact:		; custom contact check routine, equivalent to $03B72B

REP #$20				;

.CheckX				;
LDA $00				; if the sprite's clipping field is to the right of the player's,
CMP $08				; subtract the former from the latter;
BCC .CheckXSub2		; if the player's clipping field is to the right of the sprite's,
.CheckXSub1			; subtract the latter from the former
SEC					;
SBC $08				;
CMP $0C				;
BCS .ReturnNoContact	;
BRA .CheckY			;
.CheckXSub2			;
LDA $08				;
SEC					;
SBC $00				;
CMP $04				;
BCS .ReturnNoContact	;

.CheckY				;
LDA $02				; if the sprite's clipping field is below the player's,
CMP $0A				; subtract the former from the latter;
BCC .CheckYSub2		; if the player's clipping field is above the sprite's,
.CheckYSub1			; subtract the latter from the former
SEC					;
SBC $0A				;
CMP $0E				;
BCS .ReturnNoContact	;
.ReturnContact		;
SEC					;
SEP #$20				;
RTS					;
.CheckYSub2			;
LDA $0A				;
SEC					;
SBC $02				;
CMP $06				;
BCC .ReturnContact		;
.ReturnNoContact		;
CLC					;
SEP #$20				;
RTS					;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; Map16 contact check routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Map16Check:

REP #$30
LDA $1925
AND #$00FF
ASL
TAX
LDA $80BDA8,x
STA $0A
LDA $80BE28,x
STA $0D
SEP #$20
LDA #$00
STA $0C
STA $0F
XBA
LDA $07
ASL
CLC
ADC $07
REP #$20
TAY
LDA [$0A],y
STA $6B
LDA [$0D],y
STA $6E
SEP #$30
LDA #$7E
STA $6D
INC
STA $70	
REP #$30
LDA $08
AND #$01F0
STA $00
LDA $06
AND #$00F0
LSR #4
CLC
ADC $00
TAY
SEP #$20
LDA [$6E],y
XBA
LDA [$6B],y
REP #$20
RTS