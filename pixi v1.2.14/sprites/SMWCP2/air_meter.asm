;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;a fucking air meter, heavily optimized by the blind devil
;;
;;uses extension bytes now
;;
;;extension 1: air meter type (00 = drains on water, 01 = drains outside water, 02 = drains always, 03 = drains when $1473 is equal zero and ON/OFF is ON)
;;extension 2: init timer (should be FF i guess, zero and it instantly kills mario)
;;extension 3: count down frequency ( -fastest- $00, $01, $03, $07, $0f, $1f, $3f, $7f, $ff -slowest- )
;;extension 4: count up frequency ( -fastest- $00, $01, $03, $07, $0f, $1f, $3f, $7f, $ff -slowest- )
;;
;;extra bit set: meter doesn't change palette, uses from CFG
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	!RAM_AirMeter = $0F08|!Base2

	;; Where to place the meter relative to the screen boarder
	!MeterPositionX = $E8
	!MeterPositionY = $1E
	
	!MeterType = !7FAB40,x
	!InitTimer = !7FAB4C,x
	!CountDownFrequency = !7FAB58,x
	!CountUpFrequency = !7FAB64,x

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;; INIT and MAIN routines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

		PRINT "INIT ",pc
LDY #$09				; setup loop

LOOPSTART:
CPY $15E9|!Base2			;\ if sprite being checked is this one,
BEQ NEXTCYCLE				;/ branch
LDA !14C8,y				;\ if sprite being checked is non-existant,
BEQ NEXTCYCLE				;/ branch
PHX
TYX
LDA !7FAB9E,x				;\  if sprite being checked isn't
PLX					; | the same custom sprite as this one,
CMP !7FAB9E,x				; |
BNE NEXTCYCLE				;/  branch
STZ !14C8,x				; if code gets here, there is another instance of
RTL					; this sprite active, so this one is destroyed

NEXTCYCLE:
DEY					; decrease loop counter
BPL LOOPSTART				; if sprites left to check, branch

LOOPDONE:
LDA !InitTimer
STA !RAM_AirMeter
RTL

        PRINT "MAIN ",pc
        PHB
        PHK
        PLB
        JSR MAINSUB
        PLB
        RTL
        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sprite Main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
MAINSUB:
	LDA $9D
	ORA !1FD6,x
	BNE NOSOUND

	LDA $14
	AND #$0F
	BNE NOSOUND

	LDA !RAM_AirMeter	;Play warning sounds
	CMP #$04
	BCC NOSOUND
	CMP #$48
	BCS NOSOUND

SOUND:	
	LDA #$2A
	STA $1DFC|!Base2

NOSOUND:
	LDA.l !MeterType	;if a thermometer, always display it (CHECK THE TEMPERAT-- I mean, the gfx routine)
	CMP #$03
	BNE +

	JSR GFX2		;I AM TOO LAZY DAMN
	BRA NOGFX

+
	LDA !RAM_AirMeter	; If the meter is full...
	CMP #$FF		;  
	BNE Gfx			; ...and it has been for some time...
	LDA $1540,x		;
	BEQ NOGFX		; ...then skip the graphics routine
Gfx:	
	JSR SUBGFX		; Draw the sprite
NOGFX:	

	LDA $9D			; RETURN if sprites are locked
	BNE RETURN0

;Here go the checks that determine when to raise/lower the meter
;option cheatsheet (extension 1) at the top of this file

	LDA !MeterType
	AND #$03
	BEQ .water
	DEC
	BEQ .antiwater
	DEC
	BEQ COUNTDOWN

	LDA $14AF|!Base2
	BNE COUNTUP
	LDA $1473|!Base2
	BNE COUNTUP
	BRA COUNTDOWN

.antiwater
	LDA $75
	BNE COUNTUP
	BRA COUNTDOWN

.water
	LDA $75
	BEQ COUNTUP

COUNTDOWN:
	STZ !1FD6,x

	LDA $14
	AND !CountDownFrequency
	BNE SKIPMETER

	LDA !RAM_AirMeter
	DEC A
	BNE SETMETER
	BRA KILLMARIO
COUNTUP:
	LDA #$01
	STA !1FD6,x

	LDA $14
	AND !CountUpFrequency
	BNE SKIPMETER
	
	LDA !RAM_AirMeter
	CMP #$FF
	BEQ SKIPMETER

	LDA #$48
	STA !1540,x
	
	LDA !RAM_AirMeter
	INC A
	;; 	LDA #$FF		; NOTE: Reinstate to make the meter instantly reset when not in water
SETMETER:	
	STA !RAM_AirMeter
	
SKIPMETER:		
	LDA $1A			; Position = Screen boundary
	CLC
	ADC #$80
	STA !E4,x		; This is done so the sprite stays on screen
	LDA $1B
	ADC #$00
	STA !14E0,x
	LDA $1C
	STA !D8,x
	LDA $1D
	STA !14D4,x
RETURN0:	
	RTS

KILLMARIO:
	STZ $19
	JSL $00F5B7|!BankB	;way easier workaround

	;LDA #$90
	;STA $7D
	;LDA #$09		; Set death animation
	;STA $71
	;STA $1DFB		; Set death music
	;LDA #$FF
	;STA $0DDA
	;LDA #$30		; Set time until we RETURN to the OW
	;STA $1496
	;STA $9D
	;STZ $140D
	;STZ $1407
	;STZ $188A
	RTS


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Graphics Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!NumTiles = $05
	
SPRITETILEDISPX:    db $00,$08,$10,$18,$20,$27
SPRITETILEDISPY:    db $28,$20,$18,$10,$08,$01
SPRITEGFXPROP:	    db $80,$00,$00,$00,$00,$00

PALETTES:           db $09,$05,$0B
	
SUBGFX:
	LDA !7FAB10,x
	AND #$04
	BNE .cfgpal

	JSR GETPALETTE
	BRA +

.cfgpal
	LDA !15F6,x		;use palette from the cfg file
+
	STA $02
	
	LDA !RAM_AirMeter	; Load address to be timed
	LSR
	LSR
	LSR
	STA $03
	
        LDY $15EA|!Base2,x             ; get offset to sprite OAM                           

        PHX
        LDX #!NumTiles
GFXLOOPSTART:

        LDA #!MeterPositionX
        STA $0300|!Base2,Y
	
	LDA #!MeterPositionY
	CLC
	ADC SPRITETILEDISPY,x
        STA $0301|!Base2,Y

	LDA $03
	JSR CALCULATETILE
        STA $0302|!Base2,Y
	
        LDA $02
        ORA SPRITEGFXPROP,x
	ORA #$30
        STA $0303|!Base2,Y
	
        INY    
        INY                     
        INY                     
        INY                     

	DEX
        BPL GFXLOOPSTART
	PLX

        LDY #$00
        LDA #!NumTiles
        JSL $01B7B3|!BankB
        RTS                       

GETPALETTE:
	LDY #$00
	LDA !RAM_AirMeter
	CMP #$55
	BCC LOADPALETTE
	INY
	CMP #$AA
	BCC LOADPALETTE
	INY
LOADPALETTE:
	LDA PALETTES,y
	RTS	
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
CALCULATETILE:
pha
lda $13bf
cmp #$25
bcc +
pla
jmp CALCULATETILE3

+
pla
	CPX #$00
	BEQ ENDPIECE
	CPX #!NumTiles
	BEQ ENDPIECE

	PHA
	TXA
	DEC A
	ASL
	ASL
	ASL
	STA $00
	PLA

	SEC
	SBC $00
	BMI EMPTYTILE
	
	CMP #$08 	
	BCS FULLTILE

	AND #$07
	CLC
	ADC #$47
	CMP #$4B
	BCC RETURN
	CLC
	ADC #$0C
RETURN:	
	RTS

FULLTILE:
	LDA #$46
	RTS
EMPTYTILE:
	LDA #$47
	RTS
ENDPIECE:
	LDA #$56
	RTS

;alt routine

GFX2:
BRA +

namespace legfx2

SPRITETILEDISPX:    db $00,$08,$10,$18,$20,$27
SPRITETILEDISPY:    db $01,$08,$10,$18,$20,$28
SPRITEGFXPROP:	    db $80,$00,$00,$00,$00,$00

PALETTES:           db $09,$05,$0B

+
	JSR GETPALETTE
	LDA $15F6,x		; NOTE: Reinstate to use the palette from the cfg file
	STA $02
	
	LDA !RAM_AirMeter	; Load address to be timed
	LSR
	LSR
	LSR
	STA $03
	
        LDY $15EA,x             ; get offset to sprite OAM                           

        PHX
        LDX #!NumTiles
GFXLOOPSTART:

        LDA #!MeterPositionX
        STA $0300,Y
	
	LDA #!MeterPositionY
	CLC
	ADC SPRITETILEDISPY,x
        STA $0301,Y

	LDA $03
	JSR CALCULATETILE
        STA $0302,Y
	
        LDA $02
        ORA SPRITEGFXPROP,x
	ORA #$20
        STA $0303,Y
	
        INY    
        INY                     
        INY                     
        INY                     

	DEX
        BPL GFXLOOPSTART
	PLX

        LDY #$00
        LDA #!NumTiles
        JSL $01B7B3
        RTS                       

GETPALETTE:
	LDY #$00
	LDA !RAM_AirMeter
	CMP #$55
	BCC LOADPALETTE
	INY
	CMP #$AA
	BCC LOADPALETTE
	INY
LOADPALETTE:
	LDA PALETTES,y
	RTS	
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
CALCULATETILE:
	CPX #$00
	BEQ ENDPIECE
	CPX #!NumTiles
	BEQ ENDPIECE

	PHA
	TXA
	DEC A
	ASL
	ASL
	ASL
	STA $00
	PLA

	SEC
	SBC $00
	BMI EMPTYTILE
	
	CMP #$08 	
	BCS FULLTILE

	AND #$07
	CLC
	ADC #$47
	CMP #$4B
	BCC RETURN
	CLC
	ADC #$0C
RETURN:	
	RTS

FULLTILE:
	LDA #$46
	RTS
EMPTYTILE:
	LDA #$47
	RTS
ENDPIECE:
	LDA #$56
	RTS

namespace off

CALCULATETILE3:
	CPX #$00
	BEQ .ENDPIECE
	CPX #!NumTiles
	BEQ .ENDPIECE

	PHA
	TXA
	DEC A
	ASL
	ASL
	ASL
	STA $00
	PLA

	SEC
	SBC $00
	BMI .EMPTYTILE
	
	CMP #$08 	
	BCS .FULLTILE

	AND #$07
	CLC
	ADC #$27
	CMP #$2B
	BCC .RETURN
	CLC
	ADC #$0C
.RETURN
	RTS

.FULLTILE
	LDA #$26
	RTS
.EMPTYTILE
	LDA #$27
	RTS
.ENDPIECE
	LDA #$36
	RTS