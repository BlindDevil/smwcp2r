;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	!RAM_TimeLeft = $7F8827

	;; Where to place the meter relative to the screen boarder
	!MeterPositionX = $58
	!MeterPositionY = $24
	!Answers = $7F9A7B
	!Correct =	 $7F9A7E	

	;; Possible values for the below variables:
	;;   (fast) $00, $01, $03, $07, $0f, $1f, $3f, $7f, $ff (slow)
	!CountDownFrequency = $07
	!CountUpFrequency = $00

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;	
;; INIT and MAIN routines
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

PRINT "INIT ",pc
	LDA #$04
	STA $13D3
	; LDA $7FAB10,x
 	; AND #$04
	; BEQ TAR	
	; LDA $13BF
	; STA $146C
	; LDA #$04
	; STA $13BF
	; LDA #$01
	; STA $1426
TAR:	PHB
        PHK
        PLB
	LDA #$00
	STA $7F9A7B	;7F9A7B-7D = answer submission digits
	STA $7F9A7C
	STA $7F9A7D
	STA $7F9B00	;current question
	STA $7F9B01	;answer ready to CHECK flag
	STA $7F9A82	;points missed in one event variable
	TAY		;initialize on question 0
	LDA ANSWERDIGIT1,y	;$7F9A7E-80 = correct answer digits
	STA $7F9A7E
	LDA ANSWERDIGIT2,y
	STA $7F9A7F
	LDA ANSWERDIGIT3,y
	STA $7F9A80

	LDA #$9F
	STA !RAM_TimeLeft

        PLB


	;LDA #$00
	;STA $7F9B00

	;LDA #$9F
	;STA !RAM_TimeLeft
	RTL

PRINT "MAIN ",pc
        PHB
        PHK
        PLB
        JSR MAINSUB
        PLB
        RTL
        
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Sprite Main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
POINTLOSS:
	db $00,$01,$03,$03,$05
AWARD:
	db $01,$02,$03,$03,$04

ANSWERDIGIT1:	db $00,$00,$00	;questions 0~2
		db $01,$00,$00	;3~5
		db $02,$01,$07	;6~8
		db $00		;9

ANSWERDIGIT2:	db $01,$04,$02
		db $00,$00,$09
		db $01,$05,$02
		db $02

ANSWERDIGIT3:	db $03,$08,$05
		db $00,$05,$02
		db $04,$03,$09
		db $02

ANSWERSUBMIT:
	LDA $7F9B01
	BNE CHECK
	RTS
CHECK:
	LDA #$00
	STA $7F9B01

	LDA $7F9A7B	;answer given digit 1 (far left)
	CMP $7F9A7E	;correct answer digit 1
	BNE WRONG
	LDA $7F9A7C	;answer given digit 2
	CMP $7F9A7F	;correct
	BNE WRONG
	LDA $7F9A7D	;given 3
	CMP $7F9A80	;correct
	BNE WRONG
	LDA #$29
	STA $1DFC
	JSR RESET
	RTS
WRONG:
TIMEUP:
	LDA $157C,x	;number of answers WRONG
	INC
	;TAY
	;LDA POINTLOSS,y
	;STA $7F9A82
	;TYA
	STA $157C,x
	INC $1528,x	;# questions asked
	LDA #$2A
	STA $1DFC
	JSR RESET

	BRA NOSOUND

MAINSUB:
	LDA #$04
	STA $13D3
	LDA $7FAB10,x
 	AND #$04
	BEQ TARGET
	RTS
TARGET:	LDA !RAM_TimeLeft	; Play warning sounds

	BEQ TIMEUP
	CMP #$1C
	BEQ SOUND
	CMP #$14
	BEQ SOUND
	CMP #$0C
	BNE NOSOUND
SOUND:	
	LDA #$2a
	STA $1DFC
NOSOUND:

	
Gfx:	
	JSR SUBGFX		; Draw the sprite
NOGFX:	

	LDA $9D			; RETURN if sprites are locked
	BNE RETURN0
	LDA $13D4
	BNE RETURN0


COUNTDOWN:
	LDA $13
	AND #!CountDownFrequency
	BNE SKIPMETER

	LDA !RAM_TimeLeft	
	DEC A	
	STA !RAM_TimeLeft
	
	SKIPMETER:	

		JSR ANSWERSUBMIT	
		LDA $1a			; Position = Screen boundary
		CLC
		ADC #$80
		STA $E4,x		; This is done so the sprite stays on screen
		LDA $1b
		ADC #$00
		STA $14E0,x
		LDA $1c
		STA $D8,x
		LDA $1d
		STA $14D4,x
RETURN0:	
	RTS


KILLMARIO:
NOCHECK:

	RTS


RESET:
	LDA #$00	;RESET all inputted digits to 0
	STA $7F9A7B
	STA $7F9A7C
	STA $7F9A7D

	LDA $157C,x	;4 WRONG answers and done
	CMP #$04
	BEQ GAMEOVER

	LDA $7F9B00
	INC A
	CMP #$0A	;used to be $10
	BEQ GAMEOVER
	STA $7F9B00
	TAY
	LDA ANSWERDIGIT1,y
	STA $7F9A7E
	LDA ANSWERDIGIT2,y
	STA $7F9A7F
	LDA ANSWERDIGIT3,y
	STA $7F9A80

	LDA #$9F
	STA !RAM_TimeLeft
	RTS

GAMEOVER:
	LDY $157C,x
	LDA POINTLOSS,y
	STA $7F9A82
	LDA AWARD,y
	XBA
	LDA $7F9A9F
	PHX
	TAX
	XBA
	STA $7F9A91,x
	PLX

	LDA $7F9A83
	ORA #$02
	STA $7F9A83

	LDA #$05
	STA $71
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Graphics Routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	!NumTiles = $09
	
SPRITETILEDISPY:	db $00,$00,$00,$00,$00,$18,$18,$18,$18
SPRITETILEDISPX:	db $01,$10,$20,$30,$40,$F8,$10,$20,$30
SPRITEGFXPROP:	db $00,$00,$00,$00,$00,$00,$00,$00,$00

PALETTES:           db $09,$05,$0B
	
SUBGFX:

	LDA $15F6,x		; NOTE: Reinstate to use the palette from the cfg file
	STA $02
	
	LDA !RAM_TimeLeft	; Load address to be timed
	LSR
	LSR
	LSR
	STA $03
	
        LDY $15EA,x             ; get offset to sprite OAM                           

        PHX
        LDX #$00
	;LDX #!NumTiles
GFXLOOPSTART:

        LDA #!MeterPositionX
	CLC
	ADC SPRITETILEDISPX,x
        STA $0300,Y
	
	LDA #!MeterPositionY
	CLC			;
	ADC SPRITETILEDISPY,x
        STA $0301,Y

	LDA $03
	JSR GETTILE
        STA $0302,Y
	
        LDA $02
        ORA SPRITEGFXPROP,x
	ORA #$20
        STA $0303,Y
	
        INY    
        INY                     
        INY                     
        INY                     

	INX
	CPX #!NumTiles
        BCC GFXLOOPSTART
	PLX

        LDY #$02
        LDA #!NumTiles
        JSL $01B7B3
        RTS                       

GETPALETTE:
	LDY #$00
	LDA !RAM_TimeLeft
	CMP #$55
	BCC LOADPALETTE
	INY
	CMP #$AA
	BCC LOADPALETTE
	INY
LOADPALETTE:
	LDA PALETTES,y
	RTS	
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


QUESTION0TILES:	;6 + 7
	db $AA,$8C,$A6,$8E,$AA,$A4	;A6 = plus / A8 = minus / A4 = equals
QUESTION1TILES:	;8 x 6
	db $AA,$A0,$AC,$8C,$AA,$A4
QUESTION2TILES:	;34 - 9
	db $86,$88,$A8,$A2,$AA,$A4	;$80 = 0, $82 = 1... etc $A2 = 9
QUESTION3TILES:	;49 + 51
	db $88,$A2,$A6,$8A,$82,$A4
QUESTION4TILES:	;20 / 4
	db $84,$80,$AE,$88,$AA,$A4
QUESTION5TILES:	;46 x 2
	db $88,$8C,$AC,$84,$AA,$A4
QUESTION6TILES:	;856 / 4
	db $A0,$8A,$8C,$AE,$88,$A4
QUESTION7TILES:	;64 + 89
	db $8C,$88,$A6,$A0,$A2,$A4
QUESTION8TILES:	;9 x 9 x 9
	db $A2,$AC,$A2,$AC,$A2,$A4
QUESTION9TILES:	;8 + 9 + 5
	db $A0,$A6,$A2,$A6,$8A,$A4
;!Answers:
;	db $80,$80,$80,$80,$80,$80,$80,$80,$80

ANSWERTILES:
DEX
DEX
DEX
DEX
DEX
DEX
	LDA !Answers,x
	ASL
	CLC
	ADC #$80
	CMP #$8F
	BCC NOADD
	CLC
	ADC #$10
NOADD:

INX
INX
INX
INX
INX
INX	
	JMP FINNOPULL
	



GETTILE:
	CPX #$08
	BEQ ANSWERTILES
	CPX #$07
	BEQ ANSWERTILES
	CPX #$06
	BEQ ANSWERTILES
	PHY
	PHA
	LDA $7F9B00
	TAY
	PLA
	CPY #$00
	BEQ QUESTION0
	CPY #$01
	BEQ QUESTION1
	CPY #$02
	BEQ QUESTION2
	CPY #$03
	BEQ QUESTION3
	CPY #$04
	BEQ QUESTION4
	CPY #$05
	BEQ QUESTION5
	CPY #$06
	BEQ QUESTION6
	CPY #$07
	BEQ QUESTION7
	CPY #$08
	BEQ QUESTION8
	CPY #$09
	BEQ QUESTION9
	BRA QUESTION0
TILEFINISH:	
	PLY
FINNOPULL:
	RTS

QUESTION0:
	LDA QUESTION0TILES,x
	BRA TILEFINISH
QUESTION1:
	LDA QUESTION1TILES,x
	BRA TILEFINISH
QUESTION2:
	LDA QUESTION2TILES,x
	BRA TILEFINISH
QUESTION3:
	LDA QUESTION3TILES,x
	BRA TILEFINISH
QUESTION4:
	LDA QUESTION4TILES,x
	BRA TILEFINISH
QUESTION5:
	LDA QUESTION5TILES,x
	BRA TILEFINISH
QUESTION6:
	LDA QUESTION6TILES,x
	BRA TILEFINISH
QUESTION7:
	LDA QUESTION7TILES,x
	BRA TILEFINISH
QUESTION8:
	LDA QUESTION8TILES,x
	BRA TILEFINISH
QUESTION9:
	LDA QUESTION9TILES,x
	BRA TILEFINISH
	
CALCULATETILE:
	CPX #$00
	BEQ ENDPIECE
	CPX #!NumTiles
	BEQ ENDPIECE

	PHA
	TXA
	DEC A
	ASL
	ASL
	ASL
	STA $00
	PLA

	SEC
	SBC $00
	BMI EMPTYTILE
	
	CMP #$08 	
	BCS FULLTILE

	AND #$07
	CLC
	ADC #$47
	CMP #$4B
	BCC RETURN
	CLC
	ADC #$0C
RETURN:	
	RTS

FULLTILE:
	LDA #$46
	RTS
EMPTYTILE:
	LDA #$47
	RTS
ENDPIECE:
	LDA #$56
	RTS
	