;the name is truntec

!ram = $0F5F|!Base2
!hp = $05
!sfx = $28		;when boss is hit/hurt
!bank = $1DFC|!Base2

print "INIT ",pc
	phb : phk : plb
	stz !1510,x	;eye should close flag
	stz !157C,x ;not direction
	;stz !C2,x
	lda !1686,x
	ora #$09
	sta !1686,x ;tweaker
	lda !166E,x
	ora #$30
	sta !166E,x
	lda !167A,x
	ora #$02
	sta !167A,x
	lda !7FAB10,x
	and #$04
	beq .notset
	jsl $01ACF9|!BankB
	lda $148D|!Base2
	and #$03
	sta !1FD6,x
	lda #$80
	tsb !ram
	lda #$30
	sta !163E,x
	plb : rtl
.notset
	lda #$70
	sta !1504,x ;bobombs?
	lda #$70 ;78
	sta !E4,x
	stz !14E0,x
	lda #$20
	sta !D8,x
	lda #$01
	sta !14D4,x
	plb : rtl
.monty_init
	lda #$40
	sta !1540,x
	plb : rtl
	
print "MAIN ",pc
	phb : phk : plb

;blindedit: this makes it so when the player falls offscreen (death) truntec won't loop its eyes every time the low byte loops.
REP #$20
LDA $96
CMP #$01C0
BCC +

LDA #$01C0
STA $96

+
SEP #$20

	lda !7FAB10,x
	and #$04
	bne .sub
	lda !1504,x
	beq +
	dec !1504,x
	+ jsr Code
	- plb : rtl
.sub
;	lda !1602,x
;	bne +
	jsr SubCode
	bra -
;	+ jsr MontyMain
;	bra -
Code:
	LDA #$00
	%SubOffScreen()

	jsr Graphics
	lda $9D
	bne +
	stz !B6,x
	stz !AA,x
	jsl $01802A|!BankB
	lda !1528,x
	cmp #!hp
	bcs .kill
	
	lda !1510,x	;dont hurt tree if eye is closed
	bne +
	
	jsr HurtTree
	+

	jsr SpawnBomb
	lda $14
	bne +
	lda !ram ;d7 set = dont spawn vine
	bmi + ;bmi?
	jsr Spawn
	.end
	+ rts
.kill 
	lda #$3F
	sta $1887|!Base2 ;shake ground ?
	lda #$18 ;thunder sfx
	sta !bank
	phx
	ldx #!SprSize-1 ;what the shit man fuck so indie
	- stz !14C8,x
	dex
	bpl -
	plx

;new level ending code by blind devil (uberasm handled), refer to gamemode/Goal.asm for values to set for $1696.
;death doesn't freeze sprites/animation anymore (unless reverted but I like it nonetheless - EDIT: IT WAS REVERTED),
;but the code also takes this in account so don't worry.
LDA #$0B		;load action value
STA $1696|!Base2	;store to screen barrier flag (play boss victory song and set normal exit).

LDA #$01		;load value
STA $7FC0F8		;store to one-shot exanimation flag address 1.
	rts
	
Graphics:
	%GetDrawInfo()

	lda $14
	lsr #4
	and #$07
	phx
	tax
	lda .which,x
	plx
	sta $07
	lda $07 ;needed?
	beq +
	lda #$01 : sta !1510,x
	bra ++
	+ stz !1510,x
	++
.closeeye
	phx
	ldx #$03
.loop
	lda $00
	clc
	adc .xeyelid,x
	sta $0300|!Base2,y
	lda $01
	clc
	adc .yeyelid,x
	sta $0301|!Base2,y
	phx
	txa
	clc : adc $07 
	tax
	lda .eyelidtiles,x
	plx
	sta $0302|!Base2,y
	lda #$03
	sta $0303|!Base2,y

	iny
	iny
	iny
	iny
	dex
	bpl .loop
	plx
	
	
	iny #4
	jmp +++
.hurt
	lda $14
	lsr #2
	and #$07
	phx
	tax
	lda .movex,x
	clc
	adc $00
	sta $0300|!Base2,y
	lda .movey,x
	plx
	clc
	adc $01
	adc #$10
	sta $0301|!Base2,y
	jmp .branch	
+++
	lda !154C,x
	bne .hurt
	
	lda $D1
	and #$F0 ;useless? Yes.
	lsr #4
	phx
	tax
	lda .xdisp,x
	plx
	sta $03
	
	;lda $9D ;delete?
	;bne + ;
	phx
	lda $D3
	and #$F0
	lsr #4
	tax
	lda .ydisp1,x
	plx
	sta $04
	lda $D4
	bne +
	lda #$04
	sta $04
	+
	lda $00
	clc
	adc $03
	sta $0300|!Base2,y
	lda $01
	clc
	adc $04
	sta $0301|!Base2,y
.branch
	lda #$AA
	sta $0302|!Base2,y
	lda #$33
	ora $64
	sta $0303|!Base2,y

	ldy #$02
	lda #$05		;#$05? but why? #$04 doesn't seem to work so...
	jsl $01B7B3|!BankB
	
	rts
.which
db $04,$08,$08,$04
db $00,$00,$00,$00

.xeyelid
db $00,$10,$00,$10

.yeyelid
db $FF,$FF,$0F,$0F

.eyelidtiles
db $A8,$A8,$A8,$A8
db $C0,$C2,$C4,$C6
db $E0,$E2,$E4,$E6

.xdisp
db $04,$04,$05,$06
db $08,$08,$09,$0B
db $0C,$0E,$0F,$10
db $12,$13,$14,$14

.ydisp1
db $08,$0A,$0C,$0E
db $10,$10,$10,$10
db $10,$10,$10,$10
db $10,$10,$10,$10

.movex
db $0C,$04,$04,$04
db $0C,$14,$14,$14
.movey
db $00,$00,$FC,$F8
db $F6,$F8,$FC,$00

SubCode: ;vines/wires
	LDA #$00
	%SubOffScreen()
	lda !163E,x
	bne Notyet
	jsr WireGraphics
	lda $9D
	bne +
	lda !C2,x
	cmp #$04
	beq .kill
	;lda !C2,x
	and #$03
	tay
	lda !1540,x
	beq .changestate
	lda .speed,y
	sta !AA,x
	jsl $01801A|!BankB
	jsr CustomContact
	+ rts
.changestate
	lda .time,y
	sta !1540,x
	inc !C2,x
	rts
.kill
	stz !14C8,x
	lda !ram
	and #$7F
	sta !ram
	rts
.time
db $55,$30,$20,$30
.speed
db $00,$10,$00,$D0 ;changed $d0-->$c0
Notyet:
	jsr .notyetgraphics ;fuck get draw info
	stz !AA,x
	jsl $01801A|!BankB
	rts

.notyetgraphics
	%GetDrawInfo()
	lda $14
	lsr #2
	and #$03
	phx
	tax
	lda $00
	clc
	adc .xdisp,x
	sta $0300|!Base2,y ;sta $0240,y
	lda $01
	clc
	adc .ydisp,x
	sec
	sbc #$10 ;changed, because the vine length
	sta $0301|!Base2,y ;sta $0241,y
	lda .tiles,x
	plx
	sta $0302|!Base2,y ;sta $0242,y
	lda #$39
	ora $64
	sta $0303|!Base2,y ;sta $0243,y

	ldy #$00
	tya
	jsl $01B7B3|!BankB
	rts

.xdisp
db $04,$FE,$06,$F9
.ydisp
db $70,$7D,$86,$71
.tiles
db $AE,$AF,$AE,$AF

WireGraphics:
	%GetDrawInfo()
	lda !1FD6,x
	and #$03
	phx
	tax
	lda .offset,x
	sta $04
	lda $14
	lsr #2
	and #$03
	sta $03
	ldx #$05
.loop
	lda $00
	sta $0300|!Base2,y
	lda $01
	sec
	sbc #$04 ;clipping
	clc
	adc .ydisp,x
	sta $0301|!Base2,y

	lda $04
	clc
	adc $03
	phx
	tax
	lda.w .tiles,x
	plx
	
	sta $0302|!Base2,y
	lda #$2F
	sta $0303|!Base2,y
	iny #4
	dex
	bpl .loop
	lda $03
	and #$01
	tax
	lda $00
	sta $0300|!Base2,y
	lda $01
	clc
	adc #$5C
	sta $0301|!Base2,y
	lda .tiles2,x
	plx
	sta $0302|!Base2,y
	lda #$2F
	sta $0303|!Base2,y

	ldy #$02
	lda #$06
	jsl $01B7B3|!BankB
	rts
.ydisp
db $00,$10,$20,$30,$40,$50
.tiles
db $C8,$E8,$E8,$C8,$E8,$C8
db $E8,$C8,$E8,$E8,$C8,$E8
db $E8,$C8,$C8,$E8,$C8,$C8
db $C8,$E8,$C8,$C8,$E8,$E8
.tiles2
db $EA,$EC
.offset
db $00,$06,$0C,$12

Spawn:
	stz !C2,x
	phy
	jsl $01ACF9|!BankB
	lda $148D|!Base2
	and #$07 ;changed #$03
	asl #2
	sta !C2,x
	lda !1528,x
	and #$07
	asl A
	phx
	tax
	rep #$20
	lda.w .tablething,x
	sta $00
	sep #$20
	plx
	jmp ($0000)
	
	.four
	jsr .subroutine ;oh shit nigger what are you doin
	inc !C2,x
	.three
	jsr .subroutine
	inc !C2,x
	.two
	jsr .subroutine
	inc !C2,x
	.one
	jsr .subroutine
	ply
	rts
.tablething
dw Spawn_one,Spawn_two,Spawn_three,Spawn_four
dw Spawn_four,Spawn_four,Spawn_four,Spawn_four
.subroutine
	phx
	jsl $02A9DE ;spr=y, main=x
	bmi +
	lda #$01
	sta !14C8,y
	lda !7FAB9E,x
	phx
	tyx
	sta !7FAB9E,x
	plx
	lda !C2,x
	phx
	tax
	lda .xlo,x
	sta !E4,y
	lda #$00
	sta !14E0,y
	plx
	lda #$B0 ;$a0 changed ;possibly draw another tile on each vine and move back up?
	sta !D8,y
	lda #$00
	sta !14D4,y
	sta !1602,y
	phx
	tyx
	jsl $07F7D2|!BankB
	jsl $0187A7|!BankB
	lda #$0C ;8|4
	sta !7FAB10,x
	plx
	+ plx : rts
.xlo ;10-e0
db $40,$60,$80,$A0
db $60,$C0,$D0,$E0
db $50,$70,$B0,$C0
db $30,$90,$B0,$D0

db $C0,$80,$50,$A0
db $90,$40,$B0,$E0
db $A0,$D0,$30,$40
db $D0,$B0,$50,$20

.xhi
db $00

CustomContact:
.clipping
	stz $0F
	lda #$00 ;xdisp
	bpl +
	dec $0F
	+ clc
	adc !E4,x
	sta $04
	lda !14E0,x
	adc $0F
	sta $0A
	lda #$10 ;width
	sta $06
	stz $0F
	lda #$00 ;ydisp
	bpl +
	dec $0F
	+ clc
	adc !D8,x
	sta $05
	lda !14D4,x
	adc $0F
	sta $0B
	lda #$70 ;length
	sta $07
	jsl $03B664 ;mario clipping
	jsl $03B72B ;check
	bcc .no_contact
	jsl $00F5B7
.no_contact
	rts
HurtTree:
	phx
	jsl $03B6E5|!BankB
	phx
	ldx #$0C
.loop
	dex
	bmi .done
	lda !14C8,x
	beq .loop
	txa
	cmp $01,s
	beq .loop
	jsl $03B69F|!BankB
	jsl $03B72B|!BankB
	bcc .loop
	lda !14C8,x
	cmp #$09
	bcc .loop
	txy ;new spr
	plx ;tree
	lda #$00
	sta !14C8,y ;kill bobomb
	inc !1528,x
	lda #$3F
	sta $1887|!Base2 ;shake ground ?
	sta !154C,x ;hurtframe counter
	lda #!sfx
	sta !bank
	phx
	.done
	plx
	
	plx
	rts
SpawnBomb:
	phx
	lda !1504,x
	bne .return
	lda $14
	and #$FF ;change? 3f
	ora $9D
	bne .return
	lda $7F1234
	lda #$70
	sta !1504,x
	jsl $02A9DE|!BankB
	bmi .return
	tyx
	lda #$01
	sta !14C8,x
	lda #$0D ;bobomb
	sta $9E,x
	jsl $07F7D2|!BankB
	phx
	jsl $01ACF9|!BankB
	lda $148D|!Base2
	and #$01
	tax
	lda .table,x
	plx
	sta !E4,x
	lda #$00
	sta !14E0,x
	sta !14D4,x
	lda #$E0 ;changed?
	sta !D8,x
	;lda $D1 ;change ;delete ;$7f1234 etc etc
.return
	plx
	rts
.table
db $20,$E0

Full_Monty:
	pha ;
	lda !ram ;
	pla ;
SpawnMonty:
	bit !ram
	bvs .return
	lda !ram
	ora #$40
	sta !ram
;spawn monty sprite
.return
	rts