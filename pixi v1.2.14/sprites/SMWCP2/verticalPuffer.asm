;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; SMW Dolphin (sprites 41-43), by imamelia
;;
;; This is a disassembly of sprites 41-43 in SMW, the dolphins.
;;
;; Uses first extra bit: NO
;; Uses extra property bytes: YES
;;
;; The first extra property byte determines which dolphin this sprite will act like.
;; (Add 41 to it to get the equivalent sprite number.)
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; defines and tables
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

YSpeeds:
db $F0,$DC,$D0,$C8,$C0,$B8,$B2,$AC
db $A6,$A0,$9A,$96,$92,$8C,$88,$84
db $80,$04,$08,$0C,$10,$14

tiles:
db $6B,$6D


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; init routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "INIT ",pc
LDA !D8,x		; sprite Y position low byte
STA !1528,x		; save in multi-purpose table
LDA !14D4,x		; sprite Y position high byte
STA !151C,x		; same
			;
LiquidContactLoop:	;
			;
LDA !D8,x		;
CLC			;
ADC #$10		;
STA !D8,x		; shift the sprite's Y position down 1 tile
LDA !14D4,x		;
ADC #$00		;
STA !14D4,x		;
			;
;JSL $019138		; make the sprite interact with objects
			;
;LDA !164A,x		; if the sprite is not in contact with water or lava...
;BEQ LiquidContactLoop	; shift it down again until it is

;JSR SetYSpeed		;
LDA !1528,x
STA !D8,x
LDA !151C,x
STA !14D4,x
			;
			;
			;
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine wrapper
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

print "MAIN ",pc
PHB
PHK
PLB
JSR Main
PLB
RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;; main routine
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Main:
LDA #$00
%SubOffScreen()
LDA $13BF|!Base2
CMP #$17
BNE NoAdjust
LDA !166E,x
AND #$EF
STA !166E,x
NoAdjust:

JSR Graphics	;
LDA !14C8,x
CMP #$08
BNE Return0
LDA $9D		; if sprites are locked...
BNE Return0	; return

JSL $01801A|!BankB	;
JSL $01A7DC|!BankB

LDA !AA,x	; sprite Y speed
BMI MovingUp	; if the sprite is moving down...
CMP #$3F	; and its Y speed has not reached 3F...
BCS MaxYSpeed	;
MovingUp:	;
INC !AA,x	; increment its Y speed
MaxYSpeed:	;

TXA		; sprite index -> A
EOR $13		;
LSR		; every other frame depending on the sprite index...
BCC NoObjInteract;don't interact with objects
JSL $019138|!BankB	;
NoObjInteract:	;
LDA !AA,x	; if the sprite Y speed is negative...
BMI FinishMain	;
LDA $164A,x	; or the sprite isn't touching any water...
BEQ FinishMain	; skip down to the interaction
LDA !AA,x	;
SEC		;
SBC #$08	; diminish the sprite Y speed by 8
STA !AA,x	;
BPL NoZeroYSpeed; and if the result was negative...
STZ !AA,x	; just reset it to 0
NoZeroYSpeed:	;

SetYSpeed:
LDA !AA,x
BNE FinishMain
LDA !D8,x	;
SEC		;
SBC !1528,x	;
STA $00		;
LDA !14D4,x	;
SBC !151C,x	;
LSR		;
ROR $00		; uh...
LDA $00		;
LSR #3		;
TAY		;
LDA YSpeeds,y	;
BMI StoreYSpeed	;
STA !1564,x	; ...
LDA #$80	;
StoreYSpeed:	;
STA !AA,x	;
FinishMain:

Return0:
RTS		;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Graphics:		
			LDA $14
			AND #$04
			LSR #2
			STA !1626,x
			NoIncrement:
		
			PHY
			LDY #$01
			LDA !B6,x
			BPL LABELTHING
			DEY
			LABELTHING:
			TYA
			ASL #6
			STA $0E
			LDA #$2D
			ORA $0E
			STA $0E
			PLY

			%GetDrawInfo()

			REP #$20
			LDA $00
			STA $0300|!Base2,y
			SEP #$20
			
			LDA !1626,x
			PHX
			TAX
			LDA tiles,x
			PLX
			STA $0302|!Base2,y
	
			LDA $0E
			STA $0303|!Base2,y

			LDY #$02                ; \ we've already set 460 so use FF
			LDA #$00                ; | A = number of tiles drawn - 1
			JSL $01B7B3|!BankB      ; / don't draw if offscreen
QuitGraphics:		RTS                     ; return