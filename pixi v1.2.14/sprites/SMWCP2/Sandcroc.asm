;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SandCroc
; by Sonikku
; Description: Goes underground and chases Mario (And stops when it hits a solid
; surface underground, so the area it chases you in should be set to something
; like tile 25, and the top tile something like 100). If Mario is RIGHT above this
; sprite, it chomps upward. It won't go back underground until shot with a fireball,
; but while it sits aboveground, it acts solid.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; INIT and MAIN JSL targets
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

	PRINT "INIT ",pc
	%SubHorzPos()
	TYA
	STA $157C,x
	LDA $D8,x
	CLC
	ADC #$03
	STA $D8,x
	LDA $14D4,x
	ADC #$00
	STA $14D4,x
	LDA $D8,x
	STA $151C,x
	LDA $14D4,x
	STA $157C,x
	RTL

	PRINT "MAIN ",pc			
	PHB
	PHK				
	PLB				
	JSR SPRITE_ROUTINE	
	PLB
	RTL     


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; SPRITE_ROUTINE
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
XSPD:	db $20,$E0,$E0,$20
RISEFRM:	db $02,$01,$00,$00,$00,$00,$00,$00,$00,$00
CLIP:	db $10,$14,$10,$10
RETURN:	RTS
SPRITE_ROUTINE:
	LDA #$00
	%SubOffScreen()
	LDA $64
	PHA
	LDA $C2,x
	BEQ NOGFX
	LDA #$10
	STA $64
	JSR SUB_GFX
NOGFX:	PLA
	STA $64
	LDA $9D
	BNE RETURN
	LDA $14C8,x
	CMP #$08
	BNE RETURN
	JSL $01A7DC
	BCS +
	JMP NOCON

+
	%SubVertPos()
	LDA $1602,x
	CMP #$01
	BEQ SPRWINS
	CMP #$02
	BEQ SPRWINS
	CMP #$03
	BNE NOCON
	LDA $0E
	CMP #$E6
	BPL SIDES
	LDA $7D
	BMI NOCON
	LDA #$01
	STA $1471
	LDA #$06
	STA $154C,x
	STZ $7D
	LDA #$DE
	LDY $187A
	BEQ NOYOS
	LDA #$CE
NOYOS:	CLC
	ADC $D8,x
	STA $96
	LDA $14D4,x
	ADC #$FF
	STA $97
	LDY #$00
	LDA $1491
	BPL LABEL9
	DEY
LABEL9:	CLC
	ADC $94
	STA $94
	TYA
	ADC $95
	STA $95
	BRA NOCON
SPRWINS:	JSL $00F5B7
	BRA NOCON
SPINJMP:	JSL $01AA33
	JSL $01AB99
	LDA #$02
	STA $1DF9
	BRA NOCON
OFFSET:	db $01,$00,$FF,$FF
SIDES:	STZ $7B
	%SubHorzPos()
	TYA
	ASL A
	TAY
	REP #$20
	LDA $94
	CLC
	ADC OFFSET,y
	STA $94
	SEP #$20
NOCON:	LDA $1528,x
	BEQ NOFIRE
	STZ $1528,x
	INC $1594,x
	LDA #$02
	STA $C2,x
	LDA #$05
	DEC A
	CMP $1594,x
	BCS NOFIRE
	LDA #$04
	STA $14C8,x
	LDA #$1F
	STA $1540,x
	LDA #$04
	JSL $02ACE5
	JSL $07FC3B
	LDA #$08
	STA $1DF9
NOFIRE:	JSL $01802A
	LDA $C2,x
	JSL $0086DF
	dw CHASE&$FFFF
	dw CHOMP&$FFFF
	dw SINK&$FFFF
CHASE:	LDA #$18
	STA $1686,x
	LDA #$A7
	STA $167A,x
	STZ $AA,x
	LDA $14
	AND #$01
	BNE SETSPD
	%SubHorzPos()
	TYA
SETSPD:	LDA $1588,x
	AND #$03
	BEQ NOWALL
	INY
	INY
NOWALL:	LDA XSPD,y
	STA $B6,x
	LDA $D8,x
	SEC
	SBC $96
	STA $0F
	LDA $14D4,x
	SBC $97
	BMI RETURN0
	BNE RETURN0
	LDA $0F
	CMP #$28
	BCS RETURN0
	%SubHorzPos()
	LDA $0E
	CLC
	ADC #$04
	CMP #$08
	BCS RETURN0
	LDA $163E,x
	BNE RETURN0
	LDA $77
	AND #$04
	BEQ RETURN0
	LDA #$01
	STA $C2,x
	LDA #$11
	STA $1540,x
RETURN0:	RTS
CHOMP:	LDA #$A1
	STA $167A,x
	LDY $1602,x
	LDA CLIP,y
	STA $1662,x
	STZ $B6,x
	LDA $1540,x
	BEQ NOSPD
	LDA #$E0
	STA $AA,x
	LDA $1540,x
	LSR A
	TAY
	LDA RISEFRM,y
	STA $1602,x
	LDA #$01
	CMP $1540,x
	BNE RETURN1
	LDA #$19
	STA $1DFC
	LDY #$03
FNDFREE:	LDA $17C0,y
	BEQ FNDONE
	DEY
	BPL FNDFREE
	RTS
FNDONE:	LDA #$02
	STA $17C0,y
	LDA $D8,x
	SEC
	SBC #$10
	STA $17C4,y
	LDA #$04
	STA $17CC,y
	LDA $E4,x
	STA $17C8,y
RETURN1:	RTS
NOSPD:	STZ $AA,x
	LDA #$03
	STA $1602,x
	RTS
SINK:	LDA #$98
	STA $1686,x
	LDA #$A1
	STA $167A,x
	STZ $B6,x
	LDA $157C,x
	CMP $14D4,x
	BNE RETURN2
	LDA $151C,x
	CMP $D8,x
	BEQ RESET
	LDA #$10
	STA $AA,x
	RTS
RESET:	STZ $C2,x
	LDA #$20
	STA $163E,x
RETURN2:	RTS
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
TILEMAP:		db $00,$00,$00,$02,$04
		db $20,$24,$2A,$2C,$2E
		db $06,$06,$08,$0C,$0E
		db $26,$26,$28,$0C,$0E
X_OFFSET:	db $F0,$F0,$F0,$00,$10
		db $F0,$10,$F0,$00,$10
		db $F8,$F8,$08,$F8,$08
		db $F8,$F8,$08,$F8,$08
Y_OFFSET:	db $07,$07,$07,$07,$07
		db $FC,$FC,$0C,$0C,$0C
		db $FC,$FC,$FC,$0C,$0C
		db $FC,$FC,$FC,$0C,$0C

SUB_GFX: %GetDrawInfo()      ; sets y = OAM OFFSET
	LDA $1602,x
	ASL A
	ASL A
	ADC $1602,x
	STA $03             
	
	PHX
	LDX #$04
LOOP_START:          PHX
	TXA
	CLC
	ADC $03
	TAX
	LDA X_OFFSET,x
	CLC
	ADC $00                 ; \ tile x position = sprite y location ($01)
	STA $0300,y             ; /

	LDA Y_OFFSET,x
	CLC
	ADC $01                 ; \ tile y position = sprite x location ($00)
	STA $0301,y             ; /

	LDA TILEMAP,x
	STA $0302,y 
	PLX
	
	PHX
	LDX $15E9
	LDA $15F6,x             ; tile properties xyppccct, format
	ORA $64                 ; add in tile priority of level
	STA $0303,y             ; store tile properties
	PLX
	INY	 ; \ increase index to sprite tile map ($300)...
	INY	 ;  |    ...we wrote 1 16x16 tile...
	INY	 ;  |    ...sprite OAM is 8x8...
	INY	 ; /    ...so increment 4 times
	DEX
	BPL LOOP_START

	PLX
	LDY #$02                ; \ 460 = 2 (all 16x16 tiles)
	LDA #$04                ;  | A = (number of tiles drawn - 1)
	JSL $01B7B3             ; / don't draw if offscreen
	RTS	 ; RETURN