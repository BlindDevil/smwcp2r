;Frank's rock wall projectile, or death water puddle animation (extra bit), by Blind Devil
;yes I had to recode it from scratch. wonderful.

;defines and tables for wall projectile:

TILE:
db $0C,$0E	;top, bottom

YDISP:
db $F0,$00

!PalProp = $27	;we will ignore CFG

XSPEED:
db $30,$D0	;right, left

;defines and tables for water puddle:

TILE2: 		db $84,$94,$BA,$BB,$94,$84
XDISP2:  	db $F0,$F8,$00,$08,$10,$18
PROP2:		db $0D,$0D,$0D,$0D,$4D,$4D

print "MAIN ",pc
PHB
PHK
PLB

LDA !7FAB10,x
AND #$04
BNE ExtraBitSet

JSR RockWall
BRA Over

ExtraBitSet:
JSR WaterPuddle

Over:
PLB

print "INIT ",pc
RTL

WaterPuddle:
JSR Graphics2

LDA !14C8,x
CMP #$08
BNE Return
LDA $9D
BNE Return

LDA !C2,x
CMP #$60
BCS Return

INC !C2,x

LDA #$FE
STA !AA,x
JSL $01801A|!BankB

Return:
RTS

RockWall:
JSR Graphics1

LDA !14C8,x
CMP #$08
BNE Return
LDA $9D
BNE Return

LDA #$00
%SubOffScreen()

STZ !AA,x
LDY !157C,x
LDA XSPEED,y
STA !B6,x

LDA !15AC,x
BNE +
LDA !1588,x
AND #$03
BEQ +

LDA !C2,x
CMP #$01
BCS .killwall

LDA !157C,x
EOR #$01
STA !157C,x
LDA #$08
STA !15AC,x
INC !C2,x

+
JSL $01802A|!BankB
JSL $01A7DC|!BankB
RTS

.killwall
LDA #$1B
STA !1540,x
LDA !D8,x
SEC
SBC #$08
STA !D8,x
LDA !14D4,x
SBC #$00
STA !14D4,x
LDA #$04
STA !14C8,x
RTS

Graphics1:
%GetDrawInfo()

PHX
LDX #$01

.gfxloop
LDA $00
STA $0300|!Base2,y
LDA $01
CLC
ADC YDISP,x
STA $0301|!Base2,y
LDA TILE,x
STA $0302|!Base2,y
LDA #!PalProp
STA $0303|!Base2,y
INY #4
DEX
BPL .gfxloop

PLX
LDY #$02
LDA #$01

FinishOAMWrite:
JSL $01B7B3|!BankB
RTS

Graphics2:
%GetDrawInfo()

PHX
LDX #$05

.gfxloop
LDA $00
CLC
ADC XDISP2,x
STA $0300|!Base2,y
LDA $01
CLC
ADC #$15
STA $0301|!Base2,y
LDA TILE2,x
STA $0302|!Base2,y
LDA PROP2,x
ORA $64
STA $0303|!Base2,y
INY #4
DEX
BPL .gfxloop

PLX
LDY #$00
LDA #$05
BRA FinishOAMWrite