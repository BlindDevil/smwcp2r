!FreeRam3 = $0F7D|!Base2

PROPERTIES2: db $4F,$0F

	print "MAIN ",pc
	PHB
	PHK
	PLB
	JSR MainCode
	PLB
	print "INIT ",pc
	RTL
	
MainCode:
	JSR HandleGFX
	LDA $9D
	BNE Return
	LDA !1594,x : JSL $8086DF		; handle the state thingy
	dw LeafA
	dw ShockWave
	dw OtherShockWave
	dw Stone

HandleGFX:
	LDA !1594,x : JSL $0086DF|!BankB
	dw Graphics
	dw Graphics2
	dw Graphics2
	dw Graphics2

LeafA:
	JSR Contact
	LDA $9D
	BNE Return
	JSL $01801A|!BankB
	JSL $018022|!BankB
	JSL $019138|!BankB
	LDA !1588,x
	AND #$03
	BNE Kill
Return:
	RTS 
Contact:
	JSL $01A7DC|!BankB					; \ If Mario 
	BCC Return					; | has Contact 
	JSL $00F5B7					; | hurt him
	RTS
Kill:
	JSR DrawSmoke
	STZ !14C8,x
	RTS
OtherShockWave:
	JSR Contact
	LDA #$30
	STA $B6,x
	LDA #$01
	STA $157C,x
	BRA SpdSet	
ShockWave:
	JSR Contact
	LDA #$D0
	STA !B6,x
SpdSet:
	JSL $01802A
	LDA $1588,x
	AND #$03
	BNE Kill	
	RTS
Stone:
	JSR Contact
	LDA #$34
	STA $AA,x
	JSL $01802A
	LDA $1588,x
	AND #$04
	BNE Kill
	RTS	
;==================================================================
;Draw Smoke
;==================================================================

DrawSmoke:
STZ $00 : STZ $01
LDA #$1B : STA $02
LDA #$01 : %SpawnSmoke()
RTS
					
;==================================================================
; GFX Routine
;==================================================================
TILEMAP:
	db $2F,$3F,$6F,$7F

Graphics:           %GetDrawInfo()       ; sets y = OAM offset
                    LDA $157C,x             ; \ $02 = direction
                    STA $02                 ; / 
                    LDA $14                 ; \ 
                    LSR A                   ;  |
                    LSR A                   ;  |
                    ;LSR A                   ;  |
                    CLC                     ;  |
                    ADC $15E9|!Base2        ;  |
                    AND #$03                ;  |
                    STA $03                 ;  | $03 = index to frame start (0 or 1)
                    PHX                     ; /
                    
                    LDA !14C8,x
                    CMP #$02
                    BNE LOOP_START_2
                    STZ $03
                    LDA !15F6,x
                    ORA #$80
                    STA !15F6,x

LOOP_START_2:        LDA $00                 ; \ tile x position = sprite x location ($00)
                    STA $0300|!Base2,y             ; /

                    LDA $01                 ; \ tile y position = sprite y location ($01)
                    CLC
                    ADC #$08
                    STA $0301|!Base2,y             ; /

                    LDA !15F6,x             ; tile properties xyppccct, format
                    LDX $02                 ; \ if direction == 0...
                    BNE NO_FLIP             ;  |
                    ORA #$40                ; /    ...flip tile
NO_FLIP:             ORA $64                 ; add in tile priority of level
                    STA $0303|!Base2,y             ; store tile properties

                    LDX $03                 ; \ store tile
                    LDA TILEMAP,x           ;  |
                    STA $0302|!Base2,y             ; /

                    INY                     ; \ increase index to sprite tile map ($300)...

                    PLX                     ; pull, X = sprite index
                    LDY #$00                ; This means the tile drawn is 8x8.
LOLOLOLOL:
                    LDA #$00                ;  | A = (number of tiles drawn - 1)
                    JSL $01B7B3|!BankB   ; / don't 
                    RTS                     ; return

Graphics2:
	%GetDrawInfo()      ; Before actually coding the graphics, we need a routine that will get the current sprite's value 
		 	 	; into the OAM.
		  		; The OAM being a place where the tile data for the current sprite will be stored.
   
	LDA !B6,x
	STA $09

                    LDA $14
                    LSR A
                    LSR A
                    LSR A
					LSR A
					LSR A
                    AND #$01
                    ASL A
                    STA $03
	LDA $157C,x
	STA $02			; Store direction to $02 for use with property routine later.

	PHX			;\ Push the sprite index, since we're using it for a loop.	
	LDX #$00		;/ X = number of times to loop through. Since we only draw one MORE tile, loop one more time.
Loop2:
	LDA $00			;\
	STA $0300|!Base2,y		;/ Draw the X position of the sprite

	LDA $01			;\	; | Otherwise, both tiles would have been drawn to the same position!
	STA $0301|!Base2,y		; | If X is 00, i.e. first tile, then load the first value from the table and apply that
				;/ as the displacement. For the second tile, F0 is added to make it higher than the first.

	LDA $09
	BEQ FallGraph
	LDA #$60
	BRA FallPut
FallGraph:
        LDA #$40
FallPut:
        STA $0302|!Base2,y 



	PHX			
	LDX $02			
	LDA PROPERTIES2,x	
	ORA $64
	STA $0303|!Base2,y		
	PLX			

	INY			;\
	INY			; | The OAM is 8x8, but our sprite is 16x16 ..
	INY			; | So increment it 4 times.
	INY			;/
	
	DEX			; After drawing this tile, decrease number of tiles to go through loop. If the second tile
				; is drawn, then loop again to draw the first tile.

	BPL Loop2		; Loop until X becomes negative (FF).
	
	PLX			; Pull back the sprite index! We pushed it at the beginning of the routine.

	LDY #$02		; Y ends with the tile size .. 02 means it's 16x16
	jmp LOLOLOLOL