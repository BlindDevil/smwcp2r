;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Shyguy, by mikeyk (optimized by Blind Devil)
;;
;; Description: guess what happened here? bd has changed the sprite to another one and was
;; also lazy enough to just add a feature to it... yep it's less broken and way quicker than
;; recoding
;;
;; Note: When rideable, clipping tables values should be: 03 0A FE 0E
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Uses first extra bit: YES
;; clear: regular Shyguy (16x16)
;; set: giant Shyguy (32x32)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extra Property Byte 1
;;    bit 0 - move fast
;;    bit 1 - stay on ledges*
;;    bit 2 - follow mario*
;;    bit 3 - jump over shells
;;    bit 4 - enable spin killing (if rideable)
;;    bit 5 - can be carried (if rideable)
;;
;; * extension options set this automatically depending on their value
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extra Property Byte 2
;;    bit 0 - use turning image
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Extra Byte (Extension) 1
;; 00 - red Shyguy, falls from ledges
;; 01 - blue Shyguy, stays on ledges
;; 02-FF = uses palette/extra property byte configs from CFG - don't override configs
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

!JUMPHeight = $B8

;Tilemap tables:
Tilemap:
db $A8,$AA,$AA		;frame 1, frame 2, turning (if property is set)

;Sprite speeds
SpeedX:
db $08,$F8	;right, left (slow)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite init JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        print "INIT ",pc
.RedShy
	LDA !7FAB28,x
	AND #$F9
	STA !7FAB28,x

	LDA !167A,x
	STA !1528,x

	LDA #$01		;i'll keep all unnecessary spacing for the lulz
	STA !151C,x
		
        %SubHorzPos()		; Face Mario
        TYA
        STA !157C,x
        RTL                 ;first

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

        print "MAIN ",pc                                    ;biggest one so far
        PHB                  ;and
        PHK                  ;also
        PLB                  ;these
        JSR SpriteMainSub
        PLB                  ;ones
        RTL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite main code 
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Return:
	RTS

SpriteMainSub:
	JSR SubGfx
	LDA !1588,x             ; if on the ground, reset the turn counter
	
        LDA $9D                 ; \ if sprites locked, return
        BNE Return              ; /
	LDA !14C8,x
	CMP #$08
	BNE Return

	LDA #$03
        %SubOffScreen()		; handle off screen situation

	LDA !1588,x		;if hitting the ceiling
	AND #$08
	BEQ +

	STZ !AA,x		;reset Y speed

+
	INC !1570,x

	LDY !157C,x             ; Set x speed based on direction
        LDA SpeedX,y           ;random space
        STA !B6,x

	JSL $01802A|!BankB     ; Update position based on speed values
	
	LDA !1588,x             ; If sprite is in contact with an object...
        AND #$03                  ;another random space
        BEQ NoObjContact	;
        JSR SetSpriteTurning    ;    ...change direction
NoObjContact:
	
	JSR MaybeStayOnLedges
	
	LDA !1588,x             ; if on the ground, reset the turn counter
        AND #$04
        BEQ NotOnGround

STZ !AA,x
STZ !151C,x		; Reset turning flag (used if sprite stays on ledges)

;;;;;;;;;;;;;;;;;
LDA $77
AND #$04
BEQ NO_TOUCH
LDA !1588,x
AND #$04
BEQ NotOnGround
LDA $16
ORA $18
AND #$80
BEQ NO_TOUCH
LDA #$20
STA $1DF9|!Base2
LDA #!JUMPHeight
STA !AA,x
NO_TOUCH:

;;;;;;;;;;;;;;;;;

	JSR MaybeFaceMario
	JSR MaybeJumpShells
NotOnGround:
	
	LDA !1528,x
	STA !167A,x
	
        JSL $018032|!BankB	; Interact with other sprites
	JSL $01A7DC|!BankB	; Check for mario/sprite contact (carry set = contact)
        BCC Return              ; return if no contact
	
        %SubVertPos()          ; \
        LDA $0E                 ;  | if mario isn't above sprite, and there's vertical contact...
        CMP #$E6                ;  |     ... sprite wins
        BMI +		        ; /
	JMP SpriteWins		;branch went out of bounds

+
        LDA $7D                 ; \ if mario speed is upward, return
        BMI Return1             ; /

        LDA !7FAB28,x 	; Check property byte to see if sprite can be spin jumped
        AND #$10
        BEQ SpinKillDisabled    ;another unneeded space lol
        LDA $140D|!Base2         ; Branch if mario is spin jumping
        BNE SpinKill
SpinKillDisabled:	;ehg=
	LDA $187A|!Base2
	BNE RideSprite
	LDA !7FAB28,x
	AND #$20
	BEQ RideSprite

	BIT $16		        ; Don't pick up sprite if not pressing button
        BVC RideSprite
	LDA #$0B		; Sprite status = Carried
	STA !14C8,x
	LDA #$FF		; Set time until recovery
	STA !1540,x
Return1:
	RTS

SpinKill:
	JSR SUB_STOMP_PTS       ; give mario points
	LDA #$F8	        ; Set Mario Y speed
	STA $7D
        JSL $01AB99|!BankB   ; display contact graphic
        LDA #$04                ; \ status = 4 (being killed by spin jump)
        STA !14C8,x             ; /   
        LDA #$1F                ; \ set spin jump animation timer
        STA !1540,x             ; /
        JSL $07FC3B|!BankB
        LDA #$08                ; \ play sound effect
        STA $1DF9|!Base2        ; /
        RTS                     ; return

RideSprite:	
	LDA !7FAB10,x
	AND #$04
	BEQ +

	LDA #$D6
	STA !1534,x
	LDA #$C6
	STA !1FD6,x
	BRA DoneIndexing

+
	LDA #$E1
	STA !1534,x
	LDA #$D1
	STA !1FD6,x

DoneIndexing:
	LDA #$01                ; \ set "on sprite" flag
        STA $1471|!Base2        ; /
        LDA #$06                ; Disable interactions for a few frames
        STA !154C,x             
        STZ $7D                 ; Y speed = 0
        LDA !1534,x             ; \
        LDY $187A|!Base2        ;  | mario's y position += E1 or D1 depending if on yoshi
        BEQ NO_YOSHI            ;  |
        LDA !1FD6,x             ;  |
NO_YOSHI:
	CLC                     ;  |
        ADC !D8,x               ;  |
        STA $96                 ;  |
        LDA !14D4,x             ;  |
        ADC #$FF                ;  |
        STA $97                 ; /
        LDY #$00                ; \ 
        LDA $1491|!Base2        ;  | $1491 == 01 or FF, depending on direction
        BPL LABEL9              ;  | set mario's new x position
        DEY                     ;  |
LABEL9:
	CLC                     ;  |
        ADC $94                 ;  |
        STA $94                 ;  |
        TYA                     ;  |
        ADC $95                 ;  |
        STA $95                 ; /
        RTS                     

SpriteWins:
	LDA !154C,x             ; \ if disable interaction set...
        ORA !15D0,x             ;  |   ...or sprite being eaten...
        BNE Return1             ; /   ...return
        LDA $1490|!Base2         ; Branch if Mario has a star
        BNE MarioHasStar        
        JSL $00F5B7|!BankB	;my god this is endless
	RTS                    

MarioHasStar:
	%Star()
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

MaybeStayOnLedges:	
	LDA !7FAB28,x	; Stay on ledges if bit 1 is set
	AND #$02                
	BEQ NoFlipDirection
	LDA !1588,x             ; If the sprite is in the air
	ORA !151C,x             ;   and not already turning
	BNE NoFlipDirection
	JSR SetSpriteTurning 	;   flip direction
        LDA #$01                ;   set turning flag
	STA !151C,x    
NoFlipDirection:
	RTS
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
MaybeFaceMario:
	LDA !7FAB28,x	; Face Mario if bit 2 is set
	AND #$04
	BEQ Return4	
	LDA !1570,x
	AND #$7F
	BNE Return4
	LDA !157C,x
	PHA
	
	%SubHorzPos()         	; Face Mario
        TYA                       
	STA !157C,X
	
	PLA
	CMP !157C,x
	BEQ Return4
	LDA #$08
	STA !15AC,x
Return4:	
	RTS

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	
MaybeJumpShells:
	LDA !7FAB28,x		; Face Mario if bit 3 is set
	AND #$08
	BEQ Return4
	TXA                     ; \ Process every 4 frames 
        EOR $14                 ;  | 
        AND #$03		;  | 
        BNE Return0188AB        ; / 
        LDY #!SprSize-3		; \ Loop over sprites: 
JumpLoopStart:
	LDA !14C8,Y             ;  | 
        CMP #$0A       		;  | If sprite status = kicked, try to jump it 
        BEQ HandleJumpOver	;  | 
JumpLoopNext:
	DEY                     ;  | 
        BPL JumpLoopStart       ; / 
Return0188AB:
	RTS                     ; Return 

HandleJumpOver:
	LDA !E4,y             ;man
        SEC                       ;why
        SBC #$1A                ;are
        STA $00                   ;there
        LDA !14E0,y             ;so
        SBC #$00                ;many
        STA $08                   ;fucking
        LDA #$44                ;plain
        STA $02                   ;useless
        LDA !D8,y             ;spaces
        STA $01                   ;for
        LDA !14D4,y             ;absolutely
        STA $09                   ;no
        LDA #$10                ;reason
        STA $03                   ;at
        JSL $03B69F|!BankB  ;all?
        JSL $03B72B|!BankB     ;well they have a reason now, as I wrote a fuckton of crap in them lol
        BCC JumpLoopNext        ; If not close to shell, go back to main loop
	LDA !1588,x 		; \ If sprite not on ground, go back to main loop 
	AND #$04		;  |
        BEQ JumpLoopNext        ; / 
        LDA !157C,Y             ; \ If sprite not facing shell, don't jump 
        CMP !157C,x             ;  | 
        BEQ Return0188EB        ; / 
        LDA #$C0                ; \ Finally set jump speed 
        STA !AA,x               ; / 
Return0188EB:
	RTS                     ; Return
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SetSpriteTurning:
	LDA #$08                ; Set turning timer 
	STA !15AC,x   ;oh there's more
        LDA !157C,x
        EOR #$01
        STA !157C,x
Return0190B1:	;and here too
        RTS
		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite graphics routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

SubGfx:
	%GetDrawInfo()

		    STZ $05		    ;reset scratch RAM. it'll hold Y-flip value if stunned.
		    STZ $06		    ;reset scratch RAM. it'll hold number of tiles drawn.

	LDA !157C,x             ; $02 = direction
        STA $02

        LDA !14C8,x
	STA $04

	CMP #$09
	BCS Stunned
        CMP #$02
	BNE NotKilled

	STZ $03			;set killed frame
        LDA #$80
        STA $05
	BRA DrawSprite

Stunned:
        LDA #$80
        STA $05

NotKilled:
	LDA !7FAB34,x
	AND #$01
	BEQ NotTurning	;oh my fkin gosh
	LDA !15AC,x	; If turning...
	BEQ NotTurning
	LDA #$02		;    ...set turning frame
	STA $03
	BRA DrawSprite

NotTurning:
        LDA $14                 ;Set walking frame based on frame counter
        LSR #2
	PHA

	LDA !1540,x
	BEQ slow
	CMP #$40
	BCC +
slow:
	PLA
	LSR
	BRA ++
+
	PLA
++
        CLC                     ;come on
        ADC $15E9|!Base2        ;I wanna rest from this
        AND #$01                ;I'd remove
        STA $03                 ;but I want you to see what us remoderators need to face

DrawSprite:
		    PHX			    ;preserve sprite index
		    REP #$20
	            LDA $00                 ; \ tile x position = sprite x location ($00)
                    STA $0300|!Base2,y      ; /
		    SEP #$20

                    LDA !15F6,x             ; tile properties yxppccct, format
                    LDX $02                 ; \ if direction == 0...
                    BNE NO_FLIP             ;  |
                    ORA #$40                ; /    ...flip tile
NO_FLIP:	    ORA $05		    ; add Y flip if stunned
	            ORA $64                 ; add in tile priority of level
                    STA $0303|!Base2,y      ; store tile properties

                    LDX $03                 ; \ store tile
                    LDA Tilemap,x           ;  |
                    STA $0302|!Base2,y      ; /

		    INC $06		    ;increment RAM, a tile was drawn

FinishOAM:
                    PLX                     ; pull, X = sprite index
                    LDY #$02                ; \ $0460 = 2 (all 16x16 tiles)
                    LDA $06                 ;  | A = (number of tiles drawn - 1)
                    JSL $01B7B3|!BankB      ; / don't draw if offscreen
                    RTS                     ; return

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; points routine
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

STAR_SOUNDS:
	db $00,$13,$14,$15,$16,$17,$18,$19

SUB_STOMP_PTS:
	PHY                      
        LDA $1697|!Base2        ; \
        CLC                     ;  | ;even after comments
        ADC !1626,x             ; / some enemies give higher pts/1ups quicker??
        INC $1697|!Base2        ; increase consecutive enemies stomped
        TAY                     ;
        INY                     ;
        CPY #$08                ; \ if consecutive enemies stomped >= 8 ...
        BCS NO_SOUND            ; /    ... don't play sound 
        LDA STAR_SOUNDS,y       ; \ play sound effect
        STA $1DF9|!Base2        ; /   ;EEEEEHHHH
NO_SOUND:
	TYA                     ; \
        CMP #$08                ;  | if consecutive enemies stomped >= 8, reset to 8
        BCC NO_RESET            ;  |
        LDA #$08                ; /
NO_RESET:
	JSL $02ACE5|!BankB
        PLY                     ;the last one
        RTS                     ; return