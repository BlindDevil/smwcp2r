                    !EXTRA_BITS = $7FAB10
                    !NEW_SPRITE_NUM = $7FAB9E
		    !UPPER_DOOR_TILE = $001F            
                    !LOWER_DOOR_TILE = $0020

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; sprite code JSL
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

                    PRINT "MAIN ",pc  
                    LDA $1493
                    BNE RETURN
                    
                    STX $00

                    LDY #$0B    
LOOP_START:         CPY $00
                    BEQ DONT_CHECK
                    LDA $190F,y
                    AND #$40
                    BNE DONT_CHECK                  
                    LDA $14C8,y
                    BNE RETURN
DONT_CHECK:         DEY
                    BPL LOOP_START

HOLE:                JSR UP_GEN

                    PRINT "INIT ",pc
RETURN:              RTL

UP_GEN:
                    STZ $14C8,x             ; destroy the sprite

ldy #$01

-
phy
LDA #$08
STA $01
STZ $00
LDA #$1B
STA $02
LDA #$01
%SpawnSmoke()
ply
dey
bpl -
                    
                    LDA $E4,x               ; \ setup block properties
                    STA $9A                 ;  |
                    LDA $14E0,x             ;  |
                    STA $9B                 ;  |
                    LDA $D8,x               ;  |
                    STA $98                 ;  |
                    LDA $14D4,x             ;  |
                    STA $99                 ; /

REP #$20
LDA #!UPPER_DOOR_TILE
%ChangeMap16()

REP #$20
LDA $98
CLC
ADC #$0010
STA $98
LDA #!LOWER_DOOR_TILE

spawnblk:
%ChangeMap16()
SEP #$20
RETURN67:            RTS