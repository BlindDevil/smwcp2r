;==========
;New Sprite
;blindedit: added a health (gfx) displayer code here since doing it through uberasm messes up the new status bar tiles during game paused
;==========
!Flag = $0F5E|!Base2

!ShroomTile = $24
!ShroomProp = $38

print "MAIN ", pc
	PHB
	PHK
	PLB
	JSR Run
	PLB
print "INIT ", pc
	RTL

Return:
	RTS

Run:
	LDA !1528,x
	BEQ +

	JMP HealthDisplayer

+
	JSR GFX

	LDA #$00
	%SubOffScreen()

	LDA !14C8,x		;If the sprite is dead..
	CMP #$08
	BNE Return		;..return
	LDA $9D
	BNE Return		;Also return if sprites are locked.
	
	JSL $01A7DC|!BankB
	BCC Return
	
	LDA !C2,x
	PHX
	ASL A
	TAX
	JMP (Ptr,x)
	
Ptr:
	dw Hide		;00
	dw Wait		;01
	dw Spawn	;02

Spawn:
	PLX
	LDA #$10
	STA $1DF9|!Base2
	JSR DrawSmoke
	JSR SpawnYoshi
	LDA #$01
	STA !Flag

STZ $00 : STZ $01
STZ $02 : STZ $03
LDA !7FAB9E,x
SEC
%SpawnSprite()

	PHX
	TYX
	LDA #$86
	STA !167A,x
	LDA #$F0
	STA !166E,x
	LDA #$B9
	STA !1686,x
	LDA #$44
	STA !190F,x
	LDA #$01
	STA !1528,x
	PLX

	STZ !14C8,x
	RTS
Wait:
	PLX
	LDA !1540,x
	BEQ NextState	
	LDA #$FF
	STA $78
	STZ $7D
	STZ $7B
	LDA !E4,x
	STA $94
	LDA !14E0,x
	STA $95
	LDA !D8,x
	STA $96
	LDA !14D4,x
	STA $97
	LDA $13
	AND #$03
	TAY
	BNE +
	LDA #$22
	STA $1DF9|!Base2
	LDA Movement,y
	STA !B6,x
	STZ !AA,x
	JSL $01802A|!BankB
+
	RTS

NextState:
	INC !C2,x
	RTS
Hide:
	PLX
	LDA #$FF
	STA $78
	STZ $7D
	STZ $7B
	LDA #$12
	STA !1540,x	; time to wait
	INC !C2,x
	RTS

Movement: 
	db $02,$FE,$03,$FC

SpawnYoshi:
	LDA $186C,x
	BNE EndSpawn
	JSL $02A9DE|!BankB
	BMI EndSpawn
	LDA #$01
	STA !14C8,y
	LDA #$35
	STA !9E,y
	LDA $94
	STA !E4,y
	LDA $95
	STA !14E0,y
	LDA $96
	STA !D8,y
	LDA $97
	STA !14D4,y
	PHX
	TYX
	JSL $07F7D2|!BankB
	PLX
EndSpawn:
	RTS
	
DrawSmoke:
STZ $00 : STZ $01
LDA #$1B : STA $02
LDA #$01 : %SpawnSmoke()
RTS

;Sprite Routines
  
TILEMAP:	
	db $80,$82
	db $A0,$A2
	
XDISP:	
	db $00,$10
	db $00,$10
	
YDISP:	
	db $00,$00
	db $10,$10
	
GFX:
	%GetDrawInfo()
	LDA !157C,x
	STA $02
	
	LDA !15F6,x
	STA $04
	
	LDX #$00
	
OAM_Loop:
	LDA $00
	CLC
	ADC XDISP,x
	STA $0300|!Base2,y
	
	LDA $01
	CLC
	ADC YDISP,x
	STA $0301|!Base2,y
	
	LDA TILEMAP,x
	STA $0302|!Base2,y
	
	LDA $04
	ORA $64
	STA $0303|!Base2,y
	
	INY
	INY
	INY
	INY
	
	INX
	CPX #$04
	BNE OAM_Loop
	
	LDX $15E9|!Base2
	LDA #$03

FinishOAM:
	LDY #$02
	JSL $01B7B3|!BankB
	RTS

HealthDisplayer:
	LDA $1A
	CLC
	ADC #$80
	STA !E4,x
	LDA $1B
	ADC #$00
	STA !14E0,x
	LDA $1C
	CLC
	ADC #$C8
	STA !D8,x
	LDA $1D
	ADC #$00
	STA !14D4,x		;^ make the sprite stay on screen all the time

LDA $19			;load player's powerup
BEQ .NoGFX		;if equal zero, don't display any graphics.
LDA $71			;load player animation pointer
CMP #$09		;check if player is dying
BEQ .NoGFX		;if yes, don't display any graphics.

JSR Shrooms		;draw mushrooms as HP indicators

.NoGFX
RTS

SXDisp:
db $08,$10

Shrooms:
%GetDrawInfo()

PHX			;preserve sprite index

LDX #$00		;loop count starts at zero
STZ $02			;tiles drawn counter starts at zero

LDA $19			;load player's powerup
CMP #$01		;compare to value
BEQ .GFXLoop		;if equal, go to drawing routine.

INX			;increment X by one

.GFXLoop
LDA SXDisp,x		;load X displacement from table according to index
STA $0300|!Base2,y	;store to OAM.
LDA #$C8		;load Y displacement from table according to index
STA $0301|!Base2,y	;store to OAM.
LDA #!ShroomTile	;load tile number
STA $0302|!Base2,y	;store to OAM.
LDA #!ShroomProp	;load palette/properties
STA $0303|!Base2,y	;store to OAM.

INY #4			;increment OAM index four times
INC $02			;increment tiles drawn by one
DEX			;decrement X by one
BPL .GFXLoop		;loop while it's positive.

PLX			;restore sprite index
LDA $02			;load number of tiles drawn from scratch RAM
DEC			;minus one
BRA FinishOAM		;finish OAM write