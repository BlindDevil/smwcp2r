;------------;
; laser ;
;------------;
; Initial XY positions set by generating sprite.

OAMStuff:
db $40,$44,$48,$4C,$D0,$D4,$D8,$DC,$80,$84,$88,$8C,$B0,$B4,$B8,$BC,$C0,$C4,$C8,$CC

Pals:
db $04,$06,$08,$0A

Return:
RTS

Main:
PHB
LDA #$00
PHA
PLB
JSR LessMain
PLB
RTL

LessMain:
JSR Graphics
LDA $9D
BNE Return

;;;;;;;;;;;;;;;;;;;;;;;;;
;INTERACT WITH MARIO
;;;;;;;;;;;;;;;;;;;;;;;;;

PHY ;just to make sure
PHX
TYX
JSL $03B664				;Get mario clipping
JSR GetClusterSpriteClipping		;Get cluster clipping
JSL $03B72B				;Check for contact
PLX
PLY
BCC +					;If not interacting, branch
JSL $00F5B7				;Otherwise hurt mario
+

;;;;;;;;;;;;;;;;;;;;;;;;;
;BEGIN CODE TO EVAPORATE SPRITE ON CONTACT
;If you want to disable killing other sprites,
;Just remove this code block, it should work.
;;;;;;;;;;;;;;;;;;;;;;;;;

PHX
TYX
JSR GetClusterSpriteClipping

LDX #$0B
-
LDA $14C8,x
BEQ +
PHY
PHX
JSL $03B6E5				;Get sprite clipping B
JSL $03B72B
PLX
PLY
BCC +
PHX
STZ $14C8,x
LDA #$47
STA $1DFC
JSR SUB_SMOKE2
PLX
+
DEX
BPL -

.skip
PLX

;;;;;;;;;;;;;;;;;;;;;;;;;
;END CODE TO EVAPORATE SPRITE ON CONTACT
;;;;;;;;;;;;;;;;;;;;;;;;;

;Y
LDA $1E02,y
CLC
ADC #$08
STA $1E02,y
LDA $1E2A,y
ADC #$00
STA $1E2A,y

;X
LDX $1E52,y
BNE +

LDA $1E16,y
SEC
SBC #$08
STA $1E16,y
LDA $1E3E,y
SBC #$00
STA $1E3E,y
JMP .cont

+
LDA $1E16,y
CLC
ADC #$08
STA $1E16,y
LDA $1E3E,y
ADC #$00
STA $1E3E,y

.cont
PHY
JSR ObjectInteraction
;$1693 = low byte
;$18D7 = high byte
PLY
LDA $18D7
BEQ +
LDA $1693
CMP #$6D
BCS +

LDA $1E02,y
AND #$F0
SEC
SBC #$05
STA $1E02,y
LDA $1E2A,y
ADC #$00
STA $1E2A,y

LDA $1E16,y
AND #$F0
SEC
SBC #$04
STA $1E16,y
LDA $1E3E,y
ADC #$00
STA $1E3E,y


LDA #$00
STA $1892,y
STA $1E52,y
JSR SUB_SMOKE
+
RTS



Graphics:                       ; OAM routine starts here.
LDA $1E16,y
CMP $1A
LDA $1E3E,y
SBC $1B
BNE BeforeReturn2

TYX
LDA.l OAMStuff,x
TAX
LDA $1E02,y			; \ Copy Y position relative to screen Y to OAM Y.
SEC                             ;  |
SBC $1C				;  |
STA $0201,x			; /
LDA $1E16,y			; \ Copy X position relative to screen X to OAM X.
SEC					;  |
SBC $1A				;  |
STA $0200,x			; /
LDA #$EC			; \ Tile = #$E0. was d4
STA $0202,x         ; / (Spike.)
LDA $14
AND #$0F
LSR A
LSR A
PHX
TAX
LDA.l Pals,x
PLX
PHA
LDA $1E52,y
CLC
ROR A
ROR A
ROR A

ORA $01,s 			; I can be really lazy at times
ORA $64				; \ Properties per spike, some are rising so this needs a seperate table.
STA $0203,x			; /
PLA

PHX
TXA
LSR
LSR
TAX
LDA #$02
STA $0420,x
PLX
LDA $0201,x
CMP #$F0                                        ; As soon as the spike is off-screen...
BCC Return2
BeforeReturn2:
LDA #$00					; Kill sprite.
STA $1892,y					;
STA $1E52,y
Return2:
RTS

GetClusterSpriteClipping:
LDA $1E16,y
CLC
ADC #$04
STA $04
LDA $1E3E,y
ADC #$00
STA $0A
LDA.b #$02                ; Width of sprite clipping
STA $06
LDA $1E02,y
CLC
ADC #$03
STA $05
LDA $1E2A,y
ADC #$00
STA $0B
LDA #$02                ; Height of sprite clipping
STA $07
RTS

;;Object interaction

ObjInterReturn:
LDX $0F 
LDA.b #$00 
STA.w $1693
STA.w $1694
RTS

ObjectInteraction:
LDA $1E02,y 
CLC
ADC #$01		;clipping Y
STA $0C 
AND.b #$F0 
STA $00 
LDA $1E2A,y
ADC.b #$00 
STA $0D 
REP #$20
LDA $0C 
CMP.w #$01B0
SEP #$20
BCS ObjInterReturn
LDA $1E16,y
CLC
ADC #$01		;clipping X
STA $0A 
STA $01 
LDA.w $1E3E,y
ADC.b #$00 
STA $0B 
BMI ObjInterReturn
CMP $5D
BCS ObjInterReturn
LDA $01 
LSR
LSR
LSR
LSR
ORA $00 
STA $00 
LDX $0B 
LDA.l $00BA60,X 
LDY.w $185E
BEQ +
LDA.l $00BA70,X 
+
CLC
ADC $00 
STA $05 
LDA.l $00BA9C,X 
LDY.w $185E
BEQ +
LDA.l $00BAAC,X 
+
ADC $0D 
STA $06 
LDA.b #$7E 
STA $07 
LDX.w $15E9
LDA [$05]			;read map16 low byte table ($7EC800)
STA.w $1693			;Block you're interacting with (low byte) goes into $1693
INC $07 			;Switch to map16 high byte table ($7FC800)
LDA [$05]			;Load map16 high byte
STA $18D7			;Block you're interacting with (high byte) goes into $18D7
;JSL $00F545			;
RTS 

SUB_SMOKE2:
			LDX #$03
			BRA FINDFREE
SUB_SMOKE:		LDX #$01	; \ find a free slot to display effect
FINDFREE:		LDA $17C0,x	;  |
			BEQ FOUNDONE	;  |
			DEY		;  |
			BPL FINDFREE	;  |
			RTS		; / return if no slots open

FOUNDONE:		LDA #$01	; \ set effect graphic to smoke graphic
			STA $17C0,x	; /
			LDA #$17	; \ set time to show smoke
			STA $17CC,x	; /
			LDA $1E02,y	; \ smoke y position = generator y position
			STA $17C4,x	; /
			LDA $1E16,y	; \ load generator x position and store it for later
			STA $17C8,x	; /
			RTS
