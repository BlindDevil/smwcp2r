;;;;;;;;
; SMB3 Brick disassembled...
; or at least, emulated ><
; You should not use it, anyway, since custom bounce blocks exist...
; ...moreover, I have a bit badly coded this.
; Oh, forgotten: "disassemulated" by SL.
; v1.1 : included bricktype which permits emulate SMB1 brick
; v1.2 : included bouncetype for the demo world style
;	 and commentaries for making this block a SMB3 coin
; v1.3 : included a better way to change it into a SMB3 coin
;;;;;;;;;


db $42 ; enable corner and inside offsets
JMP MarioBelow : JMP MarioAbove : JMP MarioSide : JMP SpriteV : JMP SpriteH : JMP MarioCape : JMP MarioFireBall : JMP MarioCorner : JMP HeadInside : JMP BodyInside

!Bouncetype = $01	; 00 = bounce when hit
			; 01 = do NOT bounce (Demo world style)
!Bricktype = $00	; 00 = turn into coin when pow activated (set "block acts like" to 132!)
			; 01 = do NOT turn into coin when pow activated (set "block acts like" to 130!)
!SMB3coin = $01		; 00 = brick
			; 01 = SMB3 coin (keep Bricktype to $00)


MarioBelow:
	LDA $14AD
	AND.b #!Bricktype-$01	; depends on bricktype
	BRA !SMB3coin<<2
	BNE Pow			;if POW activated, do nothing (hey, you don't want a collectable shattering coin)
	BRA $02
	BNE Pow			; (CHANGED: BEQ to BNE)
	LDA $19
	BNE MarioBig		; Shatter if Mario is big or flower or blahblah, bounce if mario is small (pretty obvious)
	LDA #!Bouncetype
	BNE Pow

	LDA #$0F
	TRB $9A
	TRB $98		; clear low nibble of X and Y position of contact
	PHY
	PHB			;preserve current bank
	LDA #$02		;push 02
	PHA
	PLB			;bank = 02
	JSR iwannabounce	; block bounce sprite routine, ripped of SMB3 Brick bin file.
	PLB			;restore bank
	PLY

MarioAbove:
MarioSide:
MarioCorner:
HeadInside:
BodyInside:
MarioFireBall:
Pow:

	RTL		; return

SpriteV:
	LDA $14AD
	AND.b #!Bricktype-$01	; depends on bricktype
	BRA !SMB3coin<<2
	BNE Pow			; same thing
	BRA $02
	BNE Pow			; (CHANGED: BEQ to BNE)
	LDA $14C8,x
	CMP #$09	; do nothing if not a carryable sprite (since when you kick a shell upward, it is this status...)
	BNE Pow
	LDA $AA,x
	BPL Pow		; only a rising sprite

	LDA $0F
	PHA		; if you don't do this, the sprite will be move slightly sideways

	LDA $0A 	; $0A-D hold the x/y of sprite contact 
	AND #$F0 	; have to clear the low nybble much like $98-9B 
	STA $9A 
	LDA $0B 
	STA $9B 
	LDA $0C 
	AND #$F0 
	STA $98 
	LDA $0D 
	STA $99
	PHB
	LDA #$02
	PHA
	PLB
	LDA #$00 	;set to 0 for normal explosion, 1 for rainbow (throw blocks)
	JSL $028663 	;breaking effect
	PLB
	LDA #$07
	STA $1DFC
	LDA #$02 	; Replace block with blank tile
	STA $9C
	JSL $00BEB0	;ripped from another block, "shatter with sprite" or something

	STZ $AA,x		; not sure it is usefull anyway
	PLA
	STA $0F		; explained above

	JSR SpriteBlockInteract		; cause koopas and cie to be kicked if you break the block
	JSR Points		; give 10 points

	RTL

MarioCape:
	LDA $14AD
	AND.b #!Bricktype-$01	; depends on bricktype
	BRA !SMB3coin<<2
	BNE Pow			; again
	BRA $02
	BNE Pow		; (CHANGED: BEQ to BNE)
MarioBig:
	LDA #$0F
	TRB $9A
	TRB $98		; clear low nibble of X and Y position of contact

	PHB
	LDA #$02
	PHA
	PLB
	LDA #$00 	;set to 0 for normal explosion, 1 for rainbow (throw blocks)
	JSL $028663 	;breaking effect
	PLB
	LDA #$07
	STA $1DFC
	LDA #$02 	; Replace block with blank tile
	STA $9C
	JSL $00BEB0

	JSR SpriteBlockInteract		; cause koopas and cie to be kicked if you break the block, again.
	JSR Points		; give 10 points

return:
	RTL

SpriteH:
	LDA $14AD
	AND.b #!Bricktype-$01	; depends on bricktype
	BRA !SMB3coin<<2
	BNE return		; bah, you know...
	BRA $02
	BNE return		; (CHANGED: BEQ to BNE)
	LDA $14C8,x
	CMP #$0A
	BNE return		; only a kicked sprite

	LDA $0A 	; $0A-D hold the x/y of sprite contact 
	AND #$F0 	; have to clear the low nybble much like $98-9B 
	STA $9A 
	LDA $0B 
	STA $9B 
	LDA $0C 
	AND #$F0 
	STA $98 
	LDA $0D 
	STA $99
	PHB
	LDA #$02
	PHA
	PLB
	LDA #$00 	;set to 0 for normal explosion, 1 for rainbow (throw blocks)
	JSL $028663 	;breaking effect
	PLB
	LDA #$07
	STA $1DFC
	LDA #$02 	; Replace block with blank tile
	STA $9C
	JSL $00BEB0

	LDY #$10	;act like tile 130 to make the shell go backward
	LDA #$30
	STA $1693
	JSR Points		; give 10 points
	JSR SpriteBlockInteract		; cause koopas and cie to be kicked if you break the block. In this, case, I think it sucks, but eh, it is an emulation.

	RTL


;;;;;;;;;;;;;;;;;;;ripped from the bin file, which was already ripped (and edited a bit) from $028818;;;;;;;
iwannabounce:
	LDX #$00
	LDY #$03	; used for indexing
LDA $8789,x
	STA $1901,y	; YXPPCCCT data of bounce sprite that is being shown
TXA
INC A			; LDA #$01 ?
	STA $1699,y	; Block Bounce Sprite Image Table
LDA #$01		;seems useless
	STA $169D,y	; Block Bounce Sprite ? Table
LDA $9A
STA $16A5,y	; Block Bounce Sprite Xpos Low Byte Table 
LDA $9B
STA $16AD,y	; Block Bounce Sprite Xpos High Byte Table
LDA $98
STA $16A1,y	; Block Bounce Sprite Ypos Low Byte Table
LDA $99
STA $16A9,y	; Block Bounce Sprite Ypos High Byte Table
LDA #$08
STA $16C5,y	; Block Bounce Sprite Bouncing Flag Table
LDA #$00
STA $16C1,y	; Block Bounce Sprite Turns Into Table

LDA $1933
LSR A
ROR A
STA $08
LDX #$00
LDA $873A,x
STA $16B1,y	; Block Bounce Sprite YSpeed Table
LDA $873E,x
STA $16B5,y	; Block Bounce Sprite XSpeed Table
TXA
ORA $08		; seems useless too
STA $16C9,y	; Block Bounce Sprite ? Table
JSR SpriteBlockInteract
RTS


SpriteBlockInteract:
	LDY #$03
ALoop1:
	LDA $16CD,y	; Block Bounce Sprite Exists Flag Table
	BEQ Missing4bytes
	DEY
	BPL ALoop1
	INY	; equal to LDY #$00
Missing4bytes:
LDA $9A
STA $16D1,y
LDA $9B
STA $16D5,y
LDA $98
STA $16D9,y
LDA $99
STA $16DD,y
LDA $1933
BEQ Missing1Ebytes
LDA $9A
SEC
SBC $26
STA $16D1,y
LDA $9B
SBC $27
STA $16D5,y
LDA $98
SEC
SBC $28
STA $16D9,y
LDA $99
SBC $29
STA $16DD,y
Missing1Ebytes:
LDA #$01
STA $16CD,y	; Block Bounce Sprite Exists Flag Table
LDA #$06
STA $18F8,y	; unknown!
RTS

Points:
LDA $0DB3
ASL A
ADC $0DB3
TAX
LDA $0F34,x
CLC
ADC #$01
STA $0F34,x
LDA $0F35,x
ADC #$00
STA $0F35,x
LDA $0F36,x
ADC #$00
STA $0F36,x
RTS