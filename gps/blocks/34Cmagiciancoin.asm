;New Magician Coin
;adds one to $0F5E, read by the magician sprite

;a loop though sprites code block has become an one-liner lol
;"feel the power of reason" - azwel

db $37
JMP Check : JMP Check : JMP Check : JMP Return : JMP Return : JMP Return : JMP Return
JMP Check : JMP Check : JMP Check
JMP Check : JMP Check

Check:
INC $0F5E|!addr

Return:
RTL

print "A coin that acts like a purple coin, but also adds one (second) to address $0F5E (stopwatch), used by The Magician (TM)."