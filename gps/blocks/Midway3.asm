;Midway Point 2 block by Blind Devil

;Here are some defines copied from Kaijyuu's MMP patch.
!OWLevelNum = $13BF
!CheckpointRAM = $7FB4D9

;Checkpoint to trigger value.
!CheckpointNumber = $02

db $37

JMP Main : JMP Main : JMP Main : JMP return : JMP return : JMP return : JMP return
JMP Main : JMP Main : JMP Main
JMP Main : JMP Main

Main:
LDX !OWLevelNum			;load OW number on X
LDA #!CheckpointNumber		;load checkpoint value
STA !CheckpointRAM,x		;store to checkpoint table, indexed.
LDA #$01			;load value
STA $13CE			;store to "midway point collected" flag.

LDA #$05			;load SFX value
STA $1DF9			;store to address to play it.

LDA $7FC060			;load CDM16 flags address 1
AND #$07			;preserve bits 0, 1 and 2
ASL #4				;shift bits to the left four times
ORA $7FB539,x			;set bits to temporary SMWC coins collected address according to translevel index
STA $7FB539,x			;store result in there.

LDA $7FC060			;load CDM16 flags address 1 again
ORA #$10			;set CDM16 flag 4
STA $7FC060			;store result back.

%glitter()
%erase_block()

LDA $19				;load powerup
BNE return			;if not zero, return.

LDA #$01			;load powerup value
STA $19				;store to current powerup.

return:
RTL				;return.

print "A third midway point. Make sure you configure Conditional Direct Map16 options for it (use flag 4, and mark 'always show objects/add 0x100 tiles')."