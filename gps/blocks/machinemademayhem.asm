db $42

JMP Mario : JMP Mario : JMP Mario : JMP Return : JMP Return : JMP Return : JMP Return : JMP Mario : JMP Mario : JMP Mario

Mario:
LDA $94
CMP #$54
BCC Return
CMP #$5D
BCS Return

STZ $00			;reset scratch RAM.

LDA.w $1F02+$E|!addr	;load events passed flags address 15
AND #$01		;check if event 77 was passed
BEQ +			;if not passed, skip ahead.

INC $00			;increment scratch RAM by one

+
LDA $1F2C|!addr		;load collectibles address
AND #$01		;check if bit 0 is set (diamond collected)
BEQ +			;if not, skip ahead.

INC $00			;increment scratch RAM by one

+
LDA $71
BNE Return		;return if running some animation

LDA $16
AND #$08
BEQ Return		;return if not pressing up

LDA $00			;load scratch RAM
BEQ Return		;if zero, return.
DEC			;decrement A by one
BEQ TNKTNKTNK		;if #$01, teleport the player to a different level.

STZ $7B			;reset player's X speed.

pleasespawnit:
LDA #$4D
SEC
%spawn_sprite()
BCS pleasespawnit
%move_spawn_to_player()

STZ $13D4|!addr
STZ $1411|!addr
STZ $1412|!addr
LDA #$0B
STA $71

Return:
RTL

TNKTNKTNK:
LDA #$2A
STA $1DFC|!addr		;when you hear this you know you have messed up

REP #$20
LDA #$01BD
%teleport()
RTL

print "The mysterious machine of Opulent Oasis that spawns a diamond sprite which triggers a cutscene event before warping the player to another sublevel when Up is pressed in front of it, if both Asteroid Antics was beaten and its diamond was collected. Does nothing otherwise... or, you're free to try some combinations for a different outcome..."