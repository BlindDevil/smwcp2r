db $42
JMP Main : JMP Main : JMP Main : JMP Sprite : JMP Sprite : JMP Return : JMP Return : JMP Main : JMP Main : JMP Main

!Sink = $09

Main:
	STZ $15		;Stop controller movements

	LDA #$25	;Play sinking sound.
	STA $1DF9	;Store to bank.
	INC $185C	;disable player interaction with layers
	RTL

Sprite:
	LDA $7FAB9E,x		
	CMP #$61		;\ If the sprite is the right one...
	BEQ Solid		;/ branch

	LDA #!Sink
	STA $AA,x
	STZ $B6,x
Return:
	RTL		;Return

Solid:
	LDY #$01
	LDA #$30
	STA $1693
	RTL