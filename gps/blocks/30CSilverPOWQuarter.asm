;when touched, sets silver POW timer to 1/4 and plays the music, shake screen etc.
;by hach

db $37
JMP Main : JMP Return : JMP Return : JMP SpriteV : JMP SpriteH : JMP Return : JMP Return : JMP Return : JMP Return : JMP Return
JMP Return : JMP Return

Main:
	LDA #$10		;shakes the ground
	STA $1887|!addr
	LDA #$0B		;plays sound
	STA $1DF9|!addr
	LDA #$3F   		;sets timer
	STA $14AE|!addr
	LDA #$0E   		;plays POW music
	STA $1DFB|!addr
	JSL $02B9BD|!bank	;turns sprites into coins
	RTL

SpriteV:
	LDA !AA,x
	BPL Return
SpriteH:
	LDA !14C8,x
	CMP #$09
	BEQ Main
	CMP #$0A
	BEQ Main
Return:
	RTL

print "When touched, sets silver POW timer to 1/4 and plays the music, shake screen etc."