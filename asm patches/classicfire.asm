;Ignore the comments, they're left over from the old version and have been moved around a bit.

org $02A129
LDA #$02    ; / Make sprite fall down...
STA $14C8,x ; \ ... or disappear in smoke (depends on its settings)
autoclean JSL Mycode
JSL $02ACEF ; Jump to the score routine handler.


freecode

Mycode:
LDY $185E
LDA $1747,y
BMI +
LDA #$20
BRA ++
+
LDA #$E0
++
STA $B6,x

	;BRA Inc			;DEBUG CODE, REMOVE ME

	LDA $1F27		;Load items collected flag.
	AND #$10		;Check if Shield Ring was collected.
	BNE Inc			;If yes, directly allow increment.

	LDA $7FA04D		;Load midpoint flag from current translevel (D). EDIT: No LDX as it screws some default routines.
	CMP #$02		;Check if midpoint 2 is triggered.
	BNE Return		;If not, return.

	LDA $13BF		;Load translevel
	CMP #$0D		;Check if it's #$0D
	BNE Return		;If not, return.

Inc:
LDA $1415	;load address
CMP #$F7	;compare
BCS ForceF0	;if higher, force value.

LDA $1415		;load freeRAM address
CLC : ADC #$08		;add #$08
STA $1415		;store back
BRA Return		;branch to Return

ForceF0:
LDA #$F0	;load value
STA $1415	;store to FreeRAM

Return:

LDA #$05    ; 5 = 100. Changing it will change the amount of score you get (1=10,2=20,3=40,4=80,5=100,6=200,7=400,8=800,9=1000,A=2000,B=4000,C=8000,D=1up)
RTL