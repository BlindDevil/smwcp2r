;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;This patch fixes a bug in smw that sprites that die by falling off screen ($14C8,x set to #$02)
;STILL interacts with layer 1/2. Although I could've add a check before JSL $019138 on EVERY sprite,
;as well as checking $14C8,x on every custom block. I added it IN the routine so I don't have a flood of
;ORGs everywhere and having to check $14C8,x on every custom block that deals with the sprite.

;The interaction can be proven that koopa shells that fall off the screen suddenly veers left or right
;inside the solid blocks and on custom blocks that does anything to the sprite (such as setting $AA,x to
;#$80 to make it bounce up) still works.

if read1($00FFD5) == $23
    sa1rom

    !addr = $6000
    !bank = $000000
    !14C8 = $3242
    !1588 = $334A
else
    lorom

    !addr = $0000
    !bank = $800000
    !14C8 = $14C8
    !1588 = $1588
endif

org $019140|!bank
    autoclean JML NoBlockInteractOnDeath    ;> JML because I'm making it skip code if dying
    BRA $00

freedata

NoBlockInteractOnDeath:
    LDA !14C8,x
    CMP #$02
    BEQ .noInteract
    STZ $1694|!addr         ;\ Restore overwritten code
    STZ !1588,x             ;/
    JML $019146|!bank       ;> Run the rest of the block interaction code

.noInteract
    JML $01920B|!bank       ;> Cannot RTS at a different bank.
