HEADER
LOROM

!X  		= $0EF9		;20
!Y  		= $0F0D		;20
!properties 	= $0F21		;20
!tiles		= $0F35 	;20
!timers 	= $0F49		;6
!data		= $0F4F		;15

!t0 = $04
!t1 = $06
!t2 = $30
!t3 = $48

!smwcp_coin 	= !data

!goal_flag	= $1B97

incsrc CoinDefines.asm
!coin_counter = !TempCounter+1

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Start macros
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

macro timer(time, offset)
	LDA #<time>
	STA.w !timers+<offset>
endmacro

macro position8(x, offset)
	LDA #<x>
	STA.w !X+<offset>
	LDA #$F8
	STA.w !Y+<offset>
endmacro

macro position16(x, offset)
	LDA #<x>
	STA.w !X+<offset>
	LDA #$F0
	STA.w !Y+<offset>
endmacro

macro animate16(offset)
	LDA.w !Y+<offset>
	CMP #$08
	BEQ ?fuck
		INC.w !Y+<offset>
	?fuck:
endmacro

macro animate8(offset)
	LDA.w !Y+<offset>
	CMP #$10
	BEQ ?fuck
		INC.w !Y+<offset>
	?fuck:
endmacro

macro X_OAM_index()
	JSR get_slot
	PHX
	REP #$20
	TXA
	LSR #2
	TAX
	SEP #$20
endmacro

macro OAM8(offset)
	LDY.w #<offset>
	JSR OAM_8
endmacro

macro OAM16(offset)
	LDY.w #<offset>
	JSR OAM_16
endmacro

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Start hijack writes
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;ORG $00A59C
;	JMP init

ORG $00A2EA
	JMP main
	
;ORG $00A1DA
;	JSR run_pause
	
ORG $008C89

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Start tables
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	reset bytes
	numbers:
		db $76,$77,$46,$47,$C2,$C3,$D2
		db $D3,$D4,$D5
	coin_tiles:
		db $44,$80
	pause_clear_tiles:
		db $24, $E8

		
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Start init code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	init:
		LDX #$64
		-
			STZ.w !X,x
			DEX
		BNE -
		JSR $85FA
		JMP $A59F
	
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Start main code
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	main:
		PLA
		STA $1D
		PHP
		
		REP #$10
		TDC
		LDX !goal_flag
		BNE run_goal
		
		STZ !data+2
		LDA.w !timers
		ORA.w $18E4
		BEQ no_lives
			JSR life_handle
		no_lives:
		LDA.w !timers+$04
		BNE +
			LDA !coin_counter
			CMP !smwcp_coin
			BEQ no_smwcp_coin
			STA !smwcp_coin
		+
			JSR smwcp_coin_handle
		no_smwcp_coin:
		LDA.w !timers+$05
		ORA.w $13CC
		BEQ no_coins
			JSR coin_handle
		no_coins:
		PLP
	JMP $A2ED
	
	run_goal:
		TAX
		STX !data+2
		LDA !data+6
		BNE goal_main
		INC !data+6
		TXA
		JSR life_handle_paused
		TDC
		JSR smwcp_coin_handle_paused
		TDC
		JSR coin_handle_paused
		BRA no_coins
		
	goal_main:
		JSR life_handle_paused
		LDA #$69
		JSR smwcp_coin_handle_paused
		LDA #$69
		JSR coin_handle_paused

		LDA !timers
		BPL +
			LDA #$01
			STA !timers
	+
		LDA !timers+4
		BPL +
			LDA #$01
			STA !timers+4
	+
		LDA !timers+5
		BPL +
			LDA #$01
			STA !timers+5
	+
		BRA no_coins
	
	run_pause:
		PHP
		LDA $13D4
		BEQ +
			REP #$30
			TDC ;clear A high byte
			STZ !data+2
			SEP #$20
			JSR clear_pause_OAM
			LDA !data+5
			BNE pause_main
			INC !data+5
			TDC
			JSR life_handle_paused
			TDC
			JSR smwcp_coin_handle_paused
			TDC
			JSR coin_handle_paused
			BRA pause_return
		+
		STZ !data+5
	pause_return:
		PLP
		LDA.w $1426
	RTS
	
	pause_main:
		JSR life_handle
		JSR smwcp_coin_handle
		JSR coin_handle
		INC !timers
		INC !timers+4
		INC !timers+5	
		SEP #$10
		PHK
		PEA.w .jslrtsreturn-1
		PEA.w $0084CF-1
		JML $808494
		.jslrtsreturn
		BRA pause_return
		
	life_handle:
		LDA.w !timers
	life_handle_paused:
		BNE +
			%timer(!t0, $00)
			%position16($10, $00)
			JSR init_lives
		+
			LDA #$24
			STA.w !tiles
			LDA #$3A
			STA.w !properties
			%animate16($00)
			%OAM16($00)
			JSR update_lives
			LDA.w !Y
			CMP #$08
			BNE +
				DEC !timers
				BNE +
					LDA $18E4
				BEQ +
					DEC $18E4
					LDA #$05			
					STA $1DFC
					LDA $0DBE
					CMP #$63
					BEQ .max
						INC $0DBE
					.max
					; %timer($10, $00)
					
					LDY $18E4			; show longer if done
					BNE ++
						LDA #!t2
						BRA +++
					++
						LDA #!t1
					+++
						STA.w !timers
					
					
		+
	RTS
	
	init_lives:
		%position8($28, $01)
		%position8($30, $02)
	RTS
	update_lives:
		LDA $0DBE
		JSR hex_dec
		PHA
		TXA
		BEQ +
		
			LDA.w numbers,x
			STA.w !tiles+$01
			LDA #$30
			STA.w !properties+$01
			
			%animate8($01)
			%OAM8($01)
	+
		PLA
		TAX
		LDA.w numbers,x
		STA.w !tiles+$02
		LDA #$30
		STA.w !properties+$02
		%animate8($02)
		%OAM8($02)
	RTS
	
	smwcp_coin_handle:
		LDA.w !timers+$04
	smwcp_coin_handle_paused:
		BNE +
			%timer(!t3, $04)
			JSR init_smwcp_coins
		+
		DEC !timers+$04
		JSR update_smwcp_coins
	RTS
	
	init_smwcp_coins:
		%position16($60, $03)
		%position16($78, $04)
		%position16($90, $05)
	RTS
	
	update_smwcp_coins:
		TDC
		; LDA #$00
		; XBA
		
		LDA !coin_counter
		AND #$01
		TAX
		LDA.w coin_tiles,x
		STA.w !tiles+$03
		LDA #$34
		STA.w !properties+$03
		%animate16($03)
		%OAM16($03)
		
		LDA !coin_counter
		LSR
		AND #$01
		TAX
		LDA.w coin_tiles,x
		STA.w !tiles+$04
		LDA #$34
		STA.w !properties+$04
		%animate16($04)
		%OAM16($04)
		
		LDA !coin_counter
		LSR #2
		AND #$01
		TAX
		LDA.w coin_tiles,x
		STA.w !tiles+$05	
		LDA #$34
		STA.w !properties+$05
		%animate16($05)
		%OAM16($05)
	RTS
	
	coin_handle:
		LDA.w !timers+$05
	coin_handle_paused:
		BNE +
			%timer(!t0, $05)
			%position16($E8, $0C)
			JSR init_coins
		+
			LDA #$E8
			STA.w !tiles+$0C
			LDA #$34
			STA !properties+$0C
			%animate16($0C)
			%OAM16($0C)
			JSR update_coins
			LDA.w !Y+$0C
			CMP #$08
			BNE +
				DEC !timers+$05
				BNE +
					LDA $13CC
				BEQ +
					DEC $13CC
					LDA $0DBF
					CMP #$FF
					BEQ .max
						INC $0DBF
					.max
					; %timer($10, $05)
					
					LDA $13CC
					BNE ++
						LDA #!t2
						BRA +++
					++
						LDA #!t1
					+++
						STA.w !timers+$05
		+
	RTS
	
	init_coins:
		%position8($D8, $0E)
		%position8($E0, $0F)
	RTS
	update_coins:
		LDA $0DBF
		JSR hex_dec
		CPY #$0001
		BNE +
			INC $18E4
			TDC				; changed from ldx #$0000 and lda #$00
			TAX
			STZ $0DBF
			%position8($D8, $0E)
		+
		PHA
		LDA $0DBF
		CMP #$0A
		BCC +
			TXA
			LDA.w numbers,x
			STA.w !tiles+$0E
			LDA #$30
			STA.w !properties+$0E
			%animate8($0E)
			%OAM8($0E)
		+
		PLA
		TAX
		LDA.w numbers,x
		STA.w !tiles+$0F
		LDA #$30
		STA.w !properties+$0F
		%animate8($0F)
		%OAM8($0F)
	RTS
	
	
	get_slot:
		LDX !data+2
		slot_loop:
			LDA $0201,X
			CMP #$F0
			BEQ +
				INX #4
				CPX #$0200
				BCC slot_loop	; not sure why this was bcs
		+
		STX !data+2
	RTS
	
	OAM_16:
		%X_OAM_index()
		LDA #$02
		STA $0420,x
		BRA OAM_common
	OAM_8:
		%X_OAM_index()
		STZ $0420,x
	OAM_common:
		PLX
		LDA.w !X,y
		STA $0200,x
		LDA.w !Y,y
		STA $0201,x
		LDA.w !tiles,y
		STA $0202,x
		LDA.w !properties,y
		STA $0203,x
	RTS
	
	clear_pause_OAM:
		LDX #$01FE
		-
		LDY #$000D
		.check_next
			LDA $0201,x
			AND #$01
			BNE ++
				
				LDA numbers,y
				CMP $0200,x
				BEQ .next_slot
			+
			DEY
		BPL .check_next
		BRA ++
		.next_slot
			LDA #$F0
			STA $01FF,x
		++
		DEX #4
		BPL -
	RTS
	
	hex_dec:
		LDX #$0000 
		TXY
	.y
		CMP #$64
		BCC .x
		SBC #$64
		INY
		BRA .y
	.x
		CMP #$0A
		BCC .return
		SBC #$0A
		INX
		BRA .x
	.return
		RTS
print bytes

warnpc $009045
