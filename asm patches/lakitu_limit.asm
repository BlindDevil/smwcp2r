lorom
header

!MAX_SPINIES = 3

org $01EA21
	autoclean JSL lakitu_throw_hijack
	
freecode

lakitu_throw_hijack:	
	LDA $13BF
	CMP #$1E		;translevel number that uses limited spiny throwing
	BNE .spawn
	
	PHX
	LDY #!MAX_SPINIES-1
	LDX #$0B
.loop				; count how many spinies are alive right now
	LDA $14C8,x		; if the max was reached, don't spawn more spinies
	CMP #$08
	BCC .check_next
	LDA $9E,x
	CMP #$13
	BEQ .its_a_spiny
	CMP #$14
	BNE .check_next
.its_a_spiny
	DEY
	BMI .abort
.check_next
	DEX
	BPL .loop
	PLX
.spawn
	JML $82A9E4		; find free sprite slot routine (then returns to code)

.abort
	PLX
	LDA #$FF
	RTL			; return with N set, which will not spawn anything
