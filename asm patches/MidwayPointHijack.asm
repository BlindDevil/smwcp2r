;Midway Point block/object code hijack by Blind Devil
;Just a little patch that makes SMW's midway point trigger Kaijyuu's midway flag 0,
;and changes the object routine so it doesn't display if it's been triggered.

;Here are some defines copied from Kaijyuu's MMP patch.
!OWLevelNum = $13BF
!CheckpointRAM = $7FB4D9

;Checkpoint to trigger value.
!CheckpointNumber = $00

org $00F2D8
autoclean JSL MidpointBlock	;call our code.
NOP #4				;no operation 4 times - erase leftovers.

org $0DA699
autoclean JML MidpointObject	;jump to our code.
NOP				;no operation once - erase leftovers.

freecode
MidpointBlock:
PHX				;preserve whatever's in X
LDX !OWLevelNum			;load OW number on X
LDA #!CheckpointNumber		;load checkpoint value
STA !CheckpointRAM,x		;store to checkpoint table, indexed.
LDA #$01			;load value
STA $13CE			;store to "midway point collected" flag.
LDA $7FC060			;load CDM16 flags address 1
AND #$07			;preserve bits 0, 1 and 2
ASL #4				;shift bits to the left four times
ORA $7FB539,x			;set bits to temporary SMWC coins collected address according to translevel index
STA $7FB539,x			;store result in there.
PLX				;restore X
RTL				;return.

MidpointObject:
LDA $13CE			;load "midway point collected" flag.
BEQ .draw			;if not set, always draw bar.

LDX !OWLevelNum			;load OW number on X
LDA !CheckpointRAM,x		;load checkpoint table, indexed
CMP #!CheckpointNumber		;compare to checkpoint value
BNE .draw			;if not equal, bar can be drawn.

JML $8DA6B0			;jump back to original code - return, don't draw bar.

.draw
JML $8DA69E			;jump back to original code - draw midway point bar.