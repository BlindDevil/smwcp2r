;A small hijack I made to the switch palace switch so it generates SFX when pressed.

;SA-1 detector (though I honestly don't think the hack should or would be converted at some point)
!sa1	= 0
!addr	= $0000			; $0000 if LoROM, $6000 if SA-1 ROM.
!bank	= $800000		; $80:0000 if LoROM, $00:0000 if SA-1 ROM.

if read1($00FFD5) == $23
	!sa1	= 1
	!addr	= $6000
	!bank	= $000000
	sa1rom
endif

org $00EEC7
autoclean JSL Switch
NOP

freedata
Switch:
LDA #$0B
STA $1DF9|!addr

LDA #$FF
STA $0DDA|!addr		;restored code
RTL			;return.