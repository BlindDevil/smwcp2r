;Anti-softlock (HuFlungDu)
;has no effect when start+select to beat level debug is applied
lorom

org $00A261
	autoclean jml check_start_select	;ldy.w $13BF : lda.w $1EA2,y

org $00A269
	check_start_select_return_exit:

org $00A289
	check_start_select_return_no_exit:

freedata

check_start_select:
ldy.w $13BF
lda.w $1EA2,y
bmi .ret_exit
lda $7B
bne .ret_no_exit
lda $77
and.b #%00000100
beq .ret_no_exit

.ret_exit
jml check_start_select_return_exit

.ret_no_exit
jml check_start_select_return_no_exit
