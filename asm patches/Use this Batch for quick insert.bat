@echo Before using this, make sure Asar is placed in the same folder.
@pause

cls
@echo Welcome to Batman.
@echo.
@echo.

asar.exe "1player.asm" "SMWCP2R.smc"
@echo.
asar.exe "antisoftlock.asm" "SMWCP2R.smc"
@echo.
asar.exe "asarspritetiles.asm" "SMWCP2R.smc"
@echo.
asar.exe "autosave.asm" "SMWCP2R.smc"
@echo.
asar.exe "better_powerdown.asm" "SMWCP2R.smc"
@echo.
asar.exe "blockmask.asm" "SMWCP2R.smc"
@echo.
asar.exe "CastleBlkFix.asm" "SMWCP2R.smc"
@echo.
asar.exe "classicfire.asm" "SMWCP2R.smc"
@echo.
asar.exe "CloudTimerWarning.asm" "SMWCP2R.smc"
@echo.
asar.exe "Counter Break Yoshi.asm" "SMWCP2R.smc"
@echo.
asar.exe "DisableScreenBarrierViaRam.asm" "SMWCP2R.smc"
@echo.
asar.exe "easymode7.asm" "SMWCP2R.smc"
@echo.
asar.exe "FallingSpikeFix.asm" "SMWCP2R.smc"
@echo.
asar.exe "frozensprfix.asm" "SMWCP2R.smc"
@echo.
asar.exe "lakitu_limit.asm" "SMWCP2R.smc"
@echo.
asar.exe "LevelExtend.asm" "SMWCP2R.smc"
@echo.
asar.exe "MarioGFXDMA.asm" "SMWCP2R.smc"
@echo.
asar.exe "MidwayPointHijack.asm" "SMWCP2R.smc"
@echo.
asar.exe "MMP16.asm" "SMWCP2R.smc"
@echo.
asar.exe "NoMesBoxWin.asm" "SMWCP2R.smc"
@echo.
asar.exe "NoteWallFix.asm" "SMWCP2R.smc"
@echo.
asar.exe "OWMusic.asm" "SMWCP2R.smc"
@echo.
asar.exe "optimize_2132_store.asm" "SMWCP2R.smc"
@echo.
asar.exe "PalaceSwitchHijack.asm" "SMWCP2R.smc"
@echo.
asar.exe "quickrom.asm" "SMWCP2R.smc"
@echo.
asar.exe "RedoneSMWCHUD.asm" "SMWCP2R.smc"
@echo.
asar.exe "reverse_gravity.asm" "SMWCP2R.smc"
@echo.
asar.exe "RolloverFix.asm" "SMWCP2R.smc"
@echo.
asar.exe "SMWCP2-HexEdits.asm" "SMWCP2R.smc"
@echo.
asar.exe "BoneFix.asm" "SMWCP2R.smc"
@echo.
asar.exe "sram_plus.asm" "SMWCP2R.smc"
@echo.
asar.exe "Title.asm" "SMWCP2R.smc"
@echo.
asar.exe "tweaker.asm" "SMWCP2R.smc"
@echo.
asar.exe "VScrollReprogrammed.asm" "SMWCP2R.smc"
@echo.
asar.exe "yoshi.asm" "SMWCP2R.smc"
@echo.

@echo.
@echo - Check for any errors.
@echo.
@echo - btw remember to SHIFT+F8 your ROM in LM because SRAM Plus
@echo undoes the sprite 19 fix by default.
@echo.
@echo - If using LM v3.04, remember to apply tidefix.asm manually.
@echo.
@echo - STEP 1/2 COMPLETE -
@pause

cls
@echo Batman is about to insert 'ferrisgfxpatch.asm' into the ROM.
@echo If you've already inserted this patch previously, close this
@echo window. Else, press Enter to continue.
@pause
@echo.
asar.exe "ferrisgfxpatch.asm" "SMWCP2R.smc"
@echo.
@echo.
@echo - STEP 2/2 COMPLETE -
@pause